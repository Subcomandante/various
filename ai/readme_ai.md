# Themenkomplex: Künstliche Intelligenz

[TOC]



## Grober Überblick

### 1. **Maschinelles Lernen (Machine Learning, ML)**

- **Überwachtes Lernen (Supervised Learning):** Modelle lernen aus gelabelten Trainingsdaten, um Vorhersagen für neue, unbekannte Daten zu treffen.
- **Unüberwachtes Lernen (Unsupervised Learning):** Modelle entdecken Muster oder Strukturen in ungelabelten Daten, wie z.B. Clusterbildung.
- **Bestärkendes Lernen (Reinforcement Learning):** Agenten lernen durch Versuch und Irrtum, indem sie Aktionen in einer Umgebung ausführen und Belohnungen erhalten.
- **Neuronale Netze:** Modelle, die von der Struktur des menschlichen Gehirns inspiriert sind und besonders bei großen und komplexen Daten erfolgreich sind.

### 2. **Natürliche Sprachverarbeitung (Natural Language Processing, NLP)**

- **Textverstehen:** Analyse und Interpretation von Texten, z.B. Sentimentanalyse oder Themenextraktion.
- **Maschinelle Übersetzung:** Automatische Übersetzung von Texten zwischen verschiedenen Sprachen.
- **Spracherkennung und Sprachsynthese:** Umwandlung von gesprochener Sprache in Text und umgekehrt.
- **Dialogsysteme (Chatbots):** Systeme, die natürliche Gespräche mit Nutzern führen können.

### 3. **Computer Vision**

- **Bildklassifikation:** Erkennung und Kategorisierung von Objekten in Bildern.
- **Objekterkennung:** Lokalisierung und Identifikation von Objekten in Bildern oder Videos.
- **Bildsegmentierung:** Aufteilung eines Bildes in seine Bestandteile, z.B. Trennung des Hintergrunds vom Vordergrund.
- **Generative Modelle:** Erzeugung neuer Bilder oder Videos auf Basis von Trainingsdaten, wie bei Deepfakes.



## Data Preprocessing

Die Schritte des Data Preprocessing sind entscheidend, um die Qualität der Daten zu maximieren und die Leistung der darauf aufbauenden Modelle zu optimieren. Das Preprocessing legt oft den Grundstein für den Erfolg des gesamten KI-Projekts.

Data Preprocessing bereitet die Rohdaten so auf, dass sie von Algorithmen effizient genutzt werden können. Die wichtigsten Aspekte des Data Preprocessing umfassen:

### 1. **Datenbereinigung (Data Cleaning)**

- **Umgang mit fehlenden Werten:** Fehlende Daten können durch Imputation (z.B. Mittelwert, Median, Modus) ersetzt oder entfernt werden, je nach Menge und Verteilung der fehlenden Werte.
- **Behandlung von Ausreißern:** Identifikation und gegebenenfalls Entfernung von Ausreißern, die die Analyse verfälschen könnten.
- **Deduplizierung:** Entfernung doppelter Einträge, die die Ergebnisse beeinflussen könnten.

### 2. **Datenumwandlung (Data Transformation)**

- **Normalisierung/Standardisierung:** Skalierung der Daten auf einen bestimmten Wertebereich (z.B. [0, 1]) oder Anpassung an eine Normalverteilung (Mittelwert 0, Standardabweichung 1), um Modelle stabiler zu machen.
- **Log-Transformation:** Anwendung einer logarithmischen Transformation auf Variablen, um die Verteilung von Daten zu glätten.
- **Diskretisierung:** Umwandlung kontinuierlicher Daten in diskrete Kategorien, was nützlich sein kann, wenn ein Modell besser mit kategorischen als mit kontinuierlichen Daten arbeitet.

### 3. **Feature Engineering**

- **Feature Selection:** Auswahl der relevantesten Merkmale (Features), um Überanpassung (Overfitting) zu vermeiden und die Modellleistung zu verbessern.
- **Feature Extraction:** Erzeugung neuer Merkmale aus bestehenden Daten, z.B. durch PCA (Principal Component Analysis) oder andere Techniken zur Dimensionsreduktion.
- **Encoding von kategorialen Variablen:** Umwandlung von kategorialen Daten in numerische Formate, z.B. mittels One-Hot-Encoding oder Label-Encoding, damit sie von ML-Algorithmen verarbeitet werden können.

### 4. **Datenintegration**

- **Zusammenführen von Datensätzen:** Kombination mehrerer Datenquellen, um einen umfassenden Datensatz zu erstellen.
- **Handling von Inkonsistenzen:** Sicherstellung, dass Daten aus verschiedenen Quellen kompatibel sind, was oft eine Vereinheitlichung von Formaten und Einheiten erfordert.

### 5. **Datenreduktion**

- **Reduktion der Dimensionalität:** Verringern der Anzahl der Merkmale, um Rechenressourcen zu sparen und Modelle einfacher und schneller zu machen, ohne die Modellleistung zu stark zu beeinträchtigen.
- **Sampling:** Reduktion der Datenmenge durch zufällige oder gezielte Auswahl von Stichproben, besonders nützlich bei sehr großen Datensätzen.

### 6. **Datenanreicherung**

- **Erweiterung des Datensatzes:** Hinzufügen externer Daten, die für das Problem relevant sind, z.B. demografische Daten, Wetterinformationen oder Marktdaten.
- **Augmentation:** Erzeugung neuer Datenpunkte durch Transformationen wie Rotation, Verschiebung oder Spiegelung, insbesondere bei Bilddaten häufig verwendet.

### 7. **Zeitliche Daten (Time Series Data)**

- **Zeitreihenbereinigung:** Glätten von Daten durch Moving Averages oder andere Techniken, um Rauschen zu reduzieren.
- **Feature Creation aus Zeitreihen:** Erzeugung von Merkmalen wie Lag-Werten, rollierenden Mittelwerten oder saisonalen Komponenten.

### 8. **Data Splitting**

- **Training- und Test-Split:** Aufteilen des Datensatzes in Trainings- und Testsets, um die Modellleistung objektiv zu bewerten.
- **Cross-Validation:** Eine Methode, bei der der Datensatz in mehrere Teile geteilt wird, um das Modell in verschiedenen Trainings- und Testkombinationen zu validieren.
package de.brockhaus.ai.word2vec;

import java.io.File;
import java.io.IOException;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.ops.transforms.Transforms;

public class Word2VecSimilarity {

    private Word2Vec model;

    public Word2VecSimilarity(String modelPath) throws IOException {
        // Laden eines vortrainierten Modells
        this.model = WordVectorSerializer.readWord2VecModel(new File(modelPath));
    }

    public double calculateWordSimilarity(String word1, String word2) {
        return model.similarity(word1, word2);
    }

    public double calculateSentenceSimilarity(String sentence1, String sentence2) {
        INDArray vector1 = sentenceToVector(sentence1);
        INDArray vector2 = sentenceToVector(sentence2);
        return Transforms.cosineSim(vector1, vector2);
    }

    private INDArray sentenceToVector(String sentence) {
        String[] words = sentence.toLowerCase().split("\\s+");
        INDArray sentenceVector = model.getWordVectorMatrix(words[0]).dup();
        for (int i = 1; i < words.length; i++) {
            sentenceVector.addi(model.getWordVectorMatrix(words[i]));
        }
        return sentenceVector.divi(words.length);
    }

    public static void main(String[] args) throws IOException {
        String modelPath = "path/to/your/word2vec/model.bin";
        Word2VecSimilarity similarity = new Word2VecSimilarity(modelPath);

        // Wort-Ähnlichkeit berechnen
        double wordSimilarity = similarity.calculateWordSimilarity("Hund", "Katze");
        System.out.println("Ähnlichkeit zwischen 'Hund' und 'Katze': " + wordSimilarity);

        // Satz-Ähnlichkeit berechnen
        String sentence1 = "Der Hund läuft im Park.";
        String sentence2 = "Ein Hund spielt auf der Wiese.";
        double sentenceSimilarity = similarity.calculateSentenceSimilarity(sentence1, sentence2);
        System.out.println("Ähnlichkeit zwischen '" + sentence1 + "' und '" + sentence2 + "': " + sentenceSimilarity);
    }
}

package de.brockhaus.ai.word2vec;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.FileSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;



/**
* 
Es gibt viele interessante Anwendungsfälle für die Funktionalität des "nächsten Wortes" in Word2Vec-Modellen. Hier sind einige praktische Beispiele:

Suchmaschinen-Optimierung:

Erweiterung von Suchanfragen mit semantisch ähnlichen Begriffen.
Beispiel: Eine Suche nach "Auto" könnte auch Ergebnisse für "Fahrzeug" oder "PKW" einschließen.


Autovervollständigung und Vorschlagssysteme:

Verbesserte Texteingabe-Vorschläge basierend auf dem Kontext.
Beispiel: Beim Tippen von "Berliner" könnte das System "Mauer", "Hauptstadt" oder "Fernsehturm" vorschlagen.


Content-Empfehlungssysteme:

Vorschlagen ähnlicher Artikel oder Produkte basierend auf Schlüsselwörtern.
Beispiel: Nach dem Lesen eines Artikels über "Machine Learning" könnten Artikel zu "Künstliche Intelligenz" oder "Deep Learning" empfohlen werden.


Sentiment-Analyse und Meinungsbergbau:

Identifizierung von Synonymen und verwandten Begriffen für Stimmungsausdrücke.
Beispiel: Erkennen, dass "hervorragend" und "ausgezeichnet" ähnliche positive Konnotationen haben.


Sprachübersetzung und mehrsprachige Anwendungen:

Verbesserung von Übersetzungen durch Kontextverständnis.
Beispiel: Unterscheidung zwischen verschiedenen Bedeutungen von "Bank" (Finanzinstitut vs. Sitzgelegenheit) basierend auf den umgebenden Wörtern.


Textgenerierung und kreatives Schreiben:

Vorschlagen von alternativen Wörtern zur Verbesserung des Schreibstils.
Beispiel: Ersetzen von überverwendeten Wörtern durch stilistisch passende Alternativen.


Informationsextraktion und Named Entity Recognition:

Identifizierung von Entitäten und deren Varianten in Texten.
Beispiel: Erkennen, dass "Merkel", "Bundeskanzlerin" und "Angela" sich auf dieselbe Person beziehen können.


Plagiatserkennung:

Identifizierung von Texten mit ähnlichem Inhalt, auch wenn unterschiedliche, aber semantisch verwandte Wörter verwendet werden.


Chatbots und Dialogsysteme:

Verbesserung des Verständnisses von Benutzereingaben durch Erkennung von Synonymen und verwandten Konzepten.


Marktforschung und Trendanalyse:

Identifizierung von Clustern verwandter Begriffe in sozialen Medien oder Kundenfeedback.


Barrierefreie Kommunikation:

Unterstützung von Menschen mit eingeschränktem Wortschatz durch Vorschlagen alternativer Ausdrücke.


Bildunterschriften und visuelle Suche:

Generierung relevanter Tags oder Beschreibungen für Bilder basierend auf visuellen Elementen.

 * 
 * Zu analysierende Texte entnommen: https://wod.corpora.uni-leipzig.de/en/de/topic/2024/08/21/7
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class Word2VecService {

	private static final Logger LOG = LogManager.getLogger(); 
	
	
	public Word2Vec trainWord2VecModel(String filePath) throws Exception {

		Word2Vec model = null;

		// Laden der Textdaten aus einer Datei
		URL url = this.getClass().getClassLoader().getResource(filePath);
		File file = new File(url.toURI());
		
		List<String> stopWords = this.getListOfStopWords();

		SentenceIterator iter = new FileSentenceIterator(file);

		// Konfiguration des Tokenizers
		TokenizerFactory t = new DefaultTokenizerFactory();
		t.setTokenPreProcessor(new CommonPreprocessor());

		// Konfiguration und Training des Word2Vec-Modells
		LOG.info("Starte Training des Word2Vec-Modells...");
		model = new Word2Vec.Builder()
				.stopWords(stopWords)  // Hier werden die Stoppwörter hinzugefügt
				.minWordFrequency(5) // Minimale Häufigkeit eines Wortes
				.iterations(1) // Anzahl der Trainingsiterationen
				.layerSize(100) // Dimension des Vektorraums
				.seed(42) // Seed für Reproduzierbarkeit
				.windowSize(5) // Kontextfenstergröße
				.iterate(iter) // Eingabe-Iterator
				.tokenizerFactory(t) // Tokenizer-Factory
				.build();

		model.fit(); // Trainiert das Modell

		LOG.info("Word2Vec-Modell erfolgreich trainiert.");

		return model;
	}

	public Collection<String> getNearestWord(Word2Vec model, String word) {
		// Testet das Modell, indem es die 10 ähnlichsten Wörter zum gegebenen Wort
		// findet
		
		LOG.info("Vocabulary size: " + model.getVocab().numWords());
		LOG.info("Word in vocabulary: " + model.hasWord(word));
		LOG.info("Word vector: " + Arrays.toString(model.getWordVector(word)));

		Collection<String> similarWords = model.similarWordsInVocabTo(word, 0.1);
		LOG.info("Similar words: " + similarWords);
		
		return model.wordsNearest(word, 10);
	}
	

    /**
     * Erweitert eine Suchanfrage mit semantisch ähnlichen Begriffen.
     *
     * @param model Das trainierte Word2Vec-Modell
     * @param query Die ursprüngliche Suchanfrage
     * @param topN Anzahl der ähnlichsten Wörter, die pro Wort hinzugefügt werden sollen
     * @return Eine Liste mit der ursprünglichen Anfrage und den erweiterten Begriffen
     */
    public List<String> expandSearchQuery(Word2Vec model, String query, int topN) {
        // Initialisiere die erweiterte Anfrage mit der ursprünglichen Anfrage
        List<String> expandedQuery = new ArrayList<>();
        expandedQuery.add(query);

        // Teile die Anfrage in einzelne Wörter
        String[] queryWords = query.toLowerCase().split("\\s+");

        // Für jedes Wort in der Anfrage
        for (String word : queryWords) {
            // Überprüfe, ob das Wort im Modell vorhanden ist
            if (model.hasWord(word)) {
                // Finde die nächsten 'topN' Wörter
                Collection<String> nearestWords = model.wordsNearest(word, topN);
                // Füge diese Wörter zur erweiterten Anfrage hinzu
                expandedQuery.addAll(nearestWords);
            }
        }

        // Entferne Duplikate und gib die erweiterte Anfrage zurück
        return expandedQuery.stream().distinct().collect(Collectors.toList());
    }

	public void saveModel(Word2Vec model, String outputPath) {
		try {
			WordVectorSerializer.writeWord2VecModel(model, outputPath);
			LOG.info("Modell erfolgreich gespeichert unter: " + outputPath);
		} catch (Exception e) {
			LOG.error("Fehler beim Speichern des Modells: " + e.getMessage());
		}
	}

	public Word2Vec loadModel(String inputPath) {
		try {
			Word2Vec model = WordVectorSerializer.readWord2VecModel(inputPath);
			LOG.info("Modell erfolgreich geladen von: " + inputPath);
			return model;
		} catch (Exception e) {
			LOG.warn("Fehler beim Laden des Modells: " + e.getMessage());
			return null;
		}
	}

	private List<String> getListOfStopWords() {
		Iterator<Object> iter = GermanAnalyzer.getDefaultStopSet().iterator();
		List<String> stopWords = new ArrayList<>();
		while(iter.hasNext()) {
		    char[] stopWord = (char[]) iter.next();
		    stopWords.add(new String (stopWord));
		}
		LOG.info("German Stopwords" + stopWords);
		
		return stopWords;
	}
}
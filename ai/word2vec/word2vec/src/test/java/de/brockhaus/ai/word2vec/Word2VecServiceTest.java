package de.brockhaus.ai.word2vec;

import java.util.Collection;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.junit.Test;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class Word2VecServiceTest {
	
	@Test
	public void testTrainWord2VecService() throws Exception {
		
		Word2VecService service = new Word2VecService();
		Word2Vec model = service.trainWord2VecModel("./news.txt");
		Collection<String> hits = service.getNearestWord(model, "kamala");
		
		for (String hit : hits) {
			System.out.println(hit);
		}
	}
	
	public static void main(String[] args) throws Exception {
		Word2VecServiceTest test = new Word2VecServiceTest();
		test.testTrainWord2VecService();
	}

}

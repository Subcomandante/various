[TOC]



# ZScore

## Allgemein

Beim Training von maschinellen Lernmodellen werden die Features oft normalisiert, um eine bessere Leistung des Modells zu erzielen. Der **Z-Score hilft, alle Features auf eine vergleichbare Skala zu bringen**. Dieses Verfahren hilft, **Unterschiede fair zu bewerten, unabhängig von den spezifischen Metriken** der Einzelmerkmale.

Geschäftsszenarien für Z-Score-Normierung:

1. Finanzkennzahlenanalyse:
   - Vergleich von Unternehmen verschiedener Größen oder Branchen
   - Normalisierung von Kennzahlen wie ROI, Umsatzwachstum, Gewinnmarge
2. Qualitätskontrolle in der Produktion:
   - Identifizierung von Abweichungen in Produktionslinien
   - Vergleich der Effizienz verschiedener Maschinen oder Standorte
3. Kundensegmentierung:
   - Analyse von Kundenverhalten über verschiedene Produktkategorien
   - Identifizierung von High-Value-Kunden basierend auf normalisierten Metriken
4. Marketing-Kampagnen-Analyse:
   - Vergleich der Effektivität verschiedener Kampagnen
   - Normalisierung von Metriken wie Klickraten, Konversionsraten über verschiedene Plattformen
5. Supply Chain Optimierung:
   - Bewertung der Lieferantenleistung über verschiedene Kategorien
   - Vergleich von Lagerbeständen und Umschlagsraten zwischen Standorten
6. Personalrekrutierung:
   - Normalisierung von Bewerberbewertungen aus verschiedenen Quellen oder Interviewrunden
   - Faire Vergleiche zwischen Kandidaten mit unterschiedlichem Hintergrund

In all diesen Fällen hilft die Z-Score-Normierung, Äpfel mit Äpfeln zu vergleichen, indem sie die inhärenten Unterschiede in den Datenverteilungen verschiedener Gruppen oder Kategorien berücksichtigt.

Aber: Z-Score ist **kein Algorithmus zur Klassifizierung**, sondern eine **Methode zur Normalisierung oder Standardisierung** von Daten. Es wandelt Daten in eine Form um, bei der der Mittelwert 0 und die Standardabweichung 1 ist. Diese Transformation ermöglicht es, Daten verschiedener Skalen vergleichbar zu machen, was für viele maschinelle Lernalgorithmen vorteilhaft ist. **Es hilft also dabei, die Daten für Klassifikationsalgorithmen vorzubereiten, aber es klassifiziert nicht selbst**.

## Formeln ZScore

**Z=(X−μ) / σ**

### Variablen

- Z: Der Z-Score, der angibt, wie viele Standardabweichungen ein Datenpunkt XXX vom Mittelwert μ\muμ entfernt ist.
- X: Der Wert des einzelnen Datenpunkts, dessen Z-Score berechnet werden soll.
- μ: Der Mittelwert der Verteilung, aus der der Datenpunkt stammt.
- σ: Die Standardabweichung der Verteilung, die die Streuung der Daten um den Mittelwert angibt.

### Interpretation

- **Z = 0**: Der Datenpunkt liegt genau auf dem Mittelwert.
- **Z > 0**: Der Datenpunkt liegt über dem Mittelwert (positiver Z-Score).
- **Z < 0**: Der Datenpunkt liegt unter dem Mittelwert (negativer Z-Score).

**|Z| > 2**: Der Datenpunkt liegt mehr als zwei Standardabweichungen vom Mittelwert entfernt und könnte als potenzieller Ausreißer betrachtet werden, je nach Kontext.

Zur Berechnung  der Varianz und des Mittelwerts siehe [hier](https://welt-der-bwl.de/Varianz).

## Vorteile ZScore

Vorteile der Z-Score-Methode:

1. Einfachheit: Z-Score ist leicht zu verstehen und zu implementieren.
2. Interpretierbarkeit: Die Ergebnisse sind transparent und leicht zu erklären.
3. Geringe Datenmenge: Funktioniert auch mit kleinen Datensätzen.
4. Recheneffizienz: Benötigt wenig Rechenleistung.

# Beispiel

Über verschiedene Kategorien hinweg wird die Summe der Einkäufe von Kunden erhoben.

```java
		// customer data: map of customer ids and for each customer id a map fo category and money spentn
		Map<String, Map<String, Double>> customerData = new HashMap<>();
		customerData.put("Kunde1", Map.of("Elektronik", 1500.0, "Kleidung", 500.0, "Lebensmittel", 2000.0));
		customerData.put("Kunde2", Map.of("Elektronik", 3000.0, "Kleidung", 1000.0, "Lebensmittel", 1500.0));
		customerData.put("Kunde3", Map.of("Elektronik", 500.0, "Kleidung", 2000.0, "Lebensmittel", 3000.0));
		customerData.put("Kunde4", Map.of("Elektronik", 2000.0, "Kleidung", 1500.0, "Lebensmittel", 1000.0));
		customerData.put("Kunde5", Map.of("Elektronik", 1000.0, "Kleidung", 3000.0, "Lebensmittel", 500.0));
```

Nachfolgend werden die ZScores pro Kunde über jede der Kategorien berechnet. Hierfür werden Mittelwert der Kategorie und Standardabweichung des Einzelwerts ermittelt.

```java
	//calculation average
	private double calculateMean(double[] values) {
		double avg = Arrays.stream(values).average().orElse(0.0);
		
		return avg;
	}

	// calculating deviation
	private double calculateStandardDeviation(double[] values, double mean) {
		double variance = Arrays.stream(values).map(x -> Math.pow(x - mean, 2)).average().orElse(0.0);
		double deviation = Math.sqrt(variance);
		
		return deviation;
	}
```

Hat man die Werte, lässt sich der ZScore leicht berechnen:

```java
	private double calculateZScore(Double var, double mean, double stdDev) {
		return (var - mean) / stdDev;
		
	}
```

Das Ergebnis für die individuellen Kunden und die ihnen entsprechenden Kategorien sieht dann so aus:

```java
2024-09-02 16:21:51 INFO  CustomerSegmentationZScoreTest:28 - Kunde: Kunde4, ZScores: {Elektronik=0.46499055497527714, Lebensmittel=-0.6974858324629157, Kleidung=-0.11624763874381928}
2024-09-02 16:21:51 INFO  CustomerSegmentationZScoreTest:28 - Kunde: Kunde3, ZScores: {Elektronik=-1.278724026182012, Lebensmittel=1.62746694241347, Kleidung=0.46499055497527714}
2024-09-02 16:21:51 INFO  CustomerSegmentationZScoreTest:28 - Kunde: Kunde2, ZScores: {Elektronik=1.62746694241347, Lebensmittel=-0.11624763874381928, Kleidung=-0.6974858324629157}
2024-09-02 16:21:51 INFO  CustomerSegmentationZScoreTest:28 - Kunde: Kunde1, ZScores: {Elektronik=-0.11624763874381928, Lebensmittel=0.46499055497527714, Kleidung=-1.278724026182012}
2024-09-02 16:21:51 INFO  CustomerSegmentationZScoreTest:28 - Kunde: Kunde5, ZScores: {Elektronik=-0.6974858324629157, Lebensmittel=-1.278724026182012, Kleidung=1.62746694241347}
```

Optional kann für jeden Kunden noch ein  - ggf gewichteter - Gesamt ZScore errechnet werden 

```java
2024-09-02 16:51:33 INFO  CustomerSegmentationZScoreTest:38 - Kunde: Kunde3, Overall ZScore: 0.813733471206735
2024-09-02 16:51:33 INFO  CustomerSegmentationZScoreTest:38 - Kunde: Kunde2, Overall ZScore: 0.813733471206735
2024-09-02 16:51:33 INFO  CustomerSegmentationZScoreTest:38 - Kunde: Kunde1, Overall ZScore: -0.9299811099505542
2024-09-02 16:51:33 INFO  CustomerSegmentationZScoreTest:38 - Kunde: Kunde5, Overall ZScore: -0.3487429162314579
```



Hierbei bedeutet ein:

1. Positiver Z-Score:
   - Ein positiver Z-Score zeigt überdurchschnittliche Ausgaben an.
   - Je höher der positive Wert, desto weiter liegen die Ausgaben über dem Durchschnitt.
2. Negativer Z-Score:
   - Ein negativer Z-Score zeigt unterdurchschnittliche Ausgaben an.
   - Je niedriger (negativer) der Wert, desto weiter liegen die Ausgaben unter dem Durchschnitt.
3. Z-Score von 0:
   - Dies würde genau durchschnittliche Ausgaben bedeuten.

Abschließend kann ein bestimmtes Intervall einer Kundengruppe Premium, Good oder Basic zugeordnet werden

```java
2024-09-02 16:52:09 DEBUG CustomerSegmentationZScoreService:98 - {Kunde4=-0.34874291623145787, Kunde3=0.813733471206735, Kunde2=0.8137334712067349, Kunde1=-0.9299811099505542, Kunde5=-0.3487429162314579}
2024-09-02 16:52:09 INFO  CustomerSegmentationZScoreTest:47 - Kunde: Kunde4, Rating: Good
2024-09-02 16:52:09 INFO  CustomerSegmentationZScoreTest:47 - Kunde: Kunde3, Rating: Premium
2024-09-02 16:52:09 INFO  CustomerSegmentationZScoreTest:47 - Kunde: Kunde2, Rating: Premium
2024-09-02 16:52:09 INFO  CustomerSegmentationZScoreTest:47 - Kunde: Kunde1, Rating: Basic
2024-09-02 16:52:09 INFO  CustomerSegmentationZScoreTest:47 - Kunde: Kunde5, Rating: Good
```



# Neuronales Netz oder ZScore

Vorteile Neuronales Netz gegenüber der Z-Score-Methode:

1. Komplexe Muster: Das neuronale Netz kann nicht-lineare Beziehungen zwischen den Ausgaben in verschiedenen Kategorien erkennen.
2. Vorhersagekraft: Es kann neue Kunden direkt einem Segment zuordnen, ohne manuelle Schwellenwerte festlegen zu müssen.
3. Anpassungsfähigkeit: Mit mehr Daten könnte das Modell kontinuierlich verbessert werden.

Allerdings hat dieser Ansatz auch Nachteile:

1. Datenbedarf: Für genaue Ergebnisse benötigt man in der Regel viel mehr Daten als im Beispiel gezeigt.
2. Blackbox-Charakter: Die Entscheidungsfindung des neuronalen Netzes ist weniger transparent als bei der Z-Score-Methode.
3. Überkomplexität: Für einfache Segmentierungsaufgaben könnte ein neuronales Netz überdimensioniert sein.

## Wann ist welcher Ansatz besser?

Z-Score ist besser, wenn:

- Sie wenig Daten haben.
- Interpretierbarkeit wichtig ist.
- Sie schnelle, einfache Segmentierung benötigen.
- Die Beziehungen zwischen den Variablen relativ linear sind.

Ein neuronales Netz ist besser, wenn:

- Sie große Mengen an Kundendaten haben.
- Komplexe, nicht-lineare Beziehungen in den Daten vermutet werden.
- Höchste Genauigkeit erforderlich ist.
- Sie viele verschiedene Eingabevariablen haben.

In der Praxis könnte ein hybrider Ansatz sinnvoll sein: Beginnen Sie mit einer Z-Score-basierten Segmentierung für schnelle Einblicke und einfache Interpretierbarkeit. Wenn Sie mehr Daten sammeln und komplexere Muster vermuten, können Sie zu einem neuronalen Netz oder anderen fortgeschrittenen Machine-Learning-Techniken übergehen.

Letztendlich hängt die Wahl der Methode von Ihren spezifischen Geschäftsanforderungen, der Datenverfügbarkeit und den Ressourcen für die Implementierung und Wartung ab.




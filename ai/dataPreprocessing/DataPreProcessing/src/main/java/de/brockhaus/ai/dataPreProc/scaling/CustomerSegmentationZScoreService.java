package de.brockhaus.ai.dataPreProc.scaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class CustomerSegmentationZScoreService {
	
	private static final Logger LOG = LogManager.getLogger();

	public static void main(String[] args) {

		CustomerSegmentationZScoreService service = new CustomerSegmentationZScoreService();
		
		// customer data: map of customer ids and for each customer id a map fo category and money spentn
		Map<String, Map<String, Double>> customerData = new HashMap<>();
		customerData.put("Kunde1", Map.of("Elektronik", 1500.0, "Kleidung", 500.0, "Lebensmittel", 2000.0));
		customerData.put("Kunde2", Map.of("Elektronik", 3000.0, "Kleidung", 1000.0, "Lebensmittel", 1500.0));
		customerData.put("Kunde3", Map.of("Elektronik", 500.0, "Kleidung", 2000.0, "Lebensmittel", 3000.0));
		customerData.put("Kunde4", Map.of("Elektronik", 2000.0, "Kleidung", 1500.0, "Lebensmittel", 1000.0));
		customerData.put("Kunde5", Map.of("Elektronik", 1000.0, "Kleidung", 3000.0, "Lebensmittel", 500.0));


		Map<String, Map<String, Double>> normalizedData = service.getZScoreNormalizedData4CustomerAndCategories(customerData);
		
		Map<String, Double> overallZScores = service.getOverallZScorePerCustomer(normalizedData);
		
		// Sortierung der Kunden nach Gesamt-Z-Score
		List<Map.Entry<String, Double>> sortedCustomers = new ArrayList<>(overallZScores.entrySet());
		sortedCustomers.sort(Map.Entry.<String, Double>comparingByValue().reversed());

		// Ausgabe der Ergebnisse
		LOG.info("Normalisierte Kundendaten (Z-Scores):");
		normalizedData.forEach((customerId, scores) -> {
			System.out.println("\n" + customerId + ":");
			scores.forEach((category, score) -> System.out.printf("  %s: %.2f%n", category, score));
			System.out.printf("  Gesamt Z-Score: %.2f%n", overallZScores.get(customerId));
		});

		// Kundensegmentierung basierend auf Gesamt-Z-Score
		System.out.println("\nKundensegmentierung:");
		for (int i = 0; i < sortedCustomers.size(); i++) {
			Map.Entry<String, Double> customer = sortedCustomers.get(i);
			
			String segment = i < sortedCustomers.size() / 3 ? "Premium"
					: i < 2 * sortedCustomers.size() / 3 ? "Standard" : "Basis";
			
			System.out.printf("%s: %s (Gesamt Z-Score: %.2f)%n", customer.getKey(), segment, customer.getValue());
		}
	}
	
	public Map<String, String> getCustomerRanking(Map<String, Map<String, Double>> customerData) {
		
		Map<String, String> rankings = new HashMap<String, String>();
		Map<String, Double> zScores = this.getOverallZScorePerCustomer(customerData);
		
		for (String key : zScores.keySet()) {
			
			if (zScores.get(key) > 0.5) {
				rankings.put(key, "Premium");
			} else if (zScores.get(key) < 0.5 && zScores.get(key) > -0.5) {
				rankings.put(key, "Good");
			} else {
				rankings.put(key, "Basic");
			}
		}
		
		return rankings;
	}

	/**
	 * @param normalizedData
	 * @return
	 */
	public Map<String, Double> getOverallZScorePerCustomer(Map<String, Map<String, Double>> customerData) {
		
		// first normalize per customer and category
		Map<String, Map<String, Double>> normalizedData = this.getZScoreNormalizedData4CustomerAndCategories(customerData);
		
		// then calculate overall ZScore for customer
		Map<String, Double> overallZScores = normalizedData.entrySet().stream()
			    .collect(Collectors.toMap(
			        Map.Entry::getKey, 
			        entry -> sumValues(entry.getValue())
			    ));
		
		LOG.debug(overallZScores);
		
		return overallZScores;
	}

	public Map<String, Map<String, Double>> getZScoreNormalizedData4CustomerAndCategories(Map<String, Map<String, Double>> customerData) {
		
		// Z-Score normalization for each category
		Map<String, Map<String, Double>> normalizedData = new HashMap<>();
		
		
		String[] customerIds = customerData.keySet().toArray(new String[0]);
		LOG.debug(customerIds);
		
		Set<String> keySet = customerData.get(customerIds[0]).keySet();
		LOG.debug(keySet);
		
		for (String category : keySet) {
			
			LOG.debug("going for category: {}", category);
			
			// getting all values for the category
			double[] categoryValues = customerData.values().stream().mapToDouble(m -> m.get(category)).toArray();

			double mean = this.calculateMean(categoryValues);			
			double stdDev = this.calculateStandardDeviation(categoryValues, mean);
			
			LOG.debug("for category {}: mean {}, deviation {}", category, mean, stdDev);

			for (String customerId : customerData.keySet()) {
				
				LOG.debug("going for customer: {}", customerId);
				Double var = customerData.get(customerId).get(category);
				
				// calculating zScore
				double zScore =  this.calculateZScore(var, mean, stdDev);
				
				normalizedData.computeIfAbsent(customerId, k -> new HashMap<>()).put(category, zScore);
			}
		}

		LOG.debug(normalizedData);
		
		return normalizedData;
	}

	/**
	 * @param var
	 * @param mean
	 * @param stdDev
	 * @return 
	 */
	private double calculateZScore(Double var, double mean, double stdDev) {
		return (var - mean) / stdDev;
		
	}

	//calculation average
	private double calculateMean(double[] values) {
		double avg = Arrays.stream(values).average().orElse(0.0);
		
		return avg;
	}

	// calculating deviation
	private double calculateStandardDeviation(double[] values, double mean) {
		double variance = Arrays.stream(values).map(x -> Math.pow(x - mean, 2)).average().orElse(0.0);
		double deviation = Math.sqrt(variance);
		
		return deviation;
	}
	
	private static double sumValues(Map<String, Double> valueMap) {
	    return valueMap.values().stream()
	                   .mapToDouble(Double::doubleValue)
	                   .sum();
	}
}

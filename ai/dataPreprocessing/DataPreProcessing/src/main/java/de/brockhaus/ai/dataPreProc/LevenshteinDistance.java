package de.brockhaus.ai.dataPreProc;

/**
 * Die Levenshtein-Distanz kommt unter anderem für Rechtschreibprüfungen, Suchfunktionen oder für das Erkennen von D
 * uplikaten und Plagiaten zum Einsatz. 
 * Über die Einstellung einer bestimmten Levenshtein-Distanz zwischen dem Suchbegriff und den zu durchsuchenden 
 * Zeichenketten lässt sich beispielsweise nach ähnlichen Begriffen (unscharfe Suche) suchen. 
 * Rechtschreibfehler bei der Eingabe eines Suchbegriffs liefern durch die Vorgabe einer Levenshtein-Distanz größer 0 
 * unter Umständen dennoch das gewünschte Ergebnis.
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class LevenshteinDistance {
	
	/**
	 * Berechnet die Levenshtein-Distanz zwischen zwei Strings.
	 * 
	 * @param s1 Der erste String
	 * @param s2 Der zweite String
	 * @return Die Levenshtein-Distanz
	 */
	public static int calculate(String s1, String s2) {
		
		// Erstelle eine Matrix zur Speicherung der Zwischenergebnisse
		int[][] dp = new int[s1.length() + 1][s2.length() + 1];

		// Fülle die Matrix
		for (int i = 0; i <= s1.length(); i++) {
			
			for (int j = 0; j <= s2.length(); j++) {
				
				if (i == 0) {
					// Wenn der erste String leer ist, ist die Distanz gleich der Länge des zweiten
					// Strings
					dp[i][j] = j;
					
				} else if (j == 0) {
					// Wenn der zweite String leer ist, ist die Distanz gleich der Länge des ersten
					// Strings
					dp[i][j] = i;
					
				} else {
					// Berechne die minimale Anzahl von Operationen
					dp[i][j] = min(
							// Ersetzen (oder keine Änderung, wenn die Zeichen gleich sind)
							// Wenn die Zeichen gleich sind, kostet es 0 (beibehalten).
							// Wenn sie unterschiedlich sind, kostet es 1 (ersetzen).
							dp[i - 1][j - 1] + (s1.charAt(i - 1) == s2.charAt(j - 1) ? 0 : 1),
							
							// Löschen
							dp[i - 1][j] + 1,
							
							// Einfügen
							dp[i][j - 1] + 1);
				}
			}
		}

		// Der Wert in der unteren rechten Ecke der Matrix ist die Levenshtein-Distanz
		return dp[s1.length()][s2.length()];
	}

	/**
	 * Hilfsmethode zur Berechnung des Minimums von drei Zahlen.
	 */
	private static int min(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	public static void main(String[] args) {
		String s1 = "kitten";
		String s2 = "sitting";
		System.out.println("Levenshtein-Distanz zwischen '" + s1 + "' und '" + s2 + "': " + calculate(s1, s2));
	}
}

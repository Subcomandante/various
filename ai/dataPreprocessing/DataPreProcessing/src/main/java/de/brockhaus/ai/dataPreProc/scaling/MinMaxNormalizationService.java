package de.brockhaus.ai.dataPreProc.scaling;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * 
MinMax Normalization is a data preprocessing technique used to scale numerical features to a fixed range, typically between 0 and 1. Here's a brief explanation:

Purpose: It transforms data to have a common scale without distorting differences in the ranges of values.
Formula: For each feature x, it's calculated as:
x_normalized = (x - x_min) / (x_max - x_min)
Result: All values will be between 0 and 1, with the minimum value becoming 0 and the maximum becoming 1.
Use cases: Commonly used in machine learning algorithms that are sensitive to the scale of input features.
Pros: Preserves zero values and reduces the effect of outliers.
Cons: Doesn't handle outliers well if they are far from other data points.
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class MinMaxNormalizationService {
	
	private static final Logger LOG = LogManager.getLogger();

	/**
	 * Min/Max normalization with provided "boundaries"
	 * 
	 * @param data the data to be normalized
	 * @param min minimum to be scaled to
	 * @param max maximum to be scaled to
	 * @return array of normalized data
	 */
    public static double[] minMaxNormalize(double[] data, double min, double max) {
    	
        double[] normalizedData = new double[data.length];
        
        for (int i = 0; i < data.length; i++) {
        	
        	// here it happens
            normalizedData[i] = (data[i] - min) / (max - min);
        }
        
        return normalizedData;
    }
    
    /**
     * 
     * @param data
     * @return
     */
    public static double[] minMaxNormalize(double[] data) {
    	
    	double min = getMin(data);
    	double max = getMax(data);
    	
        double[] normalizedData = new double[data.length];
        
        for (int i = 0; i < data.length; i++) {
        	// here it happens
            normalizedData[i] = (data[i] - min) / (max - min);
        }
        
        return normalizedData;
    }

	/**
	 * Min/Max de-normalization with provided "boundaries"
	 * 
	 * @param data the data to be normalized
	 * @param min minimum to be scaled to
	 * @param max maximum to be scaled to
	 * @return array of normalized data
	 */
    public static double[] denormalize(double[] normalizedData, double min, double max) {
    	
        double[] originalData = new double[normalizedData.length];
        
        for (int i = 0; i < normalizedData.length; i++) {
        	// here it happens
            originalData[i] = normalizedData[i] * (max - min) + min;
        }
        
        return originalData;
    }
    

    // Hilfsfunktion, um das Minimum in einem Array zu finden
    public static double getMin(double[] data) {
        double min = data[0];
        
        for (double d : data) {
        	
            if (d < min) {
                min = d;
            }
        }
        
        return min;
    }

    // Hilfsfunktion, um das Maximum in einem Array zu finden
    public static double getMax(double[] data) {
    	
        double max = data[0];
        
        for (double d : data) {
        	
            if (d > max) {
                max = d;
            }
        }
        return max;
    }
}


package de.brockhaus.ai.dataPreProc.scaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class OneHotEncodingService {
	
	private static final Logger LOG = LogManager.getLogger();

    
	/**
	 * performs one-hot encoding on a given array of strings
	 * 
	 * @param data
	 * @return
	 */
    public static Map<String, int[]> oneHotEncode(String[] data) {
    	
        Map<String, int[]> encodingMap = new HashMap<>();
        
        //creates a Set from this array, which removes all duplicates
        HashSet<String> uniqueSet = new HashSet<>(Arrays.asList(data));
        
        // convert to a List
        List<String> uniqueCategories = new ArrayList<>(uniqueSet);

        for (String category : uniqueCategories) {
        	
        	// create a new integer array with a length equal to the number of unique categories
        	// array will represent the one-hot encoded vector for this category
            int[] encodedArray = new int[uniqueCategories.size()];
            
            // Find the index of the current category in the uniqueCategories list, 
            int categoryIndex = uniqueCategories.indexOf(category);
            
            // set that index to 1 in the encodedArray. All other elements remain 0.
            encodedArray[categoryIndex] = 1;
            
            // add the category (as key) and its one-hot encoded array (as value) to the encodingMap
            encodingMap.put(category, encodedArray);
        }

        return encodingMap;
    }

    /**
     * performs one-hot un-encoding on a given array of strings
     * @param encodingMap
     * @param oneHotArray
     * @return
     */
    public static String oneHotDecode(Map<String, int[]> encodingMap, int[] oneHotArray) {
    	
        for (Map.Entry<String, int[]> entry : encodingMap.entrySet()) {
        
        	if (Arrays.equals(entry.getValue(), oneHotArray)) {
                return entry.getKey();
            }
        }
        
        
        return null;  // if there is no matching value
    }
}


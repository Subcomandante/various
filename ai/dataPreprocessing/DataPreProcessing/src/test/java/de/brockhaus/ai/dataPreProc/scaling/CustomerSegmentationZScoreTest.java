package de.brockhaus.ai.dataPreProc.scaling;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class CustomerSegmentationZScoreTest {

	private static final Logger LOG = LogManager.getLogger();
	
	CustomerSegmentationZScoreService service = new CustomerSegmentationZScoreService();
	
	Map<String, Map<String, Double>> customerData;

	@Test
	public void testGetZScoreNormalizedData4CustomerAndCategories() {

		Map<String, Map<String, Double>> results = service.getZScoreNormalizedData4CustomerAndCategories(customerData);
		
		for (String key : results.keySet()) {
			LOG.info("Kunde: {}, ZScores: {}", key, results.get(key));
		}
	}

	
	@Test
	public void testGetZScoreNormalizedData4CustomerAndCategoriesgetOverallZScorePerCustomer() {
		Map<String, Double> results = service.getOverallZScorePerCustomer(customerData);
		
		for (String key : results.keySet()) {
			LOG.info("Kunde: {}, Overall ZScore: {}", key, results.get(key));
		}
	}
	
	@Test
	public void testGetCustomerRanking() {
		Map<String, String> rankings = service.getCustomerRanking(customerData);
		
		for (String key : rankings.keySet()) {
			LOG.info("Kunde: {}, Rating: {}", key, rankings.get(key));
			
		}
	}
	
	@BeforeTest
	public void init() {
		// Kundendaten: Map von Kunden-IDs zu Maps von Produktkategorien und Ausgaben
		customerData = new HashMap<>();
		customerData.put("Kunde1", Map.of("Elektronik", 1500.0, "Kleidung", 500.0, "Lebensmittel", 2000.0));
		customerData.put("Kunde2", Map.of("Elektronik", 3000.0, "Kleidung", 1000.0, "Lebensmittel", 1500.0));
		customerData.put("Kunde3", Map.of("Elektronik", 500.0, "Kleidung", 2000.0, "Lebensmittel", 3000.0));
		customerData.put("Kunde4", Map.of("Elektronik", 2000.0, "Kleidung", 1500.0, "Lebensmittel", 1000.0));
		customerData.put("Kunde5", Map.of("Elektronik", 1000.0, "Kleidung", 3000.0, "Lebensmittel", 500.0));
	}
}

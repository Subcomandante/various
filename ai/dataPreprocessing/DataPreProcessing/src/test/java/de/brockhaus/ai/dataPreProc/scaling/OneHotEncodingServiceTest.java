package de.brockhaus.ai.dataPreProc.scaling;

import java.util.Arrays;
import java.util.Map;

import org.testng.annotations.Test;

import de.brockhaus.ai.dataPreProc.scaling.OneHotEncodingService;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class OneHotEncodingServiceTest {

	@Test
	public void testOneHotEncode() {
		
        // Beispiel-Daten
        String[] categories = {"Premiumkunde", "Gelegenheitskunde", "Normalkunde"};

        // One-Hot-Encoding durchführen und in einer HashMap speichern
        Map<String, int[]> oneHotEncodedMap = OneHotEncodingService.oneHotEncode(categories);

        // Ausgabe der One-Hot-codierten Daten mit Bezug zu den Originaldaten
        System.out.println("One-Hot-codierte Daten:");
        for (Map.Entry<String, int[]> entry : oneHotEncodedMap.entrySet()) {
            System.out.println(entry.getKey() + " -> " + Arrays.toString(entry.getValue()));
        }

        // Dekodierung: Ein One-Hot-codiertes Array zurück zu seinem Originalwert dekodieren
        String original = OneHotEncodingService.oneHotDecode(oneHotEncodedMap, new int[]{0, 1, 0});
        System.out.println("Dekodiertes Ergebnis: " + original);
    }
}

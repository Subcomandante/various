package de.brockhaus.ai.dataPreProc.scaling;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class MinMaxNormalizationTest {
	
	private double[] data = { 10, 20, 30, 40, 50 };

	@Test
	public void testMinMaxNormalize() {

		// normalize data
		double[] normalizedData = MinMaxNormalizationService.minMaxNormalize(data);

		// print normalized data
		System.out.println("original data:");
		for (double d : data) {
			System.out.print(d + " ");
		}
		
		System.out.println("\n normalized data:");
		for (double d : normalizedData) {
			System.out.print(d + " ");
		}

		// back 2 original data
		double[] denormalizedData = MinMaxNormalizationService.denormalize(normalizedData, MinMaxNormalizationService.getMin(data), MinMaxNormalizationService.getMax(data));

		// print de-normalized data
		System.out.println("\n de-normalized data:");
		for (double d : denormalizedData) {
			System.out.print(d + " ");
		}
		
		Assert.assertEquals(data, denormalizedData);
	}
}

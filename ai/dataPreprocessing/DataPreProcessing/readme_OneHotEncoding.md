[TOC]



# OneHotEncoding

One-Hot-Encoding ist eine Technik, die häufig in maschinellem Lernen und Datenverarbeitung verwendet wird, um **kategoriale Variablen in eine numerische Form umzuwandeln**, die von maschinellen Lernmodellen besser verarbeitet werden kann.

Angenommen, ein E-Commerce-Unternehmen möchte ein maschinelles Lernmodell entwickeln, um vorherzusagen, welche Kunden mit höherer Wahrscheinlichkeit einen Kauf tätigen werden. Eine der Eingabevariablen ist die **Kundenkategorie**, die die Kunden in verschiedene Gruppen unterteilt, z.B.:

- **'Neukunde'**
- **'Bestandskunde'**
- **'VIP-Kunde'**

Diese Kundenkategorie ist eine kategoriale Variable, die nicht direkt in ein maschinelles Lernmodell eingegeben werden kann, da die Modelle in der Regel numerische Eingaben erwarten. Daher kann One-Hot-Encoding verwendet werden, um diese kategoriale Variable in eine numerische Form zu konvertieren.

## Anwendung von One-Hot-Encoding

Stellen wir uns vor, wir haben eine Datenreihe mit den folgenden Kunden:

| Kunde | Kundenkategorie |
| ----- | --------------- |
| A     | Neukunde        |
| B     | Bestandskunde   |
| C     | VIP-Kunde       |
| D     | Neukunde        |

Durch One-Hot-Encoding wird die Kundenkategorie in mehrere binäre Variablen umgewandelt:

| Kunde | Neukunde | Bestandskunde | VIP-Kunde |
| ----- | -------- | ------------- | --------- |
| A     | 1        | 0             | 0         |
| B     | 0        | 1             | 0         |
| C     | 0        | 0             | 1         |
| D     | 1        | 0             | 0         |

Hier wurde jede Kategorie in eine separate Spalte aufgeteilt. Der Wert "1" gibt an, dass der Kunde zu dieser Kategorie gehört, und "0", dass er nicht dazugehört.

## Warum One-Hot-Encoding?

Durch die Verwendung von One-Hot-Encoding kann das maschinelle Lernmodell besser verstehen, dass die Kundenkategorien unabhängig voneinander sind und keine natürliche Reihenfolge haben. Wenn wir beispielsweise stattdessen die Kategorien einfach als 0, 1 und 2 kodieren würden, könnte das Modell fälschlicherweise annehmen, dass es eine Art von Reihenfolge oder eine Abstandsbeziehung zwischen den Kategorien gibt, was in diesem Fall nicht zutrifft.

### Ausgangssituation

Das Unternehmen möchte mithilfe eines neuronalen Netzwerks vorhersagen, ob ein Kunde einen Kauf tätigen wird, basierend auf verschiedenen Merkmalen wie Kundenkategorie, Besuchszeit, Anzahl der besuchten Seiten usw. Ein zentrales Merkmal ist die **Kundenkategorie** mit den Werten **"Neukunde"**, **"Bestandskunde"** und **"VIP-Kunde"**.

### Datensätze und One-Hot-Encoding

Zunächst werden die Rohdaten, einschließlich der Kundenkategorie, in ein für das neuronale Netzwerk verarbeitbares Format umgewandelt.

#### Rohdaten:

| Kunde | Kundenkategorie | Besuchszeit (Minuten) | Besuchte Seiten | Kauf (Zielvariable) |
| ----- | --------------- | --------------------- | --------------- | ------------------- |
| A     | Neukunde        | 5                     | 3               | Nein                |
| B     | Bestandskunde   | 20                    | 5               | Ja                  |
| C     | VIP-Kunde       | 15                    | 7               | Ja                  |
| D     | Neukunde        | 7                     | 2               | Nein                |

#### Nach One-Hot-Encoding:



| Kunde | Neukunde | Bestandskunde | VIP-Kunde | Besuchszeit (Minuten) | Besuchte Seiten | Kauf (Zielvariable) |
| ----- | -------- | ------------- | --------- | --------------------- | --------------- | ------------------- |
| A     | 1        | 0             | 0         | 5                     | 3               | Nein                |
| B     | 0        | 1             | 0         | 20                    | 5               | Ja                  |
| C     | 0        | 0             | 1         | 15                    | 7               | Ja                  |
| D     | 1        | 0             | 0         | 7                     | 2               | Nein                |

## Anmerkung

Es macht durchaus Sinn, die Werte für **Besuchszeit** und **besuchte Seiten** sowie die Zielvariable **Kauf** zu normieren, um bessere Ergebnisse zu erzielen. Normierung, auch bekannt als Standardisierung oder Skalierung, ist eine gängige Praxis im maschinellen Lernen, insbesondere bei neuronalen Netzwerken.

| Kunde | Neukunde | Bestandskunde | VIP-Kunde | Besuchszeit (skaliert) | Besuchte Seiten (skaliert) | Kauf (binär) |
| ----- | -------- | ------------- | --------- | ---------------------- | -------------------------- | ------------ |
| A     | 1        | 0             | 0         | -0.91                  | -0.87                      | 0            |
| B     | 0        | 1             | 0         | 1.53                   | 0.29                       | 1            |
| C     | 0        | 0             | 1         | 0.76                   | 1.73                       | 1            |
| D     | 1        | 0             | 0         | -0.49                  | -1.15                      | 0            |

Hierbei wurden die Spalten Besuchszeit und Besuchte Seiten mittels ZScore normiert. Für die Zielvariable gilt:

- **"Ja"** wird zu **1** (was bedeutet, dass der Kunde einen Kauf getätigt hat).
- **"Nein"** wird zu **0** (was bedeutet, dass der Kunde keinen Kauf getätigt hat).
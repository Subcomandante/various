[TOC]



# MinMax-Normalisierung

Die Min/Max-Normalisierung ist eine Methode zur Skalierung von Daten, bei der die Werte einer bestimmten Variablen in einen festgelegten Bereich (normalerweise zwischen 0 und 1) transformiert werden. Dies geschieht, um die Variablen vergleichbarer zu machen, insbesondere wenn sie unterschiedliche Einheiten oder Größenordnungen haben und ist besonders nützlich für viele Machine-Learning-Algorithmen.

Die Normalisierung erfolgt mit der folgenden Formel:
$$
\[
\text{Normalisierter Wert} = \frac{(x - \text{min}(X))}{(\text{max}(X) - \text{min}(X))}
\]
$$


- **X**: Der ursprüngliche Wert der Variablen.
- **min(X)**: Der minimale Wert der Variablen XXX in den Daten.
- **max(X)**: Der maximale Wert der Variablen XXX in den Daten.

## Beispiel

Rohdaten

| Kunde | Kundenkategorie | Besuchszeit (Minuten) | Besuchte Seiten | Kauf (Zielvariable) |
| ----- | --------------- | --------------------- | --------------- | ------------------- |
| A     | Neukunde        | 5                     | 3               | Nein                |
| B     | Bestandskunde   | 20                    | 5               | Ja                  |
| C     | VIP-Kunde       | 15                    | 7               | Ja                  |
| D     | Neukunde        | 7                     | 2               | Nein                |



Tabelle mit den min/max-normalisierten Werten für die Spalten "Besuchszeit (Minuten)" und "Besuchte Seiten":

| Kunde | Kundenkategorie | Besuchszeit (Minuten) | Besuchte Seiten | Kauf (Zielvariable) |
| ----- | --------------- | --------------------- | --------------- | ------------------- |
| A     | Neukunde        | 0.0000                | 0.2             | Nein                |
| B     | Bestandskunde   | 1.0000                | 0.6             | Ja                  |
| C     | VIP-Kunde       | 0.6667                | 1.0             | Ja                  |
| D     | Neukunde        | 0.1333                | 0.0             | Nein                |



Nun sind sowohl die "Besuchszeit" als auch die "Besuchte Seiten" normalisiert. Die Werte liegen jeweils zwischen 0 und 1, wobei:

- Für die Besuchszeit: 0 repräsentiert die kürzeste Zeit (5 Minuten) und 1 die längste Zeit (20 Minuten).
- Für die besuchten Seiten: 0 repräsentiert die geringste Anzahl (2 Seiten) und 1 die höchste Anzahl (7 Seiten).


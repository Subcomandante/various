package de.brockhaus.ai.neuronalNetwork;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class NeuralNetworkTest {
	
	private static final Logger LOG = LogManager.getLogger();

	@Test
	public void littleRedRidinghoodTest() {
		
		// Wald, Großmutter, Wolf, Korb
		double[][] inputs = { 
				{ 1, 1, 1, 1}, // (1)  Geschichte mit Wald, Großmutter, Wolf und Korb => Rotkäppchen
				{ 1, 1, 1, 0}, // (2)  Geschichte ohne Korb  => Rotkäppchen
				{ 1, 1, 0, 1}, // (3)  Geschichte ohne Wolf
				{ 0, 0, 1, 0}, // (4)  Geschichte nur mit Wolf
				{ 1, 0, 1, 1}, // (5)  Geschichte ohne Großmutter
				{ 0, 1, 0, 0}, // (6)  Geschichte nur mit Großmutter
				{ 0, 1, 1, 1}, // (7)  Geschichte nur mit Großmutter und Wolf  => Rotkäppchen
				{ 1, 1, 0, 0}, // (8)  Geschichte nur mit Großmutter und Korb
				{ 0, 1, 1, 1}, // (9)  Geschichte ohne Wald => Rotkäppchen
				{ 1, 0, 0, 0}, // (10) Geschichte nur mit Wald
				{ 1, 0, 1, 0}, // (11) Geschichte nur mit Wald und Wolf
				{ 1, 0, 0, 1}, // (12) Geschichte nur mit Wald und Korb
				{ 0, 0, 0, 0}  // (13) Keine Elemente von Rotkäppchen
		};

		double[][] targets = { 
				{1}, // (1)  Geschichte mit Wald, Großmutter, Wolf und Korb => Rotkäppchen
				{1}, // (2)  Geschichte ohne Korb  => Rotkäppchen
				{0}, // (3)  Geschichte ohne Wolf
				{0}, // (4)  Geschichte nur mit Wolf
				{0}, // (5)  Geschichte ohne Großmutter
				{0}, // (6)  Geschichte nur mit Großmutter
				{1}, // (7)  Geschichte nur mit Großmutter und Wolf  => Rotkäppchen
				{0}, // (8)  Geschichte nur mit Großmutter und Korb
				{1}, // (9)  Geschichte ohne Wald
				{0}, // (10) Geschichte nur mit Wald
				{0}, // (11) Geschichte nur mit Wald und Wolf
				{0}, // (12) Geschichte nur mit Wald und Korb
				{0}  // (13) Keine Elemente von Rotkäppchen
		};

		// creating a neural network composed of 6 hidden layers with 4 neurons each and one output neuron
		NeuralNetwork nn = new NeuralNetwork(4, new int[] { 4,4 }, 1);
		// training the network
		nn.train(inputs, targets, 5000, 0.01);

        double[][] newStories = {
                {1, 1, 1, 1}, // alle Merkmale								=> Rotkäppchen
                {0, 0, 1, 0}, // Geschichte nur mit einem Wolf 				=> nicht Rotkäppchen
                {1, 0, 0, 0}, // Geschichte nur mit einem Wald 				=> nicht Rotkäppchen
                {0, 0, 0, 0}, // keine Merkmale von Rotkäppchen 			=> nicht Rotkäppchen
                {0, 1, 1, 1}  // Geschichte nur mit Großmutter und Wolf  	=> Rotkäppchen
            };
		
        HashMap<Integer, Double> results = new HashMap<>();
        
		// testing of trained neural network
		for (int i = 0; i < newStories.length; i++) {
			double prediction = nn.predict(newStories[i]);
			results.put(i, prediction);
			
			boolean isRedRidingHood = prediction > 0.5;
			double predictionRounded = Math.round(prediction * 100.0) / 100.0;
			
			LOG.info("Input: " + Arrays.toString(newStories[i]) + ", Prediction: " + predictionRounded + " => " + isRedRidingHood);
		}
		
		Assert.assertTrue(results.get(0) > 0.5);
		Assert.assertTrue(results.get(1) < 0.5);
		Assert.assertTrue(results.get(2) < 0.5);
		Assert.assertTrue(results.get(3) < 0.5);
		Assert.assertTrue(results.get(4) > 0.5);
	}
	
	@Test
	public void xorTest() {
	
		double[][] inputs = { 
				{ 1, 1 }, // result should be 0
				{ 0, 0 }, // result should be 0
				{ 1, 0 }, // result should be 1
				{ 0, 1 } // result should be 1
		};

		double[][] targets = { 
				{ 0 }, 
				{ 0 }, 
				{ 1 }, 
				{ 1 } 
		};
		
		// creating a neural network composed of 6 hidden layers with 4 neurons each and one output neuron
		NeuralNetwork nn = new NeuralNetwork(2, new int[] { 4 }, 1);
		// training the network
		nn.train(inputs, targets, 100000, 0.1);
		
		// testing of trained neural network
		HashMap<Integer, Double> results = new HashMap<>();

		for (int i = 0; i < targets.length; i++) {
			double prediction = nn.predict(targets[i]);
			results.put(i, prediction);

			double predictionRounded = Math.round(prediction * 100.0) / 100.0;

			LOG.info("Input: " + Arrays.toString(targets[i]) + ", Prediction: " + predictionRounded);
		}
	}
}
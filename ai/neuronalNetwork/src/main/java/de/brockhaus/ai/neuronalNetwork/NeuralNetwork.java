package de.brockhaus.ai.neuronalNetwork;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.ai.neuronalNetwork.Layer.LAYER_TYPE;
import de.brockhaus.ai.neuronalNetwork.util.ValueCalculationUtil;

/**
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class NeuralNetwork implements Serializable {
	
	private static final Logger LOG = LogManager.getLogger();
	
    private List<Layer> layers;

    /**
     * Constructor
     * 
     * @param inputSize how many input neurons (= no of params)
     * @param hiddenLayerSizes the number of items determines number of hidden layers, the concrete item determines the number of neurons
     * @param outputSize
     */
    public NeuralNetwork(int inputSize, int[] hiddenLayerSizes, int outputSize) {
        layers = new ArrayList<>();
        
        // creating input layer according to input size
        Layer inputLayer = new Layer(inputSize, LAYER_TYPE.INPUT);
        layers.add(inputLayer);
        
        Layer previousLayer = inputLayer;
        
        for (int size : hiddenLayerSizes) {
            Layer hiddenLayer = new Layer(size, LAYER_TYPE.HIDDEN);
            previousLayer.connectTo(hiddenLayer);
            layers.add(hiddenLayer);
        
            previousLayer = hiddenLayer;
        }
        
        Layer outputLayer = new Layer(outputSize, LAYER_TYPE.OUTPUT);
        previousLayer.connectTo(outputLayer);
        layers.add(outputLayer);
        
        LOG.info("created neural network with {} layers", layers.size());
    }

    /**
     * more or less the feed forward through the layers
     * @param input
     * @return
     */
    public double predict(double[] input) {
    	
        // getting input layer (first in array of layers)
        List<Neuron> inputNeurons = layers.get(0).getNeurons();
        
        // setting input values to the neurons of input layer
        for (int i = 0; i < input.length; i++) {
            inputNeurons.get(i).setOutput(input[i]);
        }

        // activate all layers
        for (int i = 1; i < layers.size(); i++) {
            layers.get(i).activateNeurons();
        }

        // return the output of the last neuron in the output layer (last in layer array)
        return layers.get(layers.size() - 1).getNeurons().get(0).getOutput();
    }

    /**
     * 
     * @param inputs
     * @param targets
     * @param epochs
     * @param learningRate
     */
    public void train(double[][] inputs, double[][] targets, int epochs, double learningRate) {
    	
    	LOG.info("Training in {} epochs with learning rate {}", epochs, learningRate);
    	
    	// for each learning period
        for (int epoch = 0; epoch < epochs; epoch++) {
            
        	double totalError = 0;
            
        	for (int i = 0; i < inputs.length; i++) {
        		// feed forward
                double prediction = this.predict(inputs[i]);
                
                // calculating the error (difference between prediction and expected value
                double target = targets[i][0];                
                double error = target - prediction;
                LOG.debug("Difference between target {} and prediction {} = {}", target, prediction, error);
                totalError += Math.abs(error);

                // backpropagating
                this.backpropagate(error);
                this.updateWeights(learningRate);
            }
        	
            LOG.info("Epoch " + (epoch + 1) + ", Error: " + totalError);
        }
    }

    /**
     * backpropagation of error
     * 
     * @param error
     */
    private void backpropagate(double error) {
    	
    	// from output layer backwards
        for (int i = layers.size() - 1; i >= 0; i--) {
            Layer layer = layers.get(i);
            LOG.debug("Backpropagation from layer {}", layer.getType());
            
            List<Neuron> neurons = layer.getNeurons();

            // for each neuron of this layer
            for (int j = 0; j < neurons.size(); j++) {
                Neuron neuron = neurons.get(j);
                double output = neuron.getOutput();

                double delta;
                if (i == layers.size() - 1) {
                    // Output layer delta considering the error of this epoch
//                    delta = error * output * (1 - output);
                	delta = error * ValueCalculationUtil.getSigmoidDerivationFunctValue(output);
                	LOG.debug("delta for neuron {}: {}", neuron.getNeuronId(), delta);
                    
                } else {
                    // hidden layers
                    delta = 0;
                    
                    for (Connection connection : neuron.getOutgoingConnections()) {
                        Neuron nextNeuron = connection.getToNeuron();
                        delta += nextNeuron.getDelta() * connection.getWeight();
                    }
                    delta *= ValueCalculationUtil.getSigmoidDerivationFunctValue(output);
                    LOG.debug("delta for neuron {}: {}", neuron.getNeuronId(), delta);
                }

                neuron.setDelta(delta);
            }
        }
    }

    /**
     * 
     * @param learningRate
     */
    private void updateWeights(double learningRate) {
        for (Layer layer : layers) {
            for (Neuron neuron : layer.getNeurons()) {
                neuron.updateWeights(learningRate);
            }
        }
    }
    
    /**
     * Saves the neural network to a file.
     * 
     * @param filename The name of the file to save the model to.
     * @throws IOException If an I/O error occurs.
     */
    public void saveModel(String filename) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(this);
        }
    }

    /**
     * Loads a neural network from a file.
     * 
     * @param filename The name of the file to load the model from.
     * @return The loaded NeuralNetwork object.
     * @throws IOException If an I/O error occurs.
     * @throws ClassNotFoundException If the class of the serialized object cannot be found.
     */
    public static NeuralNetwork loadModel(String filename) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            return (NeuralNetwork) ois.readObject();
        }
    }
}
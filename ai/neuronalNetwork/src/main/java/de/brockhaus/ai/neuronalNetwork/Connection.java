package de.brockhaus.ai.neuronalNetwork;

import java.io.Serializable;

/**
 * Connection between two neurons including the weight of the connection
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class Connection implements Serializable {
	
    private double weight;
    
    private Neuron fromNeuron;
    
    private Neuron toNeuron;

    public Connection(Neuron from, Neuron to) {
        this.fromNeuron = from;
        this.toNeuron = to;
    
        // initialize weight randomly between -1 and 1
        this.weight = Math.random() * 2 - 1; 
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Neuron getFromNeuron() {
        return fromNeuron;
    }

    public Neuron getToNeuron() {
        return toNeuron;
    }

    public void updateWeight(double delta, double learningRate) {
        weight += learningRate * delta * fromNeuron.getOutput();
    }
}
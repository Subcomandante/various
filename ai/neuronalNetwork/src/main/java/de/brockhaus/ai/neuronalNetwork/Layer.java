package de.brockhaus.ai.neuronalNetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The layer (input, hidden or output) made up of neurons
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class Layer implements Serializable {
	
	public enum LAYER_TYPE{INPUT, HIDDEN, OUTPUT};
	
	private static final Logger LOG = LogManager.getLogger();
	
	private String layerId;

	// neurons within this layer
	private List<Neuron> neurons;
	
	private LAYER_TYPE type;

	/**
	 * 
	 * @param neuronCount how many neurons we want to have within this layer
	 */
    public Layer(int neuronCount, LAYER_TYPE type) {
    	
    	this.layerId = UUID.randomUUID().toString();
    	
    	this.type = type;
    	
        neurons = new ArrayList<>(neuronCount);
        
        for (int i = 0; i < neuronCount; i++) {
            neurons.add(new Neuron());
        }
        
        LOG.info("Layer {} ({}) created with {} neurons", layerId, type.toString(), neurons.size());
    }

    /**
     * connecting layers
     * @param nextLayer the layer to connect to
     */
    public void connectTo(Layer nextLayer) {
    	
        for (Neuron fromNeuron : this.neurons) {
            
        	for (Neuron toNeuron : nextLayer.getNeurons()) {
                Connection connection = new Connection(fromNeuron, toNeuron);
                fromNeuron.addOutgoingConnection(connection);
                toNeuron.addIncomingConnection(connection);
            }
        }
    }

    /**
     * calculating the results of all neurons of this layer
     * @return result of all neurons
     */
    public List<Double> activateNeurons() {
    	
        List<Double> outputs = new ArrayList<>(neurons.size());
        
        // for each neuron in layer
        for (Neuron neuron : neurons) {
            outputs.add(neuron.activate());
        }
        
        return outputs;
    }

    /**
	 * @return the type
	 */
	public LAYER_TYPE getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(LAYER_TYPE type) {
		this.type = type;
	}

	/**
	 * @param neurons the neurons to set
	 */
	public void setNeurons(List<Neuron> neurons) {
		this.neurons = neurons;
	}

	public List<Neuron> getNeurons() {
        return neurons;
    }
}
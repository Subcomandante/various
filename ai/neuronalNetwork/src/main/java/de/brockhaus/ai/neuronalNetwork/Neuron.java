package de.brockhaus.ai.neuronalNetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.ai.neuronalNetwork.util.ValueCalculationUtil;

/**
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class Neuron implements Serializable {
	
	private static final Logger LOG = LogManager.getLogger();
	
	private String neuronId;
	
    private List<Connection> incomingConnections;
    
    private List<Connection> outgoingConnections;
    
    private double bias;
    
    private double output;
    
    private double delta;

    public Neuron() {
    	
    	this.neuronId = UUID.randomUUID().toString();
    	
        incomingConnections = new ArrayList<>();
        outgoingConnections = new ArrayList<>();
    
        // initializing the bias randomly between -1 and 1
        bias = Math.random() * 2 - 1; 
    }

    public void addIncomingConnection(Connection connection) {
        incomingConnections.add(connection);
    }

    public void addOutgoingConnection(Connection connection) {
        outgoingConnections.add(connection);
    }

    /**
     * here we go for this neuron
     * @return value according to activation function (here: sigmoid)
     */
    public double activate() {
    	// initialize sum with actual bias
        double sum = bias;
        
        for (Connection connection : incomingConnections) {
        	
        	LOG.debug("Neuron {} got {}, weighted with {}", neuronId, connection.getFromNeuron().getNeuronId(), connection.getWeight());
        	
        	// getting the output from all neurons we*re connected with and multiply with weight
        	// as set to connection
            sum += connection.getFromNeuron().getOutput() * connection.getWeight();
        }
        
        output = ValueCalculationUtil .getSigmoidFunctValue(sum);
        
        LOG.debug("Neuron {} activated: {}", this.getNeuronId(), output);
        
        return output;
    }

    public void updateWeights(double learningRate) {
    	
        for (Connection connection : incomingConnections) {
            connection.updateWeight(delta, learningRate);
        }
        
        // updating the bias
        bias += learningRate * delta;
    }

    /**
	 * @return the neuronId
	 */
	public String getNeuronId() {
		return neuronId;
	}

	/**
	 * @param neuronId the neuronId to set
	 */
	public void setNeuronId(String neuronId) {
		this.neuronId = neuronId;
	}

	/**
	 * @return the bias
	 */
	public double getBias() {
		return bias;
	}

	/**
	 * @param bias the bias to set
	 */
	public void setBias(double bias) {
		this.bias = bias;
	}

	public double getOutput() {
        return output;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public List<Connection> getIncomingConnections() {
        return incomingConnections;
    }

    public List<Connection> getOutgoingConnections() {
        return outgoingConnections;
    }
}
package de.brockhaus.ai.neuronalNetwork.util;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class ValueCalculationUtil {
	
	/**
	 * Activation function within a neuron
	 * Takes any real value as input and transforms it into a value between 0 and 1. The sigmoid function is often used to model probabilities.
	 * 
	 * @param value
	 * @return
	 */
	public static double getSigmoidFunctValue(double value) {
		return 1 / (1 + Math.exp(-value));
	}
	
	/**
	 * The derivative indicates how strong the change in the input affects the output 
	 * This value is used to propagate the error “backwards” through the network.
	 * @param value
	 * @return
	 */
	public static double getSigmoidDerivationFunctValue(double value) {
		return  value * (1 - value);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static double getReLUFunctValue(double value) {
		return Math.max(0, value);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static double getReLUDerivationFunctValue(double value) {
		return  value > 0 ? 1 : 0;
	}
}

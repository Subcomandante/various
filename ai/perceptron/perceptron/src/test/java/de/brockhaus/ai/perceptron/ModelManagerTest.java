package de.brockhaus.ai.perceptron;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class ModelManagerTest {

	
	@Test
	public void testGetJson() throws JsonProcessingException {
		Perceptron p = new Perceptron(2, 0.5);
		
		ModelManager manager = new ModelManager();
		String json = manager.getJson(p);
		
		Assert.assertTrue(! json.equals(""));
	}
}

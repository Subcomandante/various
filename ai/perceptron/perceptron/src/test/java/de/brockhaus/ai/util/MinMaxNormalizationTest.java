package de.brockhaus.ai.util;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class MinMaxNormalizationTest {

	@Test
	public void testMinMaxNormalize() {

		// sample data
		double[] data = { 10, 20, 30, 40, 50 };

		// calculating min/max
		double min = MinMaxNormalization.getMin(data);
		double max = MinMaxNormalization.getMax(data);

		// normalize data
		double[] normalizedData = MinMaxNormalization.minMaxNormalize(data, min, max);

		// print normalized data
		System.out.println("original data:");
		for (double d : data) {
			System.out.print(d + " ");
		}
		System.out.println("\n normalized data:");
		for (double d : normalizedData) {
			System.out.print(d + " ");
		}

		// back 2 original data
		double[] denormalizedData = MinMaxNormalization.denormalize(normalizedData, min, max);

		// pront de-normalized data
		System.out.println("\n de-normalized data:");
		for (double d : denormalizedData) {
			System.out.print(d + " ");
		}
		
		Assert.assertEquals(data, denormalizedData);
	}

}

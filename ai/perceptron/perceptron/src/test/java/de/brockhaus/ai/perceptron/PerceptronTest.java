package de.brockhaus.ai.perceptron;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class PerceptronTest {
	
	private static final Logger LOG = LogManager.getLogger();

	// testing perceptron
	@Test
	public static void testPerceptron() {
		PurchasePrediction purchaseDecision = new PurchasePrediction();

        // training data {age, income} 
        double[][] trainingData = {
        		 {25, 30000}, 
        		 {35, 50000}, 
        		 {45, 70000}, 
        		 {55, 40000}, 
        		 {65, 60000}
        };
        // labels: 0 = no purchase, 1 = purchase
        int[] labels = {
        		0, 
        		1, 
        		1, 
        		0, 
        		1};

        // training of model
        purchaseDecision.trainModel(trainingData, labels, 10000);

        // testing
        LOG.info("Prediction for age of 25, income 25000: " + 
                		   purchaseDecision.predictPurchase(25, 25000));
        LOG.info("Prediction for age of 30, income 40000: " + 
                           purchaseDecision.predictPurchase(30, 40000));
        LOG.info("Prediction for age of 50, income 70000: " + 
                           purchaseDecision.predictPurchase(50, 70000));
        
        Assert.assertFalse(purchaseDecision.predictPurchase(25, 25000));
        Assert.assertFalse(purchaseDecision.predictPurchase(30, 40000));
        Assert.assertTrue(purchaseDecision.predictPurchase(50, 70000));
    }
}

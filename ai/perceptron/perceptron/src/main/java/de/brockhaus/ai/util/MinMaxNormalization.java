package de.brockhaus.ai.util;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class MinMaxNormalization {


    // Funktion zur Min-Max-Normalisierung
    public static double[] minMaxNormalize(double[] data, double min, double max) {
        double[] normalizedData = new double[data.length];
        for (int i = 0; i < data.length; i++) {
            normalizedData[i] = (data[i] - min) / (max - min);
        }
        return normalizedData;
    }

    // Funktion zur Denormalisierung
    public static double[] denormalize(double[] normalizedData, double min, double max) {
        double[] originalData = new double[normalizedData.length];
        for (int i = 0; i < normalizedData.length; i++) {
            originalData[i] = normalizedData[i] * (max - min) + min;
        }
        return originalData;
    }

    // Hilfsfunktion, um das Minimum in einem Array zu finden
    public static double getMin(double[] data) {
        double min = data[0];
        for (double d : data) {
            if (d < min) {
                min = d;
            }
        }
        return min;
    }

    // Hilfsfunktion, um das Maximum in einem Array zu finden
    public static double getMax(double[] data) {
        double max = data[0];
        for (double d : data) {
            if (d > max) {
                max = d;
            }
        }
        return max;
    }
}


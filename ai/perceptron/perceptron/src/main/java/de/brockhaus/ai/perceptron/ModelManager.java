package de.brockhaus.ai.perceptron;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class ModelManager {
	
	public String getJson(Perceptron perceptron) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(perceptron);
        
        return json;
	}
	
    public void saveToJsonFile(String filename, Perceptron perceptron) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(filename), perceptron);
    }

    public static Perceptron loadFromJsonFile(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(filename), Perceptron.class);
    }
}

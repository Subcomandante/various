package de.brockhaus.ai.perceptron;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class Perceptron {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger();

	// the weights (to be changed in every epoch)
	private double[] weights;
	
	// the bias
	private double bias;
	
	// stepping within training
	private double learningRate;

	/**
	 * Constructor
	 * @param inputSize number of parameter at input
	 * @param learningRate learning rate 
	 */
	public Perceptron(int inputSize, double learningRate) {

		this.weights = new double[inputSize];

		// initialisation of weight an bias with randomized values
		for (int i = 0; i < inputSize; i++) {
			// random value between -1 und 1
			this.weights[i] = Math.random() * 2 - 1; 
		}
		
		// random number between -1,0 (inclusive) and 1,0 (exclusive)
		this.bias = Math.random() * 2 - 1;
		this.learningRate = learningRate;
		
		LOG.info("Perceptron created, configured with: weights {}, bias {}, learning rate {}", weights, bias, learningRate);
	}

	/**
	 * prediction for an input tuple like {0.25 | 0.3}
	 * 
	 * @param inputs one of the inputs
	 * @return 0 or 1 depending on the weighted sum plus bias 
	 */
	public int predict(double[] inputs) {

		LOG.debug("predicting: {}", inputs);
		// weighted sum of the values of an input tuple like {0.25 | 0.3} 
		double sum = 0;
		
		// getting weighted sum for each input
		for (int i = 0; i < inputs.length; i++) {
			sum += inputs[i] * weights[i];
			LOG.debug("weighted sum for {} = {}", inputs[i], sum);
		}
		
		// adding bias to weighted sum
		sum += bias;

		// activation function (Schwellenwert)
		int result = sum > 0 ? 1 : 0;
		LOG.debug("weighted sum plus bias for {} = {} which is bigger than 0 {}, therefore returning {}", inputs, sum, sum > 0, result);
		
		return result;
	}

	/**
	 * training of perceptron
	 * 
	 * @param trainingData training data
	 * @param labels expected values
	 * @param epochs number of learning cycles
	 */
	public void train(double[][] trainingData, int[] labels, int epochs) {
		
		LOG.debug("Training using training data {}, labels (expected values) {} in {} cycles", trainingData, labels, epochs);
		
		// training of perceptron in learning cycles
		for (int epoch = 0; epoch < epochs; epoch++) {
		
			// for each entry of training data
			for (int i = 0; i < trainingData.length; i++) {
				
				LOG.debug("training input {}", trainingData[i]);
				
				// as seen in predict method the output can be 0 or one only
				int prediction = this.predict(trainingData[i]);
				
				// three values of error possible:
				// -1 (if label is 0 and prediction is 1)
				//  0 (if label and prediction match)
				//  1 (if label is 1 and prediction is 0)
				int error = labels[i] - prediction;
				
				LOG.debug("Prediction for {} in epoch {}: {} expected {} => is error: {} (error value: {})", trainingData[i], epoch, prediction, labels[i], (error == -1 || error == 1), error );

				// adjust weight 
				for (int j = 0; j < weights.length; j++) {
					
					LOG.debug("es post weight of trainingdata {}: {} ", weights[j], trainingData[i][j]);
					weights[j] += learningRate * error * trainingData[i][j];
					LOG.debug("ex ante weight of training data {}: {} ", weights[j], trainingData[i][j]);
				}
			
				LOG.debug("ex post bias: {}", bias);
				bias += learningRate * error;
				LOG.debug("ex ante bias: {}", bias);
			}
			
			LOG.info("\n final hyperparameters are: \n weights: {} \n bias: {} \n learningRate: {}", weights, bias, learningRate);
		}
	}

	/**
	 * @return the weights
	 */
	public double[] getWeights() {
		return weights;
	}

	/**
	 * @param weights the weights to set
	 */
	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	/**
	 * @return the bias
	 */
	public double getBias() {
		return bias;
	}

	/**
	 * @param bias the bias to set
	 */
	public void setBias(double bias) {
		this.bias = bias;
	}

	/**
	 * @return the learningRate
	 */
	public double getLearningRate() {
		return learningRate;
	}

	/**
	 * @param learningRate the learningRate to set
	 */
	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}
}
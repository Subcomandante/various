package de.brockhaus.ai.perceptron;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * mbohnen, Brockhaus Consulting GmbH
 */
public class PurchasePrediction {
	
	private static final Logger LOG = LogManager.getLogger();

	private Perceptron perceptron;

	public PurchasePrediction() {
		// Initialisierung des Perzeptrons mit 2 Eingaben (Alter und Einkommen)
		this.perceptron = new Perceptron(2, 0.1);
	}

	public void trainModel(double[][] trainingData, int[] labels, int epochs) {
		double[][] normalizedData = this.normalizeData(trainingData);
		// Training des Modells
		perceptron.train(normalizedData, labels, epochs);
	}

	public boolean predictPurchase(int age, double income) {
		// normalization
		double normalizedAge = this.normalizeAge(age);
		double normalizedIncome = this.normalizeIncome(income);

		double[] input = { normalizedAge, normalizedIncome };
		int prediction = perceptron.predict(input);

		// return true or false
		return prediction == 1;
	}
	
	private double[][] normalizeData(double[][] rawData) {
		
		double[][] normalizedData = new double[rawData.length][2];
	    for (int i = 0; i < rawData.length; i++) {
	    	normalizedData[i][0] = this.normalizeAge(rawData[i][0]);
	    	normalizedData[i][1] = this.normalizeIncome(rawData[i][1]);
	    }
	    
	    LOG.debug("\n Raw data \n {} \n normalized to \n {}", rawData, normalizedData);
	    
	    return normalizedData;
	}
	
	private double normalizeAge(double age) {
		// assumption: max age 100
		return age / 100.0; 
	}
	
	private double normalizeIncome(double income) {
		// assumption: max income 100.000
		return income / 100000.0; 
	}

	/**
	 * @return the perceptron
	 */
	public Perceptron getPerceptron() {
		return perceptron;
	}

	/**
	 * @param perceptron the perceptron to set
	 */
	public void setPerceptron(Perceptron perceptron) {
		this.perceptron = perceptron;
	}
}
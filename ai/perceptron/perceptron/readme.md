# Einführung in das Perzeptron

[TOC]



## Allgemein

<img src="https://data-science-blog.com/wp-content/uploads/2018/08/perzeptron.png" style="zoom:67%;" />

Das Perzeptron ist ein grundlegendes Konzept in der [Geschichte](https://www.robominds.de/blog/die-geschichte-der-ki-von-der-turingmaschine-bis-deep-learning) der künstlichen Intelligenz und des maschinellen Lernens; es war der erste Algorithmus welcher automatisch lernen konnte und bildet die Grundlage für die Entwicklung komplexerer neuronaler Netze. Das Konzept wurde vom amerikanischen Psychologen Frank Rosenblatt im Jahr 1957 inspiriert durch die Funktionsweise biologischer Neuronen entwickelt.

Ein Perzeptron besteht aus einem Neuron mit einer binären Ausgabe. Der Ausgang kann zwei Zustände annehmen: aktiv (1) oder inaktiv (0). Ist der Ausgang des Perzeptrons aktiv, nennt man dies auch „das Perzeptron oder das Neuron feuert“. Um den Zustand am Ausgang zu erzeugen, besitzt das Perzeptron mehrere Eingänge. Die Eingänge haben eine bestimmte veränderbare Gewichtung. Wird ein gesetzter Schwellenwert durch die Gewichtungen aller Eingänge über- oder unterschritten, verändert sich der Zustand des Neuronenausgangs. Durch das Trainieren (überwachtes Lernen) eines Perzeptrons mit vorgegebenen Datenmustern verändert sich die Gewichtung der Eingänge.

Wichtig ist, dass die Daten linear trennbar sind. Unter linear trennbar versteht man die Eigenschaft, dass die in einem Diagramm dargestellten Daten durch eine Linie trennbar sind und diese Linie sie einem bestimmten Ergebnis oder Muster zuordnet. Ein prominentes Beispiel ist das XOR-Problem, dieses ist nicht linear trennbar und ein Perzeptron scheitert an der Aufgabe.

Während einlagige Perzeptren nur aus einer einzigen Schicht künstlicher Neuronen mit Eingabevektoren und Ausgängen bestehen, sind bei mehrlagigen Perzeptren Neuronen in mehreren Schichten vorhanden und untereinander vernetzt. Neuronen der gleichen Schicht haben keine Verbindungen untereinander. Der Ausgang eines Neurons fungiert als Eingabevektor eines Neurons der nächsten Schicht. Je mehr Schichten in den Perzeptren vorhanden sind, desto vielfältiger sind die Klassifizierungsfähigkeiten des neuronalen Netzes. Kann beispielsweise ein einlagiges Perzeptron nur eine lineare Trennlinie zur Separierung der Daten finden, ist das zweilagige Perzeptron dazu fähig, Klassifizierungs-Polygone zu identifizieren. Durch das Hinzufügen einer weiteren Neuronen-Schicht lassen sich komplexere Klassifizierungsformen aus den Polygonen bilden. Hierzu mehr im Beispiel für Neuronale Netzwerke.

## Anwendungsfälle für Perceptren

**Binäre Klassifikation**

- **Spam-Filterung:** Klassifikation von E-Mails als "Spam" oder "Nicht-Spam".
- **Kreditantragsprüfung:** Entscheidung darüber, ob ein Kredit gewährt wird (Genehmigt/Nicht genehmigt) basierend auf einfachen Kriterien wie Einkommen und Kreditgeschichte.
- **Medizinische Diagnose:** Klassifizierung von Patienten in "Krank" oder "Gesund" basierend auf grundlegenden diagnostischen Tests.

**Einfaches maschinelles Lernen im Marketing**

- **Kundenkaufverhalten**: Vorhersage, ob ein Kunde basierend auf Merkmalen wie Alter, Einkommen, und vergangenem Kaufverhalten ein bestimmtes Produkt kaufen wird.
- **Churn-Vorhersage**: Bestimmung, ob ein Kunde wahrscheinlich abwandert oder bleibt, basierend auf seinem Nutzungsverhalten.

**Bildverarbeitung**

- **Einfaches Mustererkennung**: Klassifikation von sehr einfachen Mustern oder Bildern, wie z.B. die Unterscheidung von handgeschriebenen Ziffern (0 oder 1).
- **Objekterkennung**: Identifizierung, ob ein bestimmtes einfaches Objekt in einem Bild vorhanden ist oder nicht.

**Anomalieerkennung**

- **Fehlererkennung in Maschinen**: Klassifizierung von Maschinendaten in "normal" oder "fehlerhaft" basierend auf einfachen Sensorwerten.

**Medizinische Bildanalyse**

- **Erkennung von Tumoren**: In der Vorstufe einer detaillierten Analyse könnte ein Perzeptron verwendet werden, um einfache Abweichungen in medizinischen Bildern (wie z.B. Röntgenbildern) zu erkennen.

**Basis für komplexere Modelle**

- **Grundstein für Multilayer-Perzeptronen (MLPs)**: Obwohl das einfache Perzeptron nur lineare Probleme lösen kann, bildet es die Grundlage für komplexere neuronale Netzwerke, die durch zusätzliche Schichten nicht-lineare Probleme lösen können.

## Perzeptron und Neuron

Ein Neuron in einem künstlichen neuronalen Netzwerk ist ähnlich, aber nicht identisch mit einem Perzeptron: einstellt die einfachste Form eines Neurons in einem neuronalen Netzwerk dar. Ein künstliches Neuron in modernen neuronalen Netzwerken ist eine verallgemeinerte Version eines Perzeptrons.  Wie das Perzeptron nimmt auch das künstliche Neuron Eingaben entgegen, gewichtet sie und gibt eine Ausgabe aus. Der Hauptunterschied liegt in der Aktivierungsfunktion, die viel flexibler gestaltet ist (z.B. ReLU, Sigmoid, Tanh). Diese Funktionen erlauben es dem Neuron, komplexe, nicht-lineare Muster zu erkennen.

# Das Beispiel (Kaufentscheidung)

Abhängig von den Parametern Alter und Einkommen soll ein Mechanismus für die Vorhersage einer Kaufentscheidung implementiert werden.

```java
        // training data {age, income} 
        double[][] trainingData = {
        		 {25, 30000}, 
        		 {35, 50000}, 
        		 {45, 70000}, 
        		 {55, 40000}, 
        		 {65, 60000}
        };
        // labels: 0 = no purchase, 1 = purchase
        int[] labels = {
        		0, 
        		1, 
        		1, 
        		0, 
        		1};
```

Hierbei ist es für das Beispiel vollkommen unerheblich, wieviele Parameter genutzt werden. 

## Implementieren eines Perzeptrons

Innerhalb des Konstruktors wird ein Perzeptron für eine bestimmte Anzahl von Eingabeparametern und eine bestimmte Lernrate erzeugt. Im Modell kontrolliert die Lernrate die Größe der Schritte, die das Modell bei jeder Iteration in Richtung des (vermeintlichen) Optimums macht.

```java
	/**
	 * Constructor
	 * @param inputSize number of parameter at input
	 * @param learningRate learning rate 
	 */
	public Perceptron(int inputSize, double learningRate) {

		this.weights = new double[inputSize];

		// initialisation of weight an bias with randomized values
		for (int i = 0; i < inputSize; i++) {
			// random value between -1 und 1
			this.weights[i] = Math.random() * 2 - 1; 
		}
		
		// random number between -1,0 (inclusive) and 1,0 (exclusive)
		this.bias = Math.random() * 2 - 1;
		this.learningRate = learningRate;
		
		LOG.info("Perceptron created, configured with: weights {}, bias {}, learning rate {}", weights, bias, learningRate);
	}
```

Innerhalb des Konstruktors werden Bias (zusätzlicher Parameter im Perzeptron, der als eine Art Schwellenwert oder Verschiebung fungiert) und die Gewichte der einzelnen Eingangsparameter mit Zufallszahlen initialisiert.

## Prognose 

Für jede Eingabe wird eine Prognose durchgeführt und entweder eine 0 oder eine 1 zurückgegeben. 

```java
	/**
	 * prediction for an input tuple like {0.25 | 0.3}
	 * 
	 * @param inputs one of the inputs
	 * @return 0 or 1 depending on the weighted sum plus bias 
	 */
	public int predict(double[] inputs) {

		LOG.debug("predicting: {}", inputs);
		// weighted sum of the values of an input tuple like {0.25 | 0.3} 
		double sum = 0;
		
		// getting weighted sum for each input
		for (int i = 0; i < inputs.length; i++) {
			sum += inputs[i] * weights[i];
			LOG.debug("weighted sum for {} = {}", inputs[i], sum);
		}
		
		// adding bias to weighted sum
		sum += bias;

		// activation function (Schwellenwert)
		int result = sum > 0 ? 1 : 0;
		LOG.debug("weighted sum plus bias for {} = {} which is bigger than 0 {}, therefore returning {}", inputs, sum, sum > 0, result);
		
		return result;
	}
```

Für die Berechnung werden die Werte eines Tupels der Trainingsdaten  - bspw. {25, 30000} - jeweils mit den Gewichten für dieses Tupel multipliziert und summiert und dieser Summe der Wert des Bias hinzugefügt.

Ist der Wert größer als 0 liefert die Funktion als Vorhersagewert eine 1, ansonsten eine 0 (welches sich hervorragend mit dem gewünschten Ergebnis vergleichen lässt).

## Normalisierung der Daten

Bei aufmerksamer Betrachtung der Trainingsdaten fällt auf, dass bei einem Tupel {25, 30000} multipliziert mit der Gewichtung und einer Addition des Bias nicht viel sinnvolles für die Ermittlung eines Rückgabewertes 0 oder 1 herauskommen kann. Daher ist eine Normalisierung der Daten notwendig. Normalisierung ist ein entscheidender Schritt in der Datenvorverarbeitung, der einen erheblichen Einfluss auf die Leistung und Genauigkeit von Modellen haben kann. Hierbei werden numerische Daten in einen gemeinsamen Maßstab gebracht, ohne die Unterschiede in den Wertebereichen zu verändern.

Bei den meisten Algorithmen des maschinellen Lernens wird davon ausgegangen, dass die Eingabedaten auf einem ähnlichen Maßstab liegen. Wenn dies nicht der Fall ist, können **Merkmale mit größeren Wertebereichen die Modellparameter stärker beeinflussen als Merkmale mit kleineren Wertebereichen**, was zu schlechteren Vorhersagen führt (und genau das ist hier der Fall, die Wertebereiche Alter und Einkommen weichen erheblich voneinander ab).

Für die Normalisierung der Daten existieren eine Reihe von Techniken, hier wird sich auf die einfachste beschränkt.

Bei dieser Art der Normalisierunghandelt es sich nicht um eine "vollständige" oder "standardisierte" Methode wie Min-Max-Skalierung oder Z-Score-Normalisierung ist. In dem Beispiel wird eine einfache Skalierung durchgeführt, bei der die Alters- und Einkommenswerte jeweils durch eine feste Annahme eines maximalen Wertes geteilt werden.

``` java
private double[][] normalizeData(double[][] rawData) {
		
		double[][] normalizedData = new double[rawData.length][2];
	    for (int i = 0; i < rawData.length; i++) {
	    	normalizedData[i][0] = this.normalizeAge(rawData[i][0]);
	    	normalizedData[i][1] = this.normalizeIncome(rawData[i][1]);
	    }
	    
	    return normalizedData;
	}
	
	private double normalizeAge(double age) {
		// assumption: max age 100
		return age / 100.0; 
	}
	
	private double normalizeIncome(double income) {
		// assumption: max income 100.000
		return income / 100000.0; 
	}
```

Das Ergebnis diese Prozesses:

```
 Raw data 
 [[25.0, 30000.0], [35.0, 50000.0], [45.0, 70000.0], [55.0, 40000.0], [65.0, 60000.0]] 
 normalized to 
 [[0.25, 0.3], [0.35, 0.5], [0.45, 0.7], [0.55, 0.4], [0.65, 0.6]]
```

Die Klasse MinMaxNormalization zeigt eine [Standard-Normierung](https://zeenea.com/de/was-ist-datennormalisierung/) exemplarisch.

## Das Training des Modells

Für das Training des Modells ist der Begriff der Backpropagation essentiell. Backpropagation ist der Prozess, durch den Fehler im Netzwerk rückwärts durch die Schichten propagiert werden, um die Gewichte der Verbindungen anzupassen.

Hierzu werden innerhalb der Methode train() für die einzelnen Eingaben die Vorhersage und die entsprechenden Fehler in jedem Trainingszyklus (Epoche) ermittelt.

```java
	/**
	 * training of perceptron
	 * 
	 * @param trainingData training data
	 * @param labels expected values
	 * @param epochs number of learning cycles
	 */
	public void train(double[][] trainingData, int[] labels, int epochs) {
		
		LOG.debug("Training using training data {}, labels (expected values) {} in {} cycles", trainingData, labels, epochs);
		
		// training of perceptron in learning cycles
		for (int epoch = 0; epoch < epochs; epoch++) {
		
			// for each entry of training data
			for (int i = 0; i < trainingData.length; i++) {
				
				LOG.debug("training input {}", trainingData[i]);
				
				// as seen in predict method the output can be 0 or one only
				int prediction = this.predict(trainingData[i]);
				
				// three values of error possible:
				// -1 (if label is 0 and prediction is 1)
				//  0 (if label and prediction match)
				//  1 (if label is 1 and prediction is 0)
				int error = labels[i] - prediction;
				
				LOG.debug("Prediction for {} in epoch {}: {} expected {} => is error: {} (error value: {})", trainingData[i], epoch, prediction, labels[i], (error == -1 || error == 1), error );

				// adjust weight 
				for (int j = 0; j < weights.length; j++) {
					
					LOG.debug("es post weight of trainingdata {}: {} ", weights[j], trainingData[i][j]);
					weights[j] += learningRate * error * trainingData[i][j];
					LOG.debug("ex ante weight of training data {}: {} ", weights[j], trainingData[i][j]);
				}
                
				// adjust bias
				LOG.debug("ex post bias: {}", bias);
				bias += learningRate * error;
				LOG.debug("ex ante bias: {}", bias);
			}
		}
```

Wenn man sich den Code genau betrachtet, dann fällt auf, dass jeder Eingabewert und dessen Prognosefehler **alle** Gewichte adjustiert. Das bezeichnet man als "online learning" oder "stochastic gradient descent" bezeichnet. Dieses Vorgehen ist korrekt und hat einige Vorteile:

1. Schnellere Anpassung: Das Modell passt sich schneller an, da es nach jeder einzelnen Trainingsinstanz aktualisiert wird.
2. Bessere Generalisierung: Häufige kleine Anpassungen können oft zu einer besseren Generalisierung führen, da das Modell nicht zu stark auf einen einzelnen Datenpunkt reagiert.
3. Möglichkeit zum Escape lokaler Minima: Die häufigen Aktualisierungen können dem Modell helfen, lokale Minima zu überwinden.

Es gibt alternative Ansätze, wie zum Beispiel "batch learning", bei dem die Gewichte erst nach dem Durchlaufen aller Trainingsdaten aktualisiert werden. Am häufigsten wird das [Mini-Batch-Learning](https://artemoppermann.com/de/variationen-des-gradientenabstiegs-im-backpropagation-algorithmus/) als Mittelweg zwischen Online Learning und Batch Learning darstellt.

- Mini-Batch Learning aktualisiert die Modellparameter nach der Verarbeitung einer kleinen Gruppe (Batch) von Trainingsbeispielen, nicht nach jedem einzelnen Beispiel (wie beim Online Learning) und nicht erst nach allen Beispielen (wie beim Batch Learning).

- Prozess:

  1. Teile die Trainingsdaten in kleine Gruppen (Mini-Batches) auf.

  1. Verarbeite jede Gruppe und akkumuliere die Fehler innerhalb dieser Gruppe.

  1. Aktualisiere die Gewichte und den Bias nach jedem Mini-Batch.


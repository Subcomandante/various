package de.brockhaus.ai.lrrh;

public class Neuron {
    private double[] weights;
    private double bias;

    public Neuron(int inputSize) {
        this.weights = Utils.createRandomArray(inputSize);
        this.bias = Math.random() - 0.5;
    }

    public double activate(double[] inputs) {
        double sum = bias;
        for (int i = 0; i < inputs.length; i++) {
            sum += inputs[i] * weights[i];
        }
        return Utils.sigmoid(sum);
    }

    public double[] getWeights() {
        return weights;
    }

    public double getBias() {
        return bias;
    }

    public void adjustWeights(double[] deltas, double learningRate) {
        for (int i = 0; i < weights.length; i++) {
            weights[i] += learningRate * deltas[i];
        }
    }

    public void adjustBias(double delta, double learningRate) {
        bias += learningRate * delta;
    }
}

package de.brockhaus.ai.lrrh;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * The neuronal network as a whole, composed out of several layers
 * 
 * mbohnen, Brockhaus Consulting GmbH
 */
public class NeuronalNetwork {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger();
	
    private Layer inputLayer;
    
    private Layer hiddenLayer;
    
    private Layer outputLayer;
    
    private double learningRate;

    /**
     * Constructor
     * 
     * @param inputSize
     * @param hiddenSize
     * @param outputSize
     * @param learningRate
     */
    public NeuronalNetwork(int inputSize, int hiddenSize, int outputSize, double learningRate) {
        this.inputLayer = new Layer(inputSize, inputSize);
        this.hiddenLayer = new Layer(hiddenSize, inputSize);
        this.outputLayer = new Layer(outputSize, hiddenSize);
        this.learningRate = learningRate;
    }

    /**
     * 
     * @param inputs
     * @return
     */
    public double[] forward(double[] inputs) {
    	
//    	double[] inLayerOutputs = inputLayer.compute(inputs);
        
    	// what we are getting from the hidden layer upon an input
    	double[] hiddenOutputs = hiddenLayer.compute(inputs);
        
        return outputLayer.compute(hiddenOutputs);
    }

    /**
     * learning something in several epochs
     * 
     * @param inputs
     * @param targets
     * @param epochs
     */
    public void train(double[][] inputs, double[][] targets, int epochs) {
    	
    	// for every epoch
        for (int epoch = 0; epoch < epochs; epoch++) {
        	
            double squaredErrorSum = 0;
            
            int inputCount = inputs.length;

            // for no of input we provided
            for (int i = 0; i < inputCount; i++) {
                
            	// Forward pass
                double[] hiddenOutputs = hiddenLayer.compute(inputs[i]);
                
                // pass further on zo get the output
                double[] predictedOutputs = outputLayer.compute(hiddenOutputs);

                // Compute output layer errors
                double[] outputErrors = new double[predictedOutputs.length];
                double[] dPredictedOutputs = new double[predictedOutputs.length];
                
                for (int j = 0; j < targets[0].length; j++) {
                	// get the difference between real and expected outputs
                    outputErrors[j] = targets[i][j] - predictedOutputs[j];
                    
                    dPredictedOutputs[j] = outputErrors[j] * Utils.sigmoidDerivative(predictedOutputs[j]);

                    // sum of squared errors for RMSE
                    squaredErrorSum += Math.pow(outputErrors[j], 2);
                }

                // Compute hidden layer errors
                double[] hiddenErrors = new double[hiddenLayer.getNeurons().length];
                double[] dHiddenLayer = new double[hiddenLayer.getNeurons().length];
                
                for (int j = 0; j < hiddenLayer.getNeurons().length; j++) {
                    hiddenErrors[j] = 0;
                    
                    for (int k = 0; k < outputLayer.getNeurons().length; k++) {
                        hiddenErrors[j] += dPredictedOutputs[k] * outputLayer.getNeurons()[k].getWeights()[j];
                    }
                    
                    dHiddenLayer[j] = hiddenErrors[j] * Utils.sigmoidDerivative(hiddenOutputs[j]);
                }

                // Adjust weights and biases for output layer
                for (int j = 0; j < outputLayer.getNeurons().length; j++) {
                	
                    outputLayer.getNeurons()[j].adjustBias(dPredictedOutputs[j], learningRate);
                    double[] hiddenOutputDeltas = new double[hiddenOutputs.length];
                    
                    for (int k = 0; k < hiddenOutputs.length; k++) {
                        hiddenOutputDeltas[k] = dPredictedOutputs[j] * hiddenOutputs[k];
                    }
                    outputLayer.getNeurons()[j].adjustWeights(hiddenOutputDeltas, learningRate);
                }

                // Adjust weights and biases for hidden layer
                for (int j = 0; j < hiddenLayer.getNeurons().length; j++) {
                    
                	hiddenLayer.getNeurons()[j].adjustBias(dHiddenLayer[j], learningRate);
                    
                	double[] inputDeltas = new double[inputs[i].length];
                   
                	for (int k = 0; k < inputs[i].length; k++) {
                        inputDeltas[k] = dHiddenLayer[j] * inputs[i][k];
                    }
                	
                    hiddenLayer.getNeurons()[j].adjustWeights(inputDeltas, learningRate);
                }
            }

            // RMSE calculation and output
            double rmse = Math.sqrt(squaredErrorSum / inputCount);
            System.out.println("Epoch " + (epoch + 1) + " RMSE: " + rmse);
        }
    }

    
    public void saveModel(String filePath) {
    	
        try (FileOutputStream fos = new FileOutputStream(filePath);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            // Speichere die Gewichte und Biases der Schichten
            oos.writeObject(hiddenLayer.getNeurons());
            oos.writeObject(outputLayer.getNeurons());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadModel(String filePath) {
    	
        try (FileInputStream fis = new FileInputStream(filePath);
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            // Lade die Gewichte und Biases der Schichten
            Neuron[] hiddenNeurons = (Neuron[]) ois.readObject();
            Neuron[] outputNeurons = (Neuron[]) ois.readObject();

            this.hiddenLayer = new Layer(hiddenNeurons);
            this.outputLayer = new Layer(outputNeurons);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

package de.brockhaus.ai.lrrh;

import java.io.Serializable;

public class Layer implements Serializable {
	
	// the neurons this layer is made of
    private Neuron[] neurons;

    public Layer(int numNeurons, int inputSize) {
        this.neurons = new Neuron[numNeurons];
        for (int i = 0; i < numNeurons; i++) {
            neurons[i] = new Neuron(inputSize);
        }
    }
    
    public Layer(Neuron[] neurons) {
        this.neurons = neurons;
    }

    public double[] compute(double[] inputs) {
    	
    	// array 4 individual results of all neurons
        double[] outputs = new double[neurons.length];
        
        for (int i = 0; i < neurons.length; i++) {
            outputs[i] = neurons[i].activate(inputs);
        }
        
        return outputs;
    }

    public Neuron[] getNeurons() {
        return neurons;
    }
}



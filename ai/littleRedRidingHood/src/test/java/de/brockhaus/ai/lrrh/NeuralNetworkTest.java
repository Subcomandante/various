package de.brockhaus.ai.lrrh;

import java.util.Arrays;

import org.junit.Test;

public class NeuralNetworkTest {

	@Test
    public static void main(String[] args) {
        double[][] inputs = {
            {1, 1, 1, 1}, // Rotkäppchen
            {1, 1, 1, 0}, // Geschichte ohne Korb
            {1, 1, 0, 1}, // Geschichte ohne Wolf
            {1, 0, 1, 1}, // Geschichte ohne Großmutter
            {0, 1, 1, 1}, // Geschichte ohne Wald
            {0, 0, 0, 0}  // Keine Elemente von Rotkäppchen
        };

        double[][] targets = {
            {1}, // Rotkäppchen
            {0}, // Nicht Rotkäppchen
            {0}, // Nicht Rotkäppchen
            {0}, // Nicht Rotkäppchen
            {0}, // Nicht Rotkäppchen
            {0}  // Nicht Rotkäppchen
        };

        NeuronalNetwork nn = new NeuronalNetwork(4, 4, 1, 0.1);
        nn.train(inputs, targets, 5000);

        double[][] newStories = {
            {1, 1, 1, 1}, // Rotkäppchen
            {0, 0, 1, 0}  // Geschichte mit einem Wolf
        };

        for (double[] story : newStories) {
            double[] prediction = nn.forward(story);
            System.out.println("Geschichte: " + Arrays.toString(story) + " -> Rotkäppchen: " + (prediction[0] > 0.5));
        }
    }
}

package de.brockhaus.encyption;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Symmetric-key block cipher plays an important role in data encryption. 
 * It means that the same key is used for both encryption and decryption. 
 * The Advanced Encryption Standard (AES) is a widely used symmetric-key encryption algorithm.
 * 
 * https://www.baeldung.com/java-aes-encryption-decryption
 * 
 * @author mbohnen
 *
 */
public class SimpleEncryption {

	@Test
	public void doEncyptAndDecrypt() throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException {
		String needsEncryption = "this is to be encoded";
		String password = "topsecret";
		String salt = "9923897";
		IvParameterSpec ivParameterSpec = this.generateIv();
		SecretKey key = this.getKeyFromPassword(password, salt);
		String cipherText = this.encryptPasswordBased(needsEncryption, key, ivParameterSpec);
		String decryptedCipherText = this.decryptPasswordBased(cipherText, key, ivParameterSpec);

		Assert.assertEquals(needsEncryption, decryptedCipherText);
	}

	/**
	 * generating initializing vector (IV)
	 * 
	 * IV is a pseudo-random value and has the same size as the block that is encrypted. 
	 * We can use the SecureRandom class to generate a random IV.
	 * 
	 * @return
	 */
	private IvParameterSpec generateIv() {
		byte[] iv = new byte[16];
		new SecureRandom().nextBytes(iv);

		return new IvParameterSpec(iv);
	}

	/**
	 * getting the symmetric secret key
	 * 
	 * AES secret key can be derived from a given password using a password-based key derivation function like PBKDF2. 
	 * We also need a salt value for turning a password into a secret key. The salt is also a random value.
	 * 
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	private SecretKey getKeyFromPassword(String password, String salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		// SecretKeyFactory class with the PBKDF2WithHmacSHA256 algorithm for generating a key from a given password.
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		
		// generating the AES key from a given password with 65,536 iterations and a key length of 256 bits
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256);
		SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
		
		return secret;
	}

	private String encryptPasswordBased(String plainText, SecretKey key, IvParameterSpec iv)
			throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException,
			InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		
		return Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes()));
	}

	public String decryptPasswordBased(String cipherText, SecretKey key, IvParameterSpec iv)
			throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException,
			InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, key, iv);
		
		return new String(cipher.doFinal(Base64.getDecoder().decode(cipherText)));
	}

}

package de.brockhaus.ui.javafx.chart;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class TestTree extends Application {

    private final Node rootIcon = new ImageView(
        new Image(getClass().getResourceAsStream("folder_16.png"))
    );

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Tree View Sample");        
        
        TreeItem<String> rootItem = new TreeItem<String> ("Inbox", rootIcon);
        rootItem.setExpanded(true);
        
        for (int i = 1; i < 6; i++) {
            TreeItem<String> item = new TreeItem<String> ("Message" + i);            
            rootItem.getChildren().add(item);
        } 
        
        TreeView<String> tree = new TreeView<String> (rootItem);   
        tree.getSelectionModel().selectedItemProperty().addListener(new TreeItemSelectionListener());
        
        StackPane root = new StackPane();
        root.getChildren().add(tree);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
    
    
    private class TreeItemSelectionListener implements ChangeListener<TreeItem<String>> {

		public void changed(
				ObservableValue<? extends TreeItem<String>> observable,
				TreeItem<String> oldValue, TreeItem<String> newValue) {
			TreeItem<String> selectedItem = newValue;
            System.out.println("Selected Text : " + selectedItem.getValue());
            // do what ever you want
			
		}
    }
}



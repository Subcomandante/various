package de.brockhaus.ui.javafx.chart;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class SensorSplitPane extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Trendserver");
		SplitPane splitPane = new SplitPane();
		splitPane.setOrientation(Orientation.HORIZONTAL);
		
		Button b1 = new Button("OK");
		Button b2 = new Button("Cancel");
		
		splitPane.getItems().add(0, b1);
		splitPane.getItems().add(1, b2);

		
		Scene scene  = new Scene(splitPane,800,600);
		
        
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}

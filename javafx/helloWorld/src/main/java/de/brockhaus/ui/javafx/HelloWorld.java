package de.brockhaus.ui.javafx;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * see this: http://javabeginners.de/Frameworks/JavaFX/Eclipse_fuer_JavaFX_einrichten.php
 * 
 * Project: javafx.helloWorld
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Jul 3, 2015
 *
 */
@SuppressWarnings("restriction")
public class HelloWorld extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }
    
    // the start() method is the main entry point for all JavaFX applications
    //  the Stage class is the top-level JavaFX container
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");
        
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        // root node is a StackPane object, which is a resizable layout node;
        // root node's size tracks the scene's size and changes when the stage is resized by a user.
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        
        // Scene class is the container for all content
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
}

package de.brockhaus.sampleApp;


import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import de.brockhaus.process.ProcessException;

public class MyProcessTest {

	private WeldContainer container;
	private MyProcess process;

	@Test
	public void testDoProcess_NoTimeOut() throws ProcessException {
		
		process.setMaxWaitTimeMs(1500);
		process.doProcess();
	}
	
	@Test(expectedExceptions = ProcessException.class)
	public void testDoProcess_TimeOut() throws ProcessException {
		
		process.setMaxWaitTimeMs(800);
		process.doProcess();
	}
	
	@BeforeClass
	public void setUp() {
	    Weld weld = new Weld();
		container = weld.initialize();
		process = container.select(MyProcess.class).get();
		FooListener listener = container.select(FooListener.class).get();
	}
}

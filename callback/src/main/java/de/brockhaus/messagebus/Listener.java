package de.brockhaus.messagebus;

/**
 * 
 * @author mbohnen
 *
 * @param <T>
 */
public interface Listener<T> {
	
	public  void  onMessage(T event);

}

package de.brockhaus.messagebus;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.callback.Event;
import jakarta.inject.Singleton;



/**
 * 
 * @author mbohnen
 *
 */
@Singleton
public class MessageBus {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(MessageBus.class);
	
	private ArrayList<Listener<?>> listeners = new ArrayList<>();
	
	public MessageBus() {
		LOG.info("MessageBus created");
	}
	
	public void notify(Event event) {
		
		LOG.info("notifying {} listeners", listeners.size());
		
		for (Listener listener : listeners) {
			listener.onMessage(event);
		}
	}
	
	public void register(Listener listener) {
		
		LOG.info("registering {} listener", listener.getClass().getSimpleName());
		
		listeners.add(listener);
	}
}

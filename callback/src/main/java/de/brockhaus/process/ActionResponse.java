package de.brockhaus.process;

/**
 * The generic response an action delivers
 * 
 * @author mbohnen
 *
 * @param <T>
 */
public class ActionResponse<T>  {
	
	private T payload;

	public ActionResponse(T payload) {
		super();
		this.payload = payload;
	}

	public T getPayload() {
		return payload;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}
}

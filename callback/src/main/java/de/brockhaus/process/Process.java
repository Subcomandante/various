package de.brockhaus.process;

import java.util.concurrent.TimeUnit;

public interface Process {

	public void doProcess() throws ProcessException;
	
}

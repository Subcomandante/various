package de.brockhaus.process;

public interface ProcessAction {

	<T> ActionResponse<T> doAction();
}

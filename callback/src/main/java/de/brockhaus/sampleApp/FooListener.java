package de.brockhaus.sampleApp;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.callback.Event;
import de.brockhaus.messagebus.Listener;
import de.brockhaus.messagebus.MessageBus;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;



/**
 * Just a listener which is called by some action
 * 
 * @author mbohnen
 *
 */
@Named
public class FooListener implements Listener<Event<String>>{
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(FooListener.class);
	
	@Inject
	private MessageBus msgBus;
	
	public FooListener() {
		LOG.info("FooListener created");
	}
	
	@Override
	public void onMessage(Event<String> event) {
		
		this.doStuff(event);		
	}

	private void doStuff(Event<String> msg) {
		
		msg.setPayload("FooListener processed: " + ((String) msg.getPayload()).toUpperCase());
		
		if(msg.getToBeNotified() != null) {
			
			try {
				// pretending being busy
				Thread.currentThread().sleep(1000);
				msg.setFiredBy(this.getClass());
				msg.getToBeNotified().onCallback(msg);
				
			} catch (InterruptedException e) {
				LOG.error(e);
			}
		}
	}
	
	@PostConstruct
	public void init() {
		LOG.info("FooListener registered himself at message bus");
		msgBus.register(this);
	}
}

package de.brockhaus.sampleApp;

import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.callback.CallbackListener;
import de.brockhaus.callback.Event;
import de.brockhaus.messagebus.MessageBus;
import de.brockhaus.process.ActionResponse;
import de.brockhaus.process.ProcessAction;
import jakarta.inject.Inject;

/**
 * The action to sned a notification (an event) to someone via the message bus.
 * 
 * @author mbohnen
 *
 */
public class SendEventAction implements ProcessAction, CallbackListener, Callable<ActionResponse<String>> {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(SendEventAction.class);

	@Inject
	private MessageBus msgBus;
	
	// are we finished (if being called back)
	private boolean isDone;
	
	private Event<String> received;
	
	boolean processSuccess;
	
	public SendEventAction() {
		
	}
	
	@Override
	public ActionResponse<String> doAction() {
		
		ActionResponse<String> resp = null;
		
		Event<String> event = new Event<>(this);
		event.setPayload("It is done, master");
		event.setToBeNotified(this);

		msgBus.notify(event);	
				
		while(! isDone) {
			// looping as long as callback is received
		}
		
		resp = new ActionResponse<String>(received.getPayload());	
		
		return resp;
	}

	
	public boolean isProcessSuccess() {
		return processSuccess;
	}

	public void setProcessSuccess(boolean processSuccess) {
		this.processSuccess = processSuccess;
	}


	// called by one of the listeners
	@Override
	public <T> void onCallback(Event<T> msg) {
		LOG.info("Callback received");
		
		if(msg.getFiredBy() == FooListener.class) {
			this.received = (Event<String>) msg;
			this.isDone = true;
			
		} else {
			LOG.info("Received event but not interested in");
		}				
	}

	@Override
	public ActionResponse<String> call() throws Exception {		
		return this.doAction();
	}
}

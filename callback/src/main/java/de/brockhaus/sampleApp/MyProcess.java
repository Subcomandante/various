package de.brockhaus.sampleApp;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.process.ActionResponse;
import de.brockhaus.process.Process;
import de.brockhaus.process.ProcessException;
import jakarta.inject.Inject;

/**
 * This simple process has one single action to perform: SendEventAction.
 * 
 * @author mbohnen
 *
 */
public class MyProcess implements Process {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(MyProcess.class);

	private int maxWaitTimeMs = 1000;

	@Inject
	private SendEventAction sendAction;

	/**
	 * synchonous invocation
	 * 
	 * @throws ProcessException
	 */
	public void doProcess() throws ProcessException {

		String result = this.sendEvent(maxWaitTimeMs, TimeUnit.MILLISECONDS);

		LOG.info("Process terminated with result: " + result);
	}

	/**
	 * asynchronous invocation of action
	 * 
	 * @param time
	 * @param timeUnit
	 * @return
	 * @throws ProcessException
	 */
	public String sendEvent(long time, TimeUnit timeUnit) throws ProcessException {

		// passing some info to the action
		sendAction.setProcessSuccess(true);

		String result;

		try {

			ExecutorService executorService = Executors.newSingleThreadExecutor();

			FutureTask<ActionResponse<String>> task = new FutureTask<ActionResponse<String>>(sendAction);

			// let's go
			executorService.execute(task);

			ActionResponse<String> got = task.get(time, timeUnit);

			if (got == null) {
				throw new ProcessException("result is null");
			}

			result = got.getPayload();

		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			LOG.error(e);

			throw new ProcessException(e);
		}

		LOG.info(result);

		return result;
	}

	public void setMaxWaitTimeMs(int maxWaitTimeMs) {
		this.maxWaitTimeMs = maxWaitTimeMs;
	}
	
	
}

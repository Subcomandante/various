package de.brockhaus.callback;

/**
 * 
 * 
 * @author mbohnen
 *
 * @param <T>
 */
public class Event<T> {
	
	// who emitted the event
	private Class firedBy;
	
	// the one who might listen for a callback
	private CallbackListener toBeNotified;
	
	// what is the payload of this event
	private T payload;

	// constructor
	public Event(CallbackListener toBeNotified) {
		super();
		this.toBeNotified = toBeNotified;
	}

	public CallbackListener getToBeNotified() {
		return toBeNotified;
	}

	
	public Class getFiredBy() {
		return firedBy;
	}

	public void setFiredBy(Class firedBy) {
		this.firedBy = firedBy;
	}

	public void setToBeNotified(CallbackListener toBeNotified) {
		this.toBeNotified = toBeNotified;
	}

	public T getPayload() {
		return payload;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}
	
}

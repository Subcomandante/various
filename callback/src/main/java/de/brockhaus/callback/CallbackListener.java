package de.brockhaus.callback;

/**
 * being able to receive callbacks on sender side
 * @author mbohnen
 *
 */
public interface CallbackListener {

	<T> void onCallback(Event<T> msg);
}

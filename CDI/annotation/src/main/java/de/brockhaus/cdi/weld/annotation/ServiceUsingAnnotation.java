package de.brockhaus.cdi.weld.annotation;

import javax.inject.Inject;

/**
 * 
 * Project: weld
 * 
 * We will inject a DataSource into the DAO ...
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Nov 4, 2014
 *
 */
public class ServiceUsingAnnotation
{
	@Inject 
	@Real // Qualifier (annotation) specifying, which one to use
	public DataSource ds;
	
	public void doFoo()
    {
		System.out.println(ds.getConnection());    
    }
}

package de.brockhaus.cdi.weld.annotation;

/**
 * 
 * @author mbohnen
 *
 */
@Real // making use of annotation to qualify this one
public class RealDataSource implements DataSource {

	public String getConnection() {

		return "This is REAL connection";
	}

}

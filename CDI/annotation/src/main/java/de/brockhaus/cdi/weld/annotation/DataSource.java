package de.brockhaus.cdi.weld.annotation;

/**
 * Just the interface
 * 
 * Project: CDIUsingAnnotations
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, May 16, 2015
 *
 */
public interface DataSource {
	
	public String getConnection();
	
}

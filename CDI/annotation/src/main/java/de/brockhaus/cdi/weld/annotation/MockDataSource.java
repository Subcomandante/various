package de.brockhaus.cdi.weld.annotation;

/**
 * 
 * Project: weld
 * 
 * Some pseudo DataSource
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Nov 4, 2014
 *
 */
@Mock // making use of annotation to qualify this one
public class MockDataSource implements DataSource
{
	public String getConnection()
	{
		return "This is a MOCK - connection";
	}
}

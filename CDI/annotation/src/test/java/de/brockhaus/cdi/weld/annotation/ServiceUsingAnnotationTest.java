package de.brockhaus.cdi.weld.annotation;

import org.jboss.weld.environment.se.Weld;
import org.testng.annotations.Test;

/**
 * 
 * @author mbohnen
 *
 */
public class ServiceUsingAnnotationTest {

	@Test
	public void testDoFoo() {
		ServiceUsingAnnotation app = new Weld().initialize().instance().select(ServiceUsingAnnotation.class).get();
		app.doFoo();
	}
}

package de.brockhaus.cdi.observer;

import org.jboss.weld.environment.se.Weld;

public class Client {
	
	public static void main(String[] args) {
		HelloMessenger messenger = new Weld().initialize().instance().select(HelloMessenger.class).get();
		messenger.sayHello();
	}
}

package de.brockhaus.cdi.observer;

import javax.enterprise.event.Event;
import javax.inject.Inject;

/**
 * 
 * Project: observerCDI
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de 
 * @author mbohnen, Aug 12, 2015
 *
 */
public class HelloMessenger {

    @Inject 
    @EventNotification
    private Event<HelloEvent> events;

    public void sayHello(){    	
    	// the event will be fired ...
        events.fire(new HelloEvent("from bean "+  this.getClass().getSimpleName() + " at: " + System.currentTimeMillis()));
    }
} 

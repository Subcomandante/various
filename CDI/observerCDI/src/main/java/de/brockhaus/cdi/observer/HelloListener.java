package de.brockhaus.cdi.observer;

import javax.enterprise.event.Observes;

/**
 * The listener watching for certain events ...
 * 
 * Project: observerCDI
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Aug 12, 2015
 *
 */
public class HelloListener {

    public void listenToHello(@Observes HelloEvent helloEvent){

        System.out.println("HelloEvent received by Listener: " + helloEvent);
    }
}

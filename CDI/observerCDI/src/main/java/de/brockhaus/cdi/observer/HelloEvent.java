package de.brockhaus.cdi.observer;

import java.io.Serializable;

/**
 * The event itself ...
 * 
 * Project: observerCDI
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Aug 12, 2015
 *
 */
public class HelloEvent implements Serializable {

    private String message;

    public HelloEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

	@Override
	public String toString() {
		
		return this.getClass().getSimpleName() + "->" + this.getMessage();
	}    
}

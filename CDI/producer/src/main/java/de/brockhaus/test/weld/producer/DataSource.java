package de.brockhaus.test.weld.producer;

public interface DataSource {
	
	public String getConnection();
	
}

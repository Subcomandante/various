package de.brockhaus.test.weld.producer;

import javax.enterprise.inject.Default;

/**
 * 
 * @author mbohnen
 *
 */
@Default
public class RealDataSource implements DataSource {

	public String getConnection() {

		return "This should be a REAL connection";
	}

}

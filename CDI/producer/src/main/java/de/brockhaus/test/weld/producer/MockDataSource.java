package de.brockhaus.test.weld.producer;

import javax.enterprise.inject.Alternative;

/**
 * 
 * Project: weld
 * 
 * Some pseudo DataSource
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Nov 4, 2014
 *
 */
@Alternative
public class MockDataSource implements DataSource
{
	public String getConnection()
	{
		return "This is a MOCK - connection";
	}
}

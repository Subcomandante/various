package de.brockhaus.test.weld.producer;

import javax.inject.Inject;

/**
 * 
 * Project: weld
 * 
 * We will inject a DataSource into the DAO ... see beans.xml for alternatives
 * 
 * https://code.google.com/p/jee6-cdi/wiki/DependencyInjectionAnIntroductoryTutorial#Code_Listing:_TransportFactory_._createTransport_using_@Produces
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Nov 4, 2014
 *
 */
public class ServiceUsingProducer
{
	@Inject
	public DataSource ds;
	
	public void doFoo()
    {
		System.out.println(ds.getConnection());    
    }

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}
}

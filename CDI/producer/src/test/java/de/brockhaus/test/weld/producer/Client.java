package de.brockhaus.test.weld.producer;

import org.jboss.weld.environment.se.Weld;

public class Client {

	public static void main(String[] args) {
		ServiceUsingProducer app = new Weld().initialize().instance().select(ServiceUsingProducer.class).get();
		app.doFoo();
	}
}

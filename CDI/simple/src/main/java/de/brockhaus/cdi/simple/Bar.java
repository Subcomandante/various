package de.brockhaus.cdi.simple;

import javax.inject.Inject;

public class Bar {
	
	@Inject 
	private Foo foo;
	
	public Bar() {
		
	}
	
	public void doTheThings() {
		foo.doStuff();
	}

}

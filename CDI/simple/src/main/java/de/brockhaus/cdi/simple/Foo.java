package de.brockhaus.cdi.simple;

import javax.annotation.PostConstruct;
import javax.inject.Named;

@Named
public class Foo {
	
	public Foo() {
		//lazy
	}
		
	public void doStuff() {
		System.out.println(this.getClass().getSimpleName() + " is doing stuff");
	}
	
	@PostConstruct
	public void init() {
		System.out.println("I'm here");
	}

}

package de.brockhaus.cdi.simple;

import org.jboss.weld.environment.se.Weld;

public class Client {

	public static void main(String[] args) {
		Bar bar = new Weld().initialize().instance().select(Bar.class).get();
		bar.doTheThings();
	}
}

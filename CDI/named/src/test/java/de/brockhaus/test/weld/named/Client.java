package de.brockhaus.test.weld.named;

import org.jboss.weld.environment.se.Weld;

public class Client {

	public static void main(String[] args) {
		ServiceUsingNamed app = new Weld().initialize().instance().select(ServiceUsingNamed.class).get();
		app.doFoo();
	}
}

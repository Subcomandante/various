package de.brockhaus.test.weld.named;

import javax.inject.Named;

/**
 * 
 * @author mbohnen
 *
 */
@Named("Real")
public class RealDataSource implements DataSource {

	public String getConnection() {
		return "This should be a REAL connection";
	}

}

package de.brockhaus.test.weld.named;

import javax.inject.Named;

/**
 * 
 * Project: weld
 * 
 * Some pseudo DataSource
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Nov 4, 2014
 *
 */
@Named("Mock")
public class MockDataSource implements DataSource
{
	public String getConnection()
	{
		return "This is a MOCK - connection";
	}
}

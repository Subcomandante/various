package de.brockhaus.test.weld.named;

public interface DataSource {
	
	public String getConnection();
	
}

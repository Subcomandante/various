/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.service;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import de.brockhaus.mongodb.javaDriver.entity.Equipment;
import de.brockhaus.mongodb.javaDriver.entity.OPCDataType;
import de.brockhaus.mongodb.javaDriver.entity.Sensor;

/**
 * Project: javaDriver
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 13, 2014
 *
 */
public class EquipmentServiceTest {
	
	private Equipment e;
	private EquipmentService service;
	private String oid;
	
	public static void main(String[] args) {
		EquipmentServiceTest test = new EquipmentServiceTest();
		test.init();
		test.setUpData();
		
//		test.testGetEquipmentAsJSON();
		
		test.testCreateEquipment();
//		test.testFindEquipmentByKey("ABC");
//		test.testFindEquipmentBy_Id();
//		test.testFindAll();
		
//		test.testUpdateEquipment_NoId();
//		test.testUpdateEquipment_NoDocument();
//		test.testUpdateDocument();
		
//		test.testDeleteEquipment();
		
	}

	private void testDeleteEquipment() {
		List<Equipment> hits = this.service.findAll();
		for (Equipment equipment : hits) {
			service.deleteEquipment(equipment);
		}
	}

	private void testUpdateDocument() {
		List<Equipment> hits = this.service.findAll();
		Equipment e = hits.get(0);
		if (null != e) {
			System.out.println(e.get_id());
			e.setDesc("updated");
			service.updateEquipment(e);
		}	
	}

	private void testUpdateEquipment_NoDocument() {
		Equipment e = new Equipment();
		e.set_id("foo");
		service.updateEquipment(e);		
	}

	private void testUpdateEquipment_NoId() {
		Equipment e = new Equipment();
		service.updateEquipment(e);	
	}

	private void testFindAll() {
		List<Equipment> hits = service.findAll();
		for (Equipment equipment : hits) {
			System.out.println(equipment.get_id() + ": " + equipment.getDesc());
			for (Sensor sensor : e.getSensors()) {
				System.out.println(sensor.getDesc() + sensor.getDataType());
			}
		}	
	}

	private void testFindEquipmentBy_Id() {
		if (oid != null) {
			e = service.findEquipmentBy_Id(oid);
			System.out.println(e.getDesc());
		}	
	}

	private void testFindEquipmentByKey(String key) {
	
		List<Equipment> hits = service.findEquipmentByKey(key);
		for (Equipment equipment : hits) {
			System.out.println(e.get_id());
			
		}
	}

	private void testCreateEquipment() {
		e = service.createEquipment(e);
		oid = e.get_id();
		
		System.out.println(oid);
		
	}

	private void init() {
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring-context.xml");
		service = (EquipmentService) context.getBean("equipmentService");	
	}

	public void testGetEquipmentAsJSON() {
		System.out.println(service.getEquipmentAsJSONString(e));
	}
		
	private void setUpData() {
		
		e = new Equipment();
		e.setKey("ABC");
		e.setDesc("some fancy equipment");
		
		Sensor s1 = new Sensor();
		s1.setDataType(OPCDataType.BOOLEAN);
		s1.setDesc("sensor.1");
		s1.setEquipment(e);
		e.getSensors().add(s1);
		
		Sensor s2 = new Sensor();
		s2.setDataType(OPCDataType.FLOAT);
		s2.setDesc("sensor.2");
		s2.setEquipment(e);
		e.getSensors().add(s2);
	}
}

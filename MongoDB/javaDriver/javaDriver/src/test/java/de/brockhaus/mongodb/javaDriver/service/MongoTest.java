/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.service;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

/**
 * Project: javaDriver
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 12, 2014
 *
 */
public class MongoTest {
	
	private MongoClient mongo;
	
	public static void main(String[] args) throws UnknownHostException {
		MongoTest test = new MongoTest();
		test.doConnect();
		test.listDataBases();
		test.getDatabase("person");
		test.listDataBases();
	}
	
	private void doConnect() throws UnknownHostException {
		mongo = new MongoClient( "localhost" , 27017 );
		
	}
	
	private void listDataBases() {
		List<String> dbs = mongo.getDatabaseNames();
		for(String db : dbs){
			System.out.println(db);
		}
	}
	
	private void getDatabase(String name) {
		DB db = mongo.getDB(name);
		DBCollection col = db.getCollection("person");
		BasicDBObject document = new BasicDBObject();
		document.put("firstname", "Matthias");
		document.put("lastname", "Bohnen");
		document.put("createdDate", new Date());
		col.insert(document);
		
	}

}

package de.brockhaus.mongodb.javaDriver.service;

import java.util.List;

import de.brockhaus.mongodb.javaDriver.dao.EquipmentDAO;
import de.brockhaus.mongodb.javaDriver.entity.Equipment;
import de.brockhaus.mongodb.javaDriver.util.JSONBuilderParserUtil;

/**
 * Project: javaDriver
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 13, 2014
 *
 */
public class EquipmentServiceBean implements EquipmentService {
	
	private JSONBuilderParserUtil jsonUtil;
	private EquipmentDAO dao;
	

	public Equipment createEquipment(Equipment equipment) {
		return this.dao.createEquipment(equipment);
	}

	public String getEquipmentAsJSONString(Equipment equipment) {		
		return jsonUtil.toJSON(equipment);
	}


	public JSONBuilderParserUtil getJsonUtil() {
		return jsonUtil;
	}

	public void setJsonUtil(JSONBuilderParserUtil jsonUtil) {
		this.jsonUtil = jsonUtil;
	}

	public EquipmentDAO getDao() {
		return dao;
	}

	public void setDao(EquipmentDAO dao) {
		this.dao = dao;
	}

	@Override
	public List<Equipment> findEquipmentByKey(String key) {
		return dao.findEquipmentByKey(key);
		
	}

	@Override
	public Equipment findEquipmentBy_Id(String _id) {
		return this.dao.findEquipmentBy_Id(_id);
	}

	@Override
	public List<Equipment> findAll() {
		return this.dao.findAll();
		
	}

	@Override
	public void updateEquipment(Equipment equipment) {
		this.dao.updateEquipment(equipment);
		
	}


	@Override
	public void deleteEquipment(Equipment equipment) {
		this.dao.deleteEquipment(equipment);
		
	}
}

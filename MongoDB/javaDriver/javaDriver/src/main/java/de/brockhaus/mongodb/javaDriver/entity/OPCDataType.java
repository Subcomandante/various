/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.entity;

import java.io.Serializable;

/**
 * Project: javaDriver
 * 
 * Copyright (C) by Brockhaus Group, Wiesbaden www.brockhaus-gruppe.de
 * 
 * @author mbohnen, Jun 12, 2014
 * 
 */
public enum OPCDataType {

	BOOLEAN(0), FLOAT(1), INTEGER(2), STRING(3);

	private final int value;

	private OPCDataType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public static String getStringValueFromInt(int i) {
		for (OPCDataType dataType : OPCDataType.values()) {
	         if (dataType.getValue() == i) {
	             return dataType.toString();
	         }
	     }
	     // throw an IllegalArgumentException or return null
	     throw new IllegalArgumentException("the given number doesn't match any Status.");
	 }

}

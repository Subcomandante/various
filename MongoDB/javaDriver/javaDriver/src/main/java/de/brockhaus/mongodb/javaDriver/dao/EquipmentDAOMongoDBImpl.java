/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.dao;

import java.awt.image.DataBufferDouble;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

import de.brockhaus.mongodb.javaDriver.entity.Equipment;
import de.brockhaus.mongodb.javaDriver.entity.Sensor;
import de.brockhaus.mongodb.javaDriver.util.JSONBuilderParserUtil;

/**
 * Project: javaDriver
 * 
 * Copyright (C) by Brockhaus Group, Wiesbaden www.brockhaus-gruppe.de
 * 
 * @author mbohnen, Jun 13, 2014
 * 
 */
public class EquipmentDAOMongoDBImpl implements EquipmentDAO {

	private static final Logger LOG = Logger.getLogger(EquipmentDAOMongoDBImpl.class);

	private MongoDBDataSource ds;
	private DB db;
	private String collectionName;
	private DBCollection dbCollection;

	public EquipmentDAOMongoDBImpl() {

	}

	public Equipment createEquipment(Equipment equipment) {
		LOG.info("creating: " + equipment.getKey());
		this.init();

		DBCollection dbCo = db.getCollection(collectionName);
		BasicDBObject document = new BasicDBObject();
		
		// generating the ID by ourselves ...so we have full control and don't need to 
		// go for a query to retrieve it and assign it to the equipment.
		ObjectId id = new ObjectId();
		equipment.set_id(id.toString());

		document.append("_id", equipment.get_id());
		document.append("desc", equipment.getDesc());
		document.append("key", equipment.getKey());

		// dealing with the sensors
		List<Sensor> sensors = equipment.getSensors();
		List<BasicDBObject> sensorList = new ArrayList<>();
		for (Sensor sensor : sensors) {
			BasicDBObject basic = new BasicDBObject();
			basic.append("desc", sensor.getDesc());
			basic.append("dataType", sensor.getDataType().toString());

			sensorList.add(basic);

		}
		document.append("sensors", sensorList);	
		
		dbCo.save(document);

		return equipment;
	}

	@Override
	public List<Equipment> findEquipmentByKey(String key) {
		this.init();
		BasicDBObject query = new BasicDBObject("key", key);

		List<Equipment> hits = new ArrayList<Equipment>();
		
		DBCursor cursor = dbCollection.find(query);
		while (cursor.hasNext()) {
			DBObject dbObj =  cursor.next();
			hits.add(JSONBuilderParserUtil.getInstance().fromJSON(Equipment.class, dbObj.toString()));
		}

		return hits;
	}

	@Override
	public Equipment findEquipmentBy_Id(String _id) {
		this.init();
		
		Equipment e = null;
		
		BasicDBObject query = new BasicDBObject("_id", _id);
		DBCursor cursor = dbCollection.find(query);
		while (cursor.hasNext()) {
			DBObject dbObj = cursor.next();
			e = JSONBuilderParserUtil.getInstance().fromJSON(Equipment.class, dbObj.toString());
		}
		return e;
	}
	
	@Override
	public List<Equipment> findAll() {
		this.init();
		DBCursor cursor = this.dbCollection.find();
		List<Equipment> hits = new ArrayList<Equipment>();

		while (cursor.hasNext()) {
			DBObject dbObj =  cursor.next();
			hits.add(JSONBuilderParserUtil.getInstance().fromJSON(Equipment.class, dbObj.toString()));
		}

		return hits;
	}
	
	@Override
	public void updateEquipment(Equipment equipment) {
		this.init();
		
		if (null == equipment.get_id()) {
			throw new NoSuchDocumentException("No _id present, was document ever stored before?");
		} else if (this.findEquipmentBy_Id(equipment.get_id()) == null) {
			throw new NoSuchDocumentException("Document doesn't exist, maybe you create it instead");
		}
		
		BasicDBObject query = new BasicDBObject("_id", equipment.get_id());
		DBCursor cursor = dbCollection.find(query);
		DBObject hit = cursor.one();
		
		// this is pretty hard-wired ... reflection would do much better
		hit.put("desc", equipment.getDesc());
		hit.put("key", equipment.getKey());

		// dealing with the sensors
		List<Sensor> sensors = equipment.getSensors();
		List<BasicDBObject> sensorList = new ArrayList<>();
		for (Sensor sensor : sensors) {
			BasicDBObject basic = new BasicDBObject();
			basic.append("desc", sensor.getDesc());
			basic.append("dataType", sensor.getDataType().toString());

			sensorList.add(basic);

		}
		hit.put("sensors", sensorList);
		
		this.dbCollection.save(hit);
	}

	@Override
	public void deleteEquipment(Equipment equipment) {
		this.init();
		if (null == equipment.get_id() || this.findEquipmentBy_Id(equipment.get_id()) == null) {
			throw new NoSuchDocumentException("Document doesn't exist, so you can't delete it");
		}
		
		BasicDBObject query = new BasicDBObject("_id", equipment.get_id());
		DBCursor cursor = dbCollection.find(query);
		DBObject hit = cursor.one();
		this.dbCollection.remove(hit);
		
	}

	private void init() {
		try {
			if (this.db == null) {
				db = ds.getDB();
			}
			if (this.dbCollection == null) {
				this.dbCollection = db.getCollection(collectionName);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public void setDs(MongoDBDataSource ds) {
		this.ds = ds;
	}

	public void setDb(DB db) {
		this.db = db;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}


}

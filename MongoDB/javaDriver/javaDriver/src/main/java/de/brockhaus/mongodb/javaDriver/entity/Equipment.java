/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

/**
 * Project: javaDriver
 * 
 * Copyright (C) by Brockhaus Group, Wiesbaden www.brockhaus-gruppe.de
 * 
 * @author mbohnen, Jun 12, 2014
 * 
 */
public class Equipment implements Serializable {

	private String _id;
	private String key;
	private String desc;

	private List<Sensor> sensors = new ArrayList<Sensor>();

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the sensors
	 */
	public List<Sensor> getSensors() {
		return sensors;
	}

	/**
	 * @param sensors the sensors to set
	 */
	public void setSensors(List<Sensor> sensors) {
		this.sensors = sensors;
	}

}

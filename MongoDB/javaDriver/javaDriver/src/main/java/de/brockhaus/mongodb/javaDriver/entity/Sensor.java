/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.entity;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Project: javaDriver
 * 
 * Copyright (C) by Brockhaus Group, Wiesbaden www.brockhaus-gruppe.de
 * 
 * @author mbohnen, Jun 12, 2014
 * 
 */
public class Sensor {

	private String _id;
	@JsonIgnore
	private Equipment equipment;
	private String desc;
	private OPCDataType dataType;

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the equipment
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * @param equipment
	 *            the equipment to set
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the dataType
	 */
	public OPCDataType getDataType() {
		return dataType;
	}

	/**
	 * @param dataType
	 *            the dataType to set
	 */
	public void setDataType(OPCDataType dataType) {
		this.dataType = dataType;
	}

}

/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.dao;

import java.util.List;

import de.brockhaus.mongodb.javaDriver.entity.Equipment;

/**
 * Project: javaDriver
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 13, 2014
 *
 */
public interface EquipmentDAO {
	
	public Equipment createEquipment(Equipment equipment);
	public List<Equipment> findEquipmentByKey(String key);
	public List<Equipment> findAll();
	public Equipment findEquipmentBy_Id(String _id);
	public void updateEquipment(Equipment equipment);
	public void deleteEquipment(Equipment equipment);

}

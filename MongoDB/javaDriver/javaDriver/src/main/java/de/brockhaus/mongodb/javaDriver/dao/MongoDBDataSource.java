package de.brockhaus.mongodb.javaDriver.dao;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

/**
 * 
 * Project: javaDriver
 *
 * Inspired by the Java DataSource, maybe it will become a Singleton one day or we will make use of Spring
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 13, 2014
 *
 */
public class MongoDBDataSource
{
	private String host;
	private String dbName;
	private int port;

	public DB getDB() throws UnknownHostException {
		 
		// connect to mongoDB, ip and port number (since 2.10.0, use MongoClient)
		MongoClient mongo = new MongoClient(host, port);

		// get database from MongoDB,
		// if database doesn't exists, mongoDB will create it automatically
		DB db = mongo.getDB(dbName);

		return db;
	}

	public DB getDB(String host, int port, String dbName) throws UnknownHostException {
		this.host = host;
		this.port = port;

		return this.getDB();
	}
	
	public List<String> getDatabasesForInstance(MongoClient mongo) {
		return mongo.getDatabaseNames();
	}
	
	public Set<String> getCollectionsForDB(DB db) {
		return db.getCollectionNames();
	}
	
	public DBCollection getCollectionByName(DB db, String collectionName) {
		return db.getCollection(collectionName);
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
}

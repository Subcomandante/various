/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.dao;

/**
 * Project: javaDriver
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 17, 2014
 *
 */
public class NoSuchDocumentException extends Error {
	
	public NoSuchDocumentException(String msg) {
		super(msg);
	}
	
	public NoSuchDocumentException() {
		super("No such object");
	}

}

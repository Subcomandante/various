/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.service;

import java.util.List;

import de.brockhaus.mongodb.javaDriver.entity.Equipment;

/**
 * Project: javaDriver
 *
 * Copyright (C) by Brockhaus Group, Wiesbaden
 * www.brockhaus-gruppe.de
 *
 * @author mbohnen, Jun 13, 2014
 *
 */
public interface EquipmentService {
	
	public Equipment createEquipment(Equipment equipment);
	
	public String getEquipmentAsJSONString(Equipment equipment);

	public List<Equipment> findEquipmentByKey(String key);
	
	public Equipment findEquipmentBy_Id(String _id);

	public List<Equipment> findAll();
	
	public void updateEquipment(Equipment equipment);
	
	public void deleteEquipment(Equipment equipment);
	
	

}

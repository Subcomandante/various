/**
 * 
 */
package de.brockhaus.mongodb.javaDriver.util;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Project: javaDriver
 * 
 * Copyright (C) by Brockhaus Group, Wiesbaden www.brockhaus-gruppe.de
 * 
 * @author mbohnen, Jun 13, 2014
 * 
 */
public class JSONBuilderParserUtil {

	private static final JSONBuilderParserUtil THIS = new JSONBuilderParserUtil();
	private ObjectMapper objectMapper = new ObjectMapper();

	private JSONBuilderParserUtil() {
		// lazy
	}

	public static JSONBuilderParserUtil getInstance() {
		return THIS;
	}

	public String toJSON(Object o) {

		String ret = null;

		try {
			objectMapper.setSerializationInclusion(Include.NON_NULL);
			ret = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		}

		return ret;
	}

	public <T> T fromJSON(Class<T> clazz, String json) {
		T ret = null;

		try {
			ret = (T) objectMapper.readValue(json, clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ret;
	}
}

package de.brockhaus.ordering.control;

import javax.inject.Inject;

import de.brockhaus.ordering.boundary.OrderService;
import de.brockhaus.ordering.entity.DocumentOrder;
import de.brockhaus.ordering.entity.DocumentOrderDAO;

public class OrderServiceBean implements OrderService {

	@Inject
	private DocumentOrderDAO dao;
	
	@Override
	public void createDocumentOrder(DocumentOrder order) {
		this.dao.createDocument(order);
	}
}

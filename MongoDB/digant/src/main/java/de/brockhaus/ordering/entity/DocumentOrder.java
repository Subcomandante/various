package de.brockhaus.ordering.entity;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "document_order")
public class DocumentOrder extends AbstractDocument {

	@Id
	private String id;
	
	@Embedded
	private List<DocumentOrderDetail> details = new ArrayList<DocumentOrderDetail>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<DocumentOrderDetail> getDetails() {
		return details;
	}

	public void setDetails(List<DocumentOrderDetail> details) {
		this.details = details;
	}
	
	
}

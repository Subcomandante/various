package de.brockhaus.ordering.entity;

import java.net.UnknownHostException;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

/**
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Jul 10, 2014
 *
 */
public class MorphiaDataSource {
	
	private String dbName = "digord";
	private String host = "localhost";
	private String port = "27017";
	
	private Morphia morphia;
	private Datastore datastore;
	
	public MorphiaDataSource() {
		this.init();
	}
	
	private void init() {
		MongoClient client = null;
		try {
			client = new MongoClient(host);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		morphia = new Morphia();
		datastore = morphia.createDatastore(client, dbName);     
		morphia.mapPackage("de.brockhaus.ordering.entity");
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public Morphia getMorphia() {
		return morphia;
	}

	public void setMorphia(Morphia morphia) {
		this.morphia = morphia;
	}

	public Datastore getDatastore() {
		return datastore;
	}

	public void setDatastore(Datastore datastore) {
		this.datastore = datastore;
	}

}

package de.brockhaus.ordering.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author mbohnen
 *
 */
public class DocumentOrderDetail {
    /** the sub-type of the document */
    @Column(name = "DOKUMENT_SUBTYP")
    private String documentSubType;

    /**
     * the version of the product, int encoded Produktversion in German
     */
    private long productVersion;

    /**
     * The id of the process in which this order entry was proceeded by DIGANT
     */
    private String digantProcessId;

    /** the unique key within DIGANT */
    private String digantApplicationId;

    /** the locking token set by DIGANT */
    @Column(name = "DIGANT_SPERRTOKEN")
    private String digantLockToken;

    // Vorname in German
    private String firstName;

    // Nachname in German
    private String lastName;

    // Geburtsdatum in German
    private String dateOfBirth;

    // Antragsdatum in German
    private Date dateOfApplication;

    // Ablaufdatum in German
    private Date dateOfExpiration;

    /** the serial number of the document ordered */
    // Seriennummer in German (SNR)
    private String serialNumber;

    /** Aussenstelle (remote branch) */
    private String branch;

    /** the id of the administrative body */
    private String idAuthority;

    /** location reference where the ordered document will be handed over */
    private long idDeliveryAdress;

    /** location reference where the invoice will be send to */
    private long idBillingAdress;

    /**
     * fast proceeding
     */
    private boolean express;

    /**
     * is there a complaint
     */
    // Reklamation in German
    private boolean complaint;

    /**
     * does the passport have more than the regular pages TODO in the future this
     * will go to order level only
     */
    private boolean extraPages;

    /**
     * Mandator we're ordering this document for
     * ID of the community Allgemeiner Gemeindeschlüssel (AGS) in German
     */
    private String mandator;

    /**
     * ordered by in case of Verwaltungsgemeinschaften it might differ from the
     * mandator ordered for
     */
    private String orderingMandator;

    /** mandator we're logged in with */
    private String currentMandator;

	public String getDocumentSubType() {
		return documentSubType;
	}

	public void setDocumentSubType(String documentSubType) {
		this.documentSubType = documentSubType;
	}

	public long getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(long productVersion) {
		this.productVersion = productVersion;
	}

	public String getDigantProcessId() {
		return digantProcessId;
	}

	public void setDigantProcessId(String digantProcessId) {
		this.digantProcessId = digantProcessId;
	}

	public String getDigantApplicationId() {
		return digantApplicationId;
	}

	public void setDigantApplicationId(String digantApplicationId) {
		this.digantApplicationId = digantApplicationId;
	}

	public String getDigantLockToken() {
		return digantLockToken;
	}

	public void setDigantLockToken(String digantLockToken) {
		this.digantLockToken = digantLockToken;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfApplication() {
		return dateOfApplication;
	}

	public void setDateOfApplication(Date dateOfApplication) {
		this.dateOfApplication = dateOfApplication;
	}

	public Date getDateOfExpiration() {
		return dateOfExpiration;
	}

	public void setDateOfExpiration(Date dateOfExpiration) {
		this.dateOfExpiration = dateOfExpiration;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getIdAuthority() {
		return idAuthority;
	}

	public void setIdAuthority(String idAuthority) {
		this.idAuthority = idAuthority;
	}

	public long getIdDeliveryAdress() {
		return idDeliveryAdress;
	}

	public void setIdDeliveryAdress(long idDeliveryAdress) {
		this.idDeliveryAdress = idDeliveryAdress;
	}

	public long getIdBillingAdress() {
		return idBillingAdress;
	}

	public void setIdBillingAdress(long idBillingAdress) {
		this.idBillingAdress = idBillingAdress;
	}

	public boolean isExpress() {
		return express;
	}

	public void setExpress(boolean express) {
		this.express = express;
	}

	public boolean isComplaint() {
		return complaint;
	}

	public void setComplaint(boolean complaint) {
		this.complaint = complaint;
	}

	public boolean isExtraPages() {
		return extraPages;
	}

	public void setExtraPages(boolean extraPages) {
		this.extraPages = extraPages;
	}

	public String getMandator() {
		return mandator;
	}

	public void setMandator(String mandator) {
		this.mandator = mandator;
	}

	public String getOrderingMandator() {
		return orderingMandator;
	}

	public void setOrderingMandator(String orderingMandator) {
		this.orderingMandator = orderingMandator;
	}

	public String getCurrentMandator() {
		return currentMandator;
	}

	public void setCurrentMandator(String currentMandator) {
		this.currentMandator = currentMandator;
	}
}

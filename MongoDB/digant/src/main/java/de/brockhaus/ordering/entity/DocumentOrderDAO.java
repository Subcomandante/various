package de.brockhaus.ordering.entity;

import javax.inject.Inject;

/**
 * 
 * @author mbohnen
 *
 */
public class DocumentOrderDAO {
	
	@Inject
	private MorphiaDataSource ds;
	
	public void createDocument(AbstractDocument document) {
		ds.getDatastore().save(document);
	}

}

package de.brockhaus.ordering.boundary;

import de.brockhaus.ordering.entity.DocumentOrder;

public interface OrderService {

	void createDocumentOrder(DocumentOrder order);

}

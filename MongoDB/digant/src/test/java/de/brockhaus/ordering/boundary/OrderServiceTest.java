package de.brockhaus.ordering.boundary;

import javax.enterprise.inject.se.SeContainer;

import org.jboss.weld.environment.se.Weld;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import de.brockhaus.ordering.entity.DocumentOrder;
import de.brockhaus.ordering.entity.DocumentOrderDetail;

/**
 * 
 * @author mbohnen
 *
 */
public class OrderServiceTest {

	private SeContainer container;
	private OrderService service;
	
	@Test
	public void createDocumentOrderTest() {
		DocumentOrder order = new DocumentOrder();
		DocumentOrderDetail detail1 = new DocumentOrderDetail();
		DocumentOrderDetail detail2 = new DocumentOrderDetail();
		
		order.getDetails().add(detail2);
		order.getDetails().add(detail1);
		
		service.createDocumentOrder(order);
	}

	@BeforeClass
	public void setUp() {
		
		container = Weld.newInstance().initialize();
		service = container.select(OrderService.class).get();
	}
	
	@AfterClass
	public void tearDown() {
		container.close();
	}
}

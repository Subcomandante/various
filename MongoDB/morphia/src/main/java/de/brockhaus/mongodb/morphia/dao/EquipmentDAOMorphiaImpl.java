package de.brockhaus.mongodb.morphia.dao;

import java.util.List;

import de.brockhaus.mongodb.morphia.entity.Equipment;

public class EquipmentDAOMorphiaImpl implements EquipmentDAO {
	
	// we might make use of Spring DI
	private MorphiaDataSource ds = new MorphiaDataSource();

	public Equipment createEquipment(Equipment equipment) {
		ds.getDatastore().save(equipment);
		return null;
	}

	public List<Equipment> findEquipmentByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Equipment> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Equipment findEquipmentBy_Id(String _id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateEquipment(Equipment equipment) {
		// TODO Auto-generated method stub
		
	}

	public void deleteEquipment(Equipment equipment) {
		// TODO Auto-generated method stub
		
	}

}

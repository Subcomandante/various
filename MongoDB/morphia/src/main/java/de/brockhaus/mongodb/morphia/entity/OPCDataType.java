/**
 * 
 */
package de.brockhaus.mongodb.morphia.entity;

import java.io.Serializable;

/**
 * 
 * Project: morphia
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Jul 10, 2014
 *
 */
public enum OPCDataType {

	BOOLEAN(0), FLOAT(1), INTEGER(2), STRING(3);

	private final int value;

	private OPCDataType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public static String getStringValueFromInt(int i) {
		for (OPCDataType dataType : OPCDataType.values()) {
	         if (dataType.getValue() == i) {
	             return dataType.toString();
	         }
	     }
	     // throw an IllegalArgumentException or return null
	     throw new IllegalArgumentException("the given number doesn't match any Status.");
	 }

}

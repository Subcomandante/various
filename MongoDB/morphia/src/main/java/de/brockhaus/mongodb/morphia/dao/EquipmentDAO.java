/**
 * 
 */
package de.brockhaus.mongodb.morphia.dao;

import java.util.List;

import de.brockhaus.mongodb.morphia.entity.Equipment;

/**
 * 
 * Project: morphia
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Jul 10, 2014
 *
 */
public interface EquipmentDAO {
	
	public Equipment createEquipment(Equipment equipment);
	public List<Equipment> findEquipmentByKey(String key);
	public List<Equipment> findAll();
	public Equipment findEquipmentBy_Id(String _id);
	public void updateEquipment(Equipment equipment);
	public void deleteEquipment(Equipment equipment);

}

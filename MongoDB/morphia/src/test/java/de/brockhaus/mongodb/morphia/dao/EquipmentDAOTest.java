package de.brockhaus.mongodb.morphia.dao;

import org.bson.types.ObjectId;

import de.brockhaus.mongodb.morphia.entity.Equipment;
import de.brockhaus.mongodb.morphia.entity.OPCDataType;
import de.brockhaus.mongodb.morphia.entity.Sensor;



public class EquipmentDAOTest {
	
	private Equipment e;

	public static void main(String[] args) {
		EquipmentDAOTest test = new EquipmentDAOTest();
		test.setUpData();
		
		test.testCreateEquipment();

	}

	private void testCreateEquipment() {
		EquipmentDAO dao = new EquipmentDAOMorphiaImpl();
		dao.createEquipment(e);
	}
	
	private void setUpData() {
		
		e = new Equipment();
		e.setKey("ABC");
		e.setDesc("some fancy equipment");
		
		Sensor s1 = new Sensor();
		s1.setDataType(OPCDataType.BOOLEAN);
		s1.setDesc("sensor.1");
		s1.setEquipment(e);
		e.getSensors().add(s1);
		
		Sensor s2 = new Sensor();
		s2.setDataType(OPCDataType.FLOAT);
		s2.setDesc("sensor.2");
		s2.setEquipment(e);
		e.getSensors().add(s2);
	}

}

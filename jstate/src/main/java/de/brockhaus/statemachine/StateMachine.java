package de.brockhaus.statemachine;

import unquietcode.tools.esm.EnumStateMachine;
import unquietcode.tools.esm.StateHandler;
import unquietcode.tools.esm.TransitionHandler;

/**
 * A simple example of a state machine
 * 
 * @author mbohnen
 *
 */
public class StateMachine extends EnumStateMachine<State> {

	public StateMachine(State initialState) {
		
		this.setInitialState(initialState);
		this.init();
		
	}

	private void init() {
		
		// defining a transition handler for all states
		TransitionHandler<State> transitionHandler = new TransitionHandler<State>() {
			
			// catching all transitions
			@Override
			public void onTransition(State from, State to) {
				System.out.println("state changed from: " + from + " to: " + to);
				
				// going for individual transition
				if(from == State.Ready && to == State.Running) {
					System.out.println("now running");
				}
			}
		};
		
		// defining a state handler 
		// anonymous instantiation of functional interface; alternatively see
		// OnEnterStateRunning class
		StateHandler<State> stateHandlerEnterRunning = new StateHandler<State>() {
			
			public void onState(State state) {
				System.out.println("State handler reports entering state: " + state);
			}
		};
		
		StateHandler<State> stateHandlerLeaveReady = new StateHandler<State>() {
			
			public void onState(State state) {
				System.out.println("State handler reports leaving state: " + state);
			}
		};
		
		// adding transition handler to dedicated states
		this.addTransitions(transitionHandler, State.Ready, State.Running, State.Finished);
		
		this.addTransitions(State.Running, State.Paused, State.Stopping);
		this.addTransitions(State.Paused, State.Running, State.Stopping);
		this.addTransitions(State.Stopping, State.Stopped);
		this.addTransitions(State.Stopped, State.Finished);
		this.addTransitions(State.Finished, State.Ready, null);
		
		// setting the state handler once we enter the state
		this.onEntering(State.Running, stateHandlerEnterRunning);
		
		// setting the state handler once we are leaving the state
		this.onExiting(State.Ready, stateHandlerLeaveReady);
		
	}
}

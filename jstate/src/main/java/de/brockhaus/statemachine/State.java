package de.brockhaus.statemachine;

/**
 * Enumeration for states allowed
 * 
 * @author mbohnen
 *
 */
public enum State {

	 Ready, Running, Paused, Stopping, Stopped, Finished;
}

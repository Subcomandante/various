package de.brockhaus.statemachine;

import org.testng.annotations.Test;

import unquietcode.tools.esm.EnumStateMachine;
import unquietcode.tools.esm.TransitionException;

/**
 * 
 * @author mbohnen
 *
 */
public class StateMachineTest extends EnumStateMachine<State> {

	@Test
	public void testTransitionState() {
		StateMachine sm = new StateMachine(State.Ready);
		sm.transition(State.Running);
		System.out.println("current state: " + sm.currentState());
	}
	
	@Test(expectedExceptions = TransitionException.class)
	public void testTransitionStateFails() {
		StateMachine sm = new StateMachine(State.Ready);
		sm.transition(State.Paused);
		System.out.println("current state: " + sm.currentState());
	}
}

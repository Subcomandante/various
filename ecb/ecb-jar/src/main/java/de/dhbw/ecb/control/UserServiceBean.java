package de.dhbw.ecb.control;



import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import de.dhbw.ecb.boundary.rmi.UserService;
import de.dhbw.ecb.entity.User;
import de.dhbw.ecb.entity.UserDAO;

@Stateless
@Remote(UserService.class)
public class UserServiceBean implements UserService {
	
	private static final Logger LOG = Logger.getLogger(UserServiceBean.class);
	
	@EJB
	private UserDAO dao;

	@Override
	public User createOrUpdateUser(User user) {
		user = this.dao.createOrUpdateUser(user);
		return user;
	}

	@Override
	public User findUserByCredentials(String user, String pwd) {
		return this.dao.findUserByCredentials(user, pwd);
	}

}

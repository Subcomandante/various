package de.dhbw.ecb.entity;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@LocalBean
@Stateless
public class UserDAO {
	
	@PersistenceContext(name = "resource_pu")
	private EntityManager em;
	
	public User createOrUpdateUser(User u) {
		if(0 == u.getId()) {
			this.em.persist(u);
		}
		this.em.merge(u);
		
		return u;
	}
	
	public User findUserByCredentials(String user, String pwd) {
		String jpql = "FROM User AS u WHERE u.pwd = :pwd AND u.user = :user";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		query.setParameter("pwd", pwd);
		query.setParameter("user", user);
		
		User u = query.getSingleResult();
		
		return u;
	}
}

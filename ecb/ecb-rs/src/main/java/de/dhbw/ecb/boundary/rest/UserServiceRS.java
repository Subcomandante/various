package de.dhbw.ecb.boundary.rest;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import de.dhbw.ecb.boundary.rmi.UserService;
import de.dhbw.ecb.entity.User;

@Path("/userServiceRS")
public class UserServiceRS {

	private UserService userService;
	
	public UserServiceRS() {
		this.init();
	}
	
	private void init() {
		try {
			Context ctx = new InitialContext();
			// internal lookup !!
			userService = (UserService) ctx.lookup("java:global/ecb-all/ecb-jar-0.0.1-SNAPSHOT/UserServiceBean!de.dhbw.ecb.boundary.rmi.UserService");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * http://localhost:8080/ecb-rs/userServiceRS/createUser
	 * 
	 * 
{
  "user" : "maryp",
  "pwd" : "tinkerbell",
  "person" : {

    "firstName" : "Mary",
    "lastName" : "Poppins"
  }
}
	 * 
	 * @param userAsJSON
	 * @return
	 */
	@POST
	@Path("createUser")
	public String createOrUpdateUser(String userAsJSON) {
		
		User user = JSONBuilderParserUtil.getInstance().fromJSON(User.class, userAsJSON);
		user = userService.createOrUpdateUser(user);
		
		return JSONBuilderParserUtil.getInstance().toJSON(user);
		
	}
	
	/**
	 * http://localhost:8080/ecb-rs/userServiceRS/getUser/peterp/neverland
	 * @param user
	 * @param pwd
	 * @return
	 */
	@GET
	@Path("getUser/{user}/{pwd}")
	public String getUserByCredentials(@PathParam("user") String user, @PathParam("pwd") String pwd) {
		User hit = userService.findUserByCredentials(user, pwd); 
		String ret = JSONBuilderParserUtil.getInstance().toJSON(hit);
		
		return ret;
	}
}

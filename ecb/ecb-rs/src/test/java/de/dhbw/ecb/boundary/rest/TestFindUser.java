package de.dhbw.ecb.boundary.rest;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import de.dhbw.ecb.boundary.rmi.UserService;
import de.dhbw.ecb.entity.User;

public class TestFindUser {
	
	public static void main(String[] args) throws NamingException {
		Context ctx = new InitialContext();
		UserService userService = (UserService) ctx.lookup("ecb-all/ecb-jar-0.0.1-SNAPSHOT/UserServiceBean!de.dhbw.ecb.boundary.rmi.UserService");
		User u = userService.findUserByCredentials("peterp", "neverland");
		System.out.println(u.getUser());
	}

}

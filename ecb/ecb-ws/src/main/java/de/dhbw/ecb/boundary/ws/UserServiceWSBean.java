package de.dhbw.ecb.boundary.ws;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import de.dhbw.ecb.boundary.rmi.UserService;
import de.dhbw.ecb.entity.User;

@Stateless
@Remote(UserServiceWS.class)
@WebService
public class UserServiceWSBean implements UserServiceWS {
	
	@EJB
	private UserService service;
	
	public UserServiceWSBean() {
//		this.init();
	}

	@Override
	public User createOrUpdateUser(@WebParam(name="user") User user) {
		return service.createOrUpdateUser(user);
	}

	@Override
	public User findUserByCredentials(@WebParam(name="user") String user,@WebParam(name="pwd") String pwd) {
		return service.findUserByCredentials(user, pwd);
	}
	
	private void init() {
			//not needed as deployed within the same ear
//			try {
//				Context ctx = new InitialContext();
//				// internal lookup !!
//				service = (UserService) ctx.lookup("java:global/ecb-all/ecb-jar-0.0.1-SNAPSHOT/UserServiceBean!de.dhbw.ecb.boundary.rmi.UserService");
//			} catch (NamingException e) {
//				e.printStackTrace();
//			}
	}
	

}

package de.dhbw.ecb.boundary.ws;

import javax.ejb.Remote;
import javax.jws.WebService;

import de.dhbw.ecb.entity.User;

@WebService
@Remote // do we need this?
public interface UserServiceWS {
	
	public User createOrUpdateUser(User user);
	public User findUserByCredentials(String user, String pwd);

}

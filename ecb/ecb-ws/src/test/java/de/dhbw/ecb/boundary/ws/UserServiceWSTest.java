package de.dhbw.ecb.boundary.ws;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import de.dhbw.ecb.entity.Person;
import de.dhbw.ecb.entity.User;


public class UserServiceWSTest {
	
	private UserServiceWS userService;
	
	public static void main(String[] args) {
		UserServiceWSTest test = new UserServiceWSTest();
		test.setUp();
		
		test.testFindUserByCredentials();
//		test.testCreateOrUpdateUser();
	}

	private void testCreateOrUpdateUser() {
		User u = new User();
		u.setPwd("badass");
		u.setUser("cpth");
		
		Person p = new Person();
		p.setFirstName("Captain");
		p.setLastName("Hook");
		p.getUsers().add(u);
		
		u.setPerson(p);
		
		userService.createOrUpdateUser(u);
		
	}

	private void testFindUserByCredentials() {
		User u = this.userService.findUserByCredentials("peterp", "neverland");
		System.out.println(u.getUser() + u.getPerson().getFirstName());
		
	}

	private void setUp() {
		try {
			URL wsdlLocation = new URL("http://localhost:8080/ecb-ws-0.0.1-SNAPSHOT/UserServiceWSBean?wsdl");
			// from wsdl: targetNamespace, ServiceName
			QName serviceName = new QName("http://ws.boundary.ecb.dhbw.de/", "UserServiceWSBeanService");
			Service service = Service.create(wsdlLocation, serviceName);

			userService = (UserServiceWS) service.getPort(UserServiceWS.class);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

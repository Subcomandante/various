package de.dhbw.ecb.boundary.rmi;

import javax.ejb.Remote;
import javax.jws.WebService;

import de.dhbw.ecb.entity.User;

@Remote
public interface UserService {

	User createOrUpdateUser(User user);
	User findUserByCredentials(String user, String pwd);	
}

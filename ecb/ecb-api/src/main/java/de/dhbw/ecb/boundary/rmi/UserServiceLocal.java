package de.dhbw.ecb.boundary.rmi;

import javax.ejb.Local;

/**
 * 
 * @author mbohnen
 *
 */
@Local
public interface UserServiceLocal extends UserService {

}

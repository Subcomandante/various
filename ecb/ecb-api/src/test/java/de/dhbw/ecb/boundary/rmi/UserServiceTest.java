package de.dhbw.ecb.boundary.rmi;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import de.dhbw.ecb.boundary.rmi.UserService;
import de.dhbw.ecb.entity.Person;
import de.dhbw.ecb.entity.User;


public class UserServiceTest {
	
	private UserService service;
	
	public static void main(String[] args) {
		UserServiceTest test = new UserServiceTest();
		 
		test.init();
//		test.testFindUserByCredentials();
		test.testCreateUser();
	}

	private void testCreateUser() {
		User user = new User();
		user.setPwd("neverland");
		user.setUser("peterp");
		
		Person p = new Person();
		p.setFirstName("Peter");
		p.setLastName("Pan");
		
		p.getUsers().add(user);
		user.setPerson(p);
		
		service.createOrUpdateUser(user);
		
	}

	private void testFindUserByCredentials() { 
		User u = service.findUserByCredentials("peterp", "neverland");
		System.out.println(u.getUser());
		
	}
	
	
	private void init() {
		try {
			Context ctx = new InitialContext();
			service = (UserService) ctx.lookup("/mds-all/userService-0.0.1-SNAPSHOT/UserServiceBean!de.cellent.mds.userService.boundary.rmi.UserService");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

}

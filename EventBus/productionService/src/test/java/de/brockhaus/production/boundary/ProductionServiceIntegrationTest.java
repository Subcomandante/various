package de.brockhaus.production.boundary;

import java.util.UUID;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import de.brockhaus.production.domain.entity.product.ProductionOrder;


/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 12, 2024
 *
 */
public class ProductionServiceIntegrationTest {

	private WeldContainer container;
	private ProductionService service;
	
	@Test
	public void testFinishProductionOrder() throws Exception {

		ProductionOrder order = new ProductionOrder();
		order.setCustomerId("CUSTOMERID_123");
		order.setProductId("PRODUCT_456");
		order.setProductionQuantity(99);
		
		service.finishProductionOrder(order, UUID.randomUUID().toString());
	}
	
	@BeforeClass
	public void setUp() {
		Weld weld = new Weld();
		container = weld.initialize();
		service = container.select(ProductionService.class).get();
	}

	@AfterClass
	public void tearDown() {
		container.shutdown();
	}
}

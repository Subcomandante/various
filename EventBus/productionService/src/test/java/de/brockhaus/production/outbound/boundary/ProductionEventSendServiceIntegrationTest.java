package de.brockhaus.production.outbound.boundary;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.common.event.domain.outbound.EventMessageLogEntry;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 25, 2023
 *
 */
public class ProductionEventSendServiceIntegrationTest {
	
	private static final Logger LOG = LogManager.getLogger(ProductionEventSendServiceIntegrationTest.class);
	
	private ProductionEventSendService service;
	
	private WeldContainer container;
	
	@Test
	public void testFindEvents2BSend() {
		List<EventMessageLogEntry> hits = service.findEvents2BSend();
		LOG.info(hits.size());
	}
	
	@Test
	public void testDoSend() throws Exception {
		
		List<EventMessageLogEntry> hits = service.findEvents2BSend();
		
		if(hits.size() > 0) {
			service.doSend(hits);	
		} else {
			LOG.warn("no hits, nothing 2 send");
		}
		
	}

	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		service = container.select(ProductionEventSendService.class).get();
		
	}
	
	@AfterTest
	public void tearDiwn() {
		container.close();
	}

}

package de.brockhaus.production.outbound.control;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.production.domain.entity.product.ProductionOrder;
import de.brockhaus.production.outbound.control.ProductionOutboundEventAdapterKafka;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Mar 6, 2024
 *
 */
public class ProductionOutboundEventAdapterKafkaIntegrationTest {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionOutboundEventAdapterKafkaIntegrationTest.class);
	
	private WeldContainer container;
	
	private ProductionOutboundEventAdapterKafka sender;
	
	@Test(expectedExceptions = NullPointerException.class)
	public void testSendEvent_EventIsNull() throws Exception {
		sender.sendEvent(null);
	}

	@Test
	public void testSendEvent() throws Exception {
		EventMessage message = new EventMessage.EventMessageBuilder().
				withDomainClass(ProductionOrder.class).
				withDomainObjectId("99").
				withEventClassification(EventClassification.PRODUCTION_ORDER_FINISHED.toString()).
				withTransactionId(UUID.randomUUID().toString()).
				build();
		
		sender.sendEvent(message);
	}
	
	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		sender = container.select(ProductionOutboundEventAdapterKafka.class).get();
		
	}
	
	@AfterTest
	public void tearDown() {
		LOG.info("Listener down");
		container.close();
	}
}

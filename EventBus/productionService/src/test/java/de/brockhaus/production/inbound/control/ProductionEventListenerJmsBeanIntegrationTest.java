package de.brockhaus.production.inbound.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.production.inbound.control.ProductionEventListenerJmsBean;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Dec 18, 2023
 *
 */
public class ProductionEventListenerJmsBeanIntegrationTest {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionEventListenerJmsBeanIntegrationTest.class);
	
	private WeldContainer container;
	private ProductionEventListenerJmsBean receiver;
	
	@Test
	public void run() {
		while(true);
	}

	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		receiver = container.select(ProductionEventListenerJmsBean.class).get();
		
	}
	
	@AfterTest
	public void tearDown() {
		LOG.info("Listener down");
		container.close();
	}
}

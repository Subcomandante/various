package de.brockhaus.production.domain.entity.event;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.brockhaus.common.event.domain.inbound.EventMessageReceived;

/**
 * The messages received (which might be processed later on)
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 6, 2024
 *
 */
@Entity
@Table( uniqueConstraints = { 
		@UniqueConstraint(name = "Idempotency", columnNames = { "TRANSACTION_ID", "EVENT_ID", "EVENT_CLASSIFICATION" }) },
		schema = "billing")
public class MessageReceived extends EventMessageReceived {
	
	
}

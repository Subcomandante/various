package de.brockhaus.production.outbound.control;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Named;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.exception.EventSendingException;
import de.brockhaus.common.util.json.JSONParser;

/**
 * https://www.slingacademy.com/article/kafka-create-a-simple-producer-in-java/
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Mar 3, 2024
 *
 */
@Named("kafka")
public class ProductionOutboundEventAdapterKafka implements EventSenderAdapter {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionOutboundEventAdapterKafka.class);

	private static final String SEND_TOPIC_NAME = "UvzEvent";

	private Producer<String, String> producer;

	private Properties kafkaConnectProps;

	@Override
	public void sendEvent(EventMessage event) throws Exception {

		Objects.requireNonNull(event, "Event must not be null");

		try {

			if (null != producer) {

				/// getting payload from event message log entry
				String payload = new JSONParser().toJSON(event, true, false);
				
				LOG.info("Sending event through Kafka" + payload);				

				Callback callback = new Callback() {
					@Override
					public void onCompletion(RecordMetadata recordMetadata, Exception e) {
						if (e == null) {
							// the record was successfully sent
							LOG.info("Received new metadata. \n" + "Topic:" + recordMetadata.topic() + "\n"
									+ "Partition: " + recordMetadata.partition() + "\n" + "Offset: "
									+ recordMetadata.offset() + "\n" + "Timestamp: " + recordMetadata.timestamp());
						} else {
							LOG.error("Error while producing", e);
						}
					}
				};

				// creating the record
				ProducerRecord<String, String> record = new ProducerRecord<String, String>(SEND_TOPIC_NAME, payload);

				// get will get the metadata immediately, otherwise the callback is triggered by
				// another thread
				RecordMetadata metadata = producer.send(record, callback).get();
				producer.flush();
				
				LOG.debug("Message successfully send");

			} else {
				LOG.error("no producer, was null");
			}

		} catch (Exception e) {
			LOG.error(e);
			throw e;

		}
	}
	
	@PostConstruct
	public void setUp() throws EventSendingException {
//		this.checkKafkaAvailability();
		producer = this.createProducer();
	}

	/**
	 * Creating a producer
	 * 
	 * @return
	 * @throws EventSendingException 
	 */
	private Producer<String, String> createProducer() throws EventSendingException {

		Producer<String, String> producer = null;

		try {
			// 4 properties see:
			// https://medium.com/@anupriya7295/kafka-producer-error-handling-37e4fa218e69
			kafkaConnectProps = new Properties();
			kafkaConnectProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9099");
			kafkaConnectProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringSerializer");
			kafkaConnectProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringSerializer");
			kafkaConnectProps.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
			kafkaConnectProps.setProperty(ProducerConfig.ACKS_CONFIG, "all");
			kafkaConnectProps.setProperty(ProducerConfig.RETRIES_CONFIG, Integer.toString(Integer.MAX_VALUE));
			kafkaConnectProps.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");

			producer = new KafkaProducer<>(kafkaConnectProps);

		} catch (Exception e) {
			LOG.error(e);
			throw new EventSendingException("Creation of producer failed", e);
		}

		return producer;
	}

	/**
	 * this is an awful hack but it seems to be the only option
	 * @throws EventSendingException 
	 */
	public boolean checkKafkaAvailability() throws EventSendingException {
		
		boolean ret = false;
		
		try {
			Properties kafkaConnectProps = new Properties();
			kafkaConnectProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9099");
			kafkaConnectProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringDeserializer");
			kafkaConnectProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringDeserializer");

			KafkaConsumer consumer = new KafkaConsumer<>(kafkaConnectProps);
			Map topics = consumer.listTopics(Duration.ofMillis(3000));

			consumer.close();
			
			ret = true;
			
		} catch (Exception e) {
			LOG.error(e);
			throw new EventSendingException("Establishing connection to Kafka failed", e);
		}
		
		return ret;
	}

	@PreDestroy
	public void tearDown() {
		if (null != producer) {
			LOG.error("Producer closed");
			producer.close();

		} else {
			LOG.warn("No producer 2 close, was null");
		}
	}
}

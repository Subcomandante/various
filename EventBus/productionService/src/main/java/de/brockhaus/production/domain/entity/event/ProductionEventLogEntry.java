package de.brockhaus.production.domain.entity.event;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.event.domain.outbound.EventLogEntry;

/**
 * 
 * The unique events related to an business aggregate or domain object like a deed.
 * 
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Oct 14, 2023
 *
 */
@Entity
@Table(name = "EVENT_LOG", schema = "PRODUCTION")
@XmlRootElement
public class ProductionEventLogEntry extends EventLogEntry {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	

	public ProductionEventLogEntry() {
		
	}

	public ProductionEventLogEntry(EventClassification classification) {
		super(classification);
	}
	
}

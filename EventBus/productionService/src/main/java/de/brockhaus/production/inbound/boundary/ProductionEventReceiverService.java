package de.brockhaus.production.inbound.boundary;

import de.brockhaus.common.event.boundary.EventReceiverAdapter;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Dec 18, 2023
 *
 */
public interface ProductionEventReceiverService extends EventReceiverAdapter{

}

package de.brockhaus.production.outbound.control;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Named;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ScheduledMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessage.MessageHeaderProperties;
import de.brockhaus.common.util.json.JSONParser;
import jakarta.jms.Connection;
import jakarta.jms.JMSException;
import jakarta.jms.MessageProducer;
import jakarta.jms.Queue;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

/**
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Oct 14, 2023
 *
 */
@Named("jms")
public class ProductionOutboundEventAdapterJMS implements EventSenderAdapter {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionOutboundEventAdapterJMS.class);

//	private static final String REPLY_TOPIC_NAME = "prod/reply";

	private static final String SEND_TOPIC_NAME = "prod/event";

	private Connection connection;

	private Queue destination;

	private MessageProducer producer;

	private Session session;

	private Queue replyTo;

	@Override
	public void sendEvent(EventMessage eventMsg) throws Exception {
		
		Objects.requireNonNull(eventMsg, "Event must not be null");

		LOG.info("Sending events through JMS");

		try {

			LOG.info("Transactional Session?: {}", session.getTransacted());
			
			// getting payload from event message log entry
			String payload = new JSONParser().toJSON(eventMsg, true, false);

			// create a JMS message object with payload
			TextMessage jmsMsg = session.createTextMessage(payload);

			// set JMS Header properties
			jmsMsg.setStringProperty("TRANSACTION_ID", eventMsg.getMessageHeader().get(MessageHeaderProperties.TRANSACTION_ID));
			jmsMsg.setStringProperty("EVENT_CLASSIFICATION", eventMsg.getMessageHeader().get(MessageHeaderProperties.EVENT_CLASSIFICATION));
			jmsMsg.setStringProperty("EVENT_ID", eventMsg.getMessageHeader().get(MessageHeaderProperties.EVENT_ID));
			jmsMsg.setStringProperty("SYSTEM", eventMsg.getMessageHeader().get(MessageHeaderProperties.SENDER_IDENTIFIER));
			jmsMsg.setStringProperty("DOMAIN_CLASS", eventMsg.getMessageHeader().get(MessageHeaderProperties.DOMAIN_CLASS));
			jmsMsg.setStringProperty("DOMAIN_CLASS_EVENT", "CREATED");
			jmsMsg.setStringProperty("DOMAIN_OBJECT_ID", eventMsg.getMessageHeader().get(MessageHeaderProperties.DOMAINOBJECT_ID));
			jmsMsg.setStringProperty("CONTENT_TYPE", eventMsg.getMessageHeader().get(MessageHeaderProperties.MESSAGE_DATATYPE));
			jmsMsg.setStringProperty("CONTENT_SCHEMA", eventMsg.getMessageHeader().get(MessageHeaderProperties.MESSAGE_SCHEMA_URI));


			// setting reply queue
			jmsMsg.setJMSReplyTo(replyTo);
			
			// set redelivery
			jmsMsg.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_REPEAT, 2);

			// send the message to the queue - not send 2 WaWi right now, something like
			// "prepare"
			producer.send(jmsMsg);

			// now finally send ... (a crash here will cause the previous written event 2b
			// be changed within the exception handling)
			session.commit();

		} catch (Exception e) {
			LOG.error(e);
			throw e;
		}

	}

	@PostConstruct
	public void setUp() {

		try {
			// create a connection to ActiveMQ JMS broker using AMQP protocol
			ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
			connection = factory.createConnection();
			connection.start();

			// create a session (transactional)
			session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);

			// create a queue
			destination = session.createQueue(SEND_TOPIC_NAME);
//			  replyTo = session.createQueue(REPLY_TOPIC_NAME);

			// create a producer specific to queue
			producer = session.createProducer(destination);

		} catch (JMSException e) {
			LOG.error(e);
		}
	}

	@PreDestroy
	public void cleanUp() {
		try {
			connection.close();
		} catch (JMSException e) {
			LOG.error(e);
		}
	}
}

package de.brockhaus.production.boundary;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Nov 9, 2023
 *
 */
@ApplicationPath("/production/service")
public class ProductionApplication extends Application {

}

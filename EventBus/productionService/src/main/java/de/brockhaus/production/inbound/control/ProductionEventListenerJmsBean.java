package de.brockhaus.production.inbound.control;

import static org.apache.activemq.ActiveMQConnection.DEFAULT_BROKER_URL;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.service.repository.RepositoryException;
import de.brockhaus.production.domain.boundary.ProductionRepository;
import de.brockhaus.production.domain.entity.event.MessageReceived;
import de.brockhaus.production.inbound.boundary.ProductionEventReceiverService;
import jakarta.jms.Connection;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageListener;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;
import jakarta.jms.Topic;

/**
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Dec 18, 2023
 *
 */
public class ProductionEventListenerJmsBean implements MessageListener {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionEventListenerJmsBean.class);
	
	private static final String APPFACT_EVENT_DESTINATION = "appfact/event";

	private static final String FILTER_CRITERIA = "EVENT_CLASSIFICATION = 'APPFACT_INVOICE_CREATED'";
	
	@Inject
	private ProductionEventReceiverService service;
	
	@Inject
	private ProductionRepository repository;

	private Connection connection;
	private Session session;
	private Topic receiveDestination;

	@Override
	public void onMessage(Message message) {
		
		TextMessage txtMsg = (TextMessage) message;
		
		try {
			LOG.info("Message received: {} -> {}", FILTER_CRITERIA, txtMsg.getText());
			
			// save and delegate further ...
			this.saveMessage(txtMsg);
			
		} catch (Exception e) {
			LOG.error(e);
		}

	}

	private void saveMessage(TextMessage txtMsg) throws JMSException, RepositoryException {
		MessageReceived msg = new MessageReceived();
		msg.setClassification(EventClassification.valueOf(txtMsg.getStringProperty("EVENT_CLASSIFICATION")));
		msg.setTransactionId(txtMsg.getStringProperty("TRANSACTION_ID"));
		msg.setEventId(txtMsg.getStringProperty("EVENT_ID"));
		msg.setMsgData(((TextMessage) txtMsg).getText());
		
		this.repository.createOrUpdateEntity(msg);
		
	}

	@PostConstruct
	public void setUp() {

		try {
			// create a connection to ActiveMQ 
			ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(DEFAULT_BROKER_URL); // amqp://localhost:5672
			connection = factory.createConnection();

			// for durable subscription
			connection.setClientID(this.getClass().getSimpleName());

			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			
			receiveDestination = session.createTopic(APPFACT_EVENT_DESTINATION);
			
			MessageConsumer consumer = session.createDurableSubscriber(receiveDestination, this.getClass().getSimpleName(), FILTER_CRITERIA, false);

			consumer.setMessageListener(this);
			
			connection.start();
			
			LOG.info("Listener up'n'running");

		} catch (JMSException e) {
			LOG.error(e);
		}
	}

	@PreDestroy
	public void cleanUp() {
		try {
			connection.close();
			LOG.info("Listener down");

		} catch (JMSException e) {
			LOG.error(e);
		}
	}
}

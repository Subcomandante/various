package de.brockhaus.production.outbound.boundary;

import java.util.List;

import de.brockhaus.common.event.domain.outbound.EventMessageLogEntry;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 25, 2023
 *
 */
public interface ProductionEventSendService {
	
	List<EventMessageLogEntry> findEvents2BSend();

	void doSend(List<EventMessageLogEntry> events) throws Exception;
}

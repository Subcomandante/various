package de.brockhaus.production.domain.entity.product;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.service.domain.AbstractEntity;



/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Dec 12, 2023
 *
 */
@Entity
@Table(schema = "PRODUCTION")
public class ProductionOrder extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	// reference by the id of a product (no association)
	private String productId;
	
	// reference by the id to a customer (no association)
	private String customerId;
	
	private int productionQuantity;
	
	@Enumerated(EnumType.STRING)
	private EventClassification currentStatus;
	
	@Column(nullable = false, name = "STATUS_CHANGED_WHEN")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime lastStatusChange;

	@Override
	public Long getId() {	
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public int getProductionQuantity() {
		return productionQuantity;
	}

	public void setProductionQuantity(int productionQuantity) {
		this.productionQuantity = productionQuantity;
	}

	public EventClassification getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(EventClassification currentStatus) {
		this.currentStatus = currentStatus;
	}

	public LocalDateTime getLastStatusChange() {
		return lastStatusChange;
	}

	public void setLastStatusChange(LocalDateTime lastStatusChange) {
		this.lastStatusChange = lastStatusChange;
	}
	
}

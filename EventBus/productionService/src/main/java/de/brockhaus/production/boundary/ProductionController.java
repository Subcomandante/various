package de.brockhaus.production.boundary;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * RESTful interface 4 Events
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 24, 2023
 *
 */
@Path("/service/v1")
@ApplicationScoped
public class ProductionController {
	
	@GET
	@Path("/ping")
	@Produces(MediaType.TEXT_PLAIN)
	public String ping() {
		return this.getClass().getSimpleName() + " -> " + "pong";
	}
}

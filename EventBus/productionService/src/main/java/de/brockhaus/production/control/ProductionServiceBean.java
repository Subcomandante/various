package de.brockhaus.production.control;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.event.domain.outbound.EventMessageStatus;
import de.brockhaus.common.service.control.GenericServiceBean;
import de.brockhaus.production.boundary.ProductionService;
import de.brockhaus.production.domain.boundary.ProductionRepository;
import de.brockhaus.production.domain.entity.event.ProductionEventLogEntry;
import de.brockhaus.production.domain.entity.event.ProductionEventMessageLogEntry;
import de.brockhaus.production.domain.entity.product.ProductionOrder;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 8, 2024
 *
 */
public class ProductionServiceBean extends GenericServiceBean implements ProductionService {

	@Inject
	private ProductionRepository repository;

	@Override
	public void setRepository() {
		super.repository = this.repository;
		
	}

	/**
	 * According to transactional outbox once a deed is created the according entries to EventLog and EventMessageLog must be 
	 * created as well
	 * 
	 * @throws Exception 
	 * 
	 */
	@Override
	public ProductionOrder finishProductionOrder(ProductionOrder prodOrder, String businessTransactionId) throws Exception {
		
		// create event log entry ...
		ProductionEventLogEntry event = new ProductionEventLogEntry(EventClassification.PRODUCTION_ORDER_FINISHED);
		
		EntityManager em = this.repository.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			prodOrder.setCurrentStatus(EventClassification.PRODUCTION_ORDER_FINISHED);
			prodOrder.setLastStatusChange(LocalDateTime.now());
			// persist but not commit to get the auto generated id
			em.persist(prodOrder);
			
			// with new transaction id
			event.setTransactionId(businessTransactionId);
			event.setDomainObjectClass(prodOrder.getClass().getCanonicalName());
			// with new event id
			event.setEventId(UUID.randomUUID().toString());
			event.setDomainObjectId(prodOrder.getId().toString());
					
			event.getEventDataMap().put("DomainId", prodOrder.getId().toString());
			event.getEventDataMap().put("DomainClass", prodOrder.getClass().getCanonicalName());
			event.getEventDataMap().put("CustomerId", prodOrder.getCustomerId());
			event.getEventDataMap().put("ProductId", prodOrder.getProductId());
			event.getEventDataMap().put("ProductQuantity", prodOrder.getProductionQuantity());
			event.getEventDataMap().put("ProductOrderStatus", prodOrder.getCurrentStatus());
			event.getEventDataMap().put("ProductOrderStatus_Change", prodOrder.getLastStatusChange().toString());
			
			// create the event message log entry
			ProductionEventMessageLogEntry msgLogEntry = new ProductionEventMessageLogEntry(EventMessageStatus.MSG_SEND_PENDING, event);
			msgLogEntry.setSendAttempts(0);
			event.getMsgLogEntries().add(msgLogEntry);
			
			em.persist(event);
			
			// commit all
			tx.commit();
			
		} catch (Exception e) {
			throw e;
		}

		return prodOrder;
	}

}

package de.brockhaus.production.outbound.control;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessageLogEntry;
import de.brockhaus.common.event.domain.outbound.EventMessageStatus;
import de.brockhaus.common.service.repository.RepositoryException;
import de.brockhaus.common.util.json.JSONParser;
import de.brockhaus.production.domain.boundary.ProductionRepository;
import de.brockhaus.production.domain.entity.event.ProductionEventMessageLogEntry;
import de.brockhaus.production.outbound.boundary.ProductionEventSendService;

/**
 * The service related to sending of events. Should be triggered by some
 * CRON-style job
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Oct 18, 2023
 *
 */
public class ProductionEventSendServiceBean implements ProductionEventSendService {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionEventSendServiceBean.class);

	private static final int MAX_SEND_ATTEMPTS = 3;
	
	@Inject
	@Named("jms") // inject either jms, rest or anything (2b configured external at one time)
	private EventSenderAdapter adapter;

	@Inject
	private ProductionRepository repository;

	public List<EventMessageLogEntry> findEvents2BSend() {

		List<EventMessageLogEntry> hits = this.repository.findEntitiesByNamedQuery(EventMessageLogEntry.class,
				ProductionEventMessageLogEntry.FIND_UNSEND);

		return hits;
	}

	public void doSend(List<EventMessageLogEntry> events) throws Exception {
		
		for (EventMessageLogEntry event : events) {
			
			// increase counter by one
			event.setSendAttempts(event.getSendAttempts() +1); 
			
			try {
				EventMessage eventMessage = new JSONParser().fromJSON(EventMessage.class, event.getPayLoad());
				adapter.sendEvent(eventMessage);
				event.setEventMsgStatus(EventMessageStatus.MSG_SEND_OK);
				event.setRemarks("sucessfully send");

			} catch (Exception e) {

				event.setRemarks(e.getLocalizedMessage());
				if(event.getSendAttempts() >= MAX_SEND_ATTEMPTS) {
					event.setEventMsgStatus(EventMessageStatus.MSG_SEND_FAILED);
					
				} else {
					event.setEventMsgStatus(EventMessageStatus.MSG_SEND_NOK);	
				}
				
				
			}
			
			try {
				repository.createOrUpdateEntity(event);
				
			} catch (RepositoryException e) {
				LOG.error(e);
				throw e;
			}
		}
	}
}

package de.brockhaus.production.outbound.control;

import java.util.Objects;

import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.util.json.JSONParser;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 14, 2023
 *
 */
@Named("rest")
public class ProductionOutboundEventAdapterRest implements EventSenderAdapter {

	//just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionOutboundEventAdapterRest.class);

	@Override
	public void sendEvent(EventMessage event) throws JsonProcessingException{
		
		Objects.requireNonNull(event, "Event must not be null");
		
		LOG.info("Sending event through REST" + new JSONParser().toJSON(event, true, false));
		
		//TODO implement
	}

}

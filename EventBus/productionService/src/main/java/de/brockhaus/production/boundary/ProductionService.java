package de.brockhaus.production.boundary;

import de.brockhaus.common.service.boundary.GenericService;
import de.brockhaus.production.domain.entity.product.ProductionOrder;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 8, 2024
 *
 */
public interface ProductionService extends GenericService {
	
	ProductionOrder finishProductionOrder(ProductionOrder prodOrder, String bussinessTransactionId) throws Exception;

}

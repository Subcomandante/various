package de.brockhaus.production.control;

import javax.inject.Inject;

import de.brockhaus.common.service.control.GenericServiceBean;
import de.brockhaus.production.boundary.ProductionEventService;
import de.brockhaus.production.domain.boundary.ProductionRepository;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 24, 2023
 *
 */
public class ProductionEventServiceBean extends GenericServiceBean implements ProductionEventService {
	
	@Inject
	private ProductionRepository repository;

	@Override
	public void setRepository() {
		super.repository = this.repository;
		
	}

}

package de.brockhaus.production.boundary;

import de.brockhaus.common.service.boundary.GenericService;

/**
 * Service interface for the most common interactions related 2 Events
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 24, 2023
 *
 */
public interface ProductionEventService extends GenericService {

}

package de.brockhaus.production.domain.control;

import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.service.repository.GenericRepositoryBean;
import de.brockhaus.production.domain.boundary.ProductionRepository;

/**
 * Repository 4 everything related to UVZ
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 24, 2023
 *
 */
public class ProductionRepositoryBean extends GenericRepositoryBean implements ProductionRepository{

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProductionRepositoryBean.class);

	// which persistence unit is made use of (see persistence.xml)
	static final String PERSISTENCE_UNIT = "uvz";

	// the only one
	private EntityManagerFactory emf;

	public ProductionRepositoryBean() {
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
	}

	@Override
	public synchronized EntityManager getEntityManager() {

		LOG.debug("getting entity manager by thread: {}", Thread.currentThread().getId());

		EntityManager em = emf.createEntityManager();

		Map<String, Object> properties = em.getProperties();
		String propertiesAsString = properties.keySet().stream().map(key -> key + "=" + properties.get(key))
				.collect(Collectors.joining(", ", "{", "}"));

		LOG.debug("Persistence properties: {]", propertiesAsString);

		return em;
	}

	@Override
	protected String getPersistenceUnit() {
		return ProductionRepositoryBean.PERSISTENCE_UNIT;
	}

	@PreDestroy
	public void shutDown() {
		emf.close();
	}

}

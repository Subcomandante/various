package de.brockhaus.production.domain.entity.event;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.event.boundary.MessageContentType;
import de.brockhaus.common.event.domain.outbound.EventLogEntry;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessageLogEntry;
import de.brockhaus.common.event.domain.outbound.EventMessageStatus;
import de.brockhaus.common.util.json.JSONParser;

/**
 * Tracking sending of event messages
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Nov 24, 2023
 *
 */
@Entity
@Table(name = "EVENTMESSAGE_LOG", schema = "PRODUCTION")
public class ProductionEventMessageLogEntry extends EventMessageLogEntry {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	
	public ProductionEventMessageLogEntry() {
		super();
	}
	
	public ProductionEventMessageLogEntry(EventMessageStatus eventMsgStatus, EventLogEntry event) throws JsonProcessingException {
		super(eventMsgStatus, event);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	@Override
	protected String createMessagePayload() throws JsonProcessingException {
		
		EventMessage message = new EventMessage.EventMessageBuilder().
				withTransactionId(this.getEvent().getTransactionId()).
				withEventId(this.getEvent().getEventId()).
				withMessageDataType(MessageContentType.JSON).
				withMsgData(this.getEvent().getEventDataMap()).
				withSenderIdentifier("PRODUCTION").
				withEventClassification(this.getEvent().getClassification().toString()).
				withReplyTo("Some JMS destination or URI").
				withDomainObjectId(this.getEvent().getDomainObjectId()).
				withDomainClass(this.getEvent().getDomainObjectClass()).
				build();
		
		String payload = new JSONParser().toJSON(message, true, false);
		return payload;
	}

}

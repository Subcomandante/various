DROP TABLE PRODUCTION.PRODUCTIONORDER;
DROP TABLE PRODUCTION.EVENTMESSAGE_LOG;
DROP TABLE PRODUCTION.EVENT_LOG;
DROP TABLE BILLING.EVENTMESSAGERECEIVED 

DELETE FROM PRODUCTION.PRODUCTIONORDER;
DELETE FROM PRODUCTION.EVENTMESSAGE_LOG;
DELETE FROM PRODUCTION.EVENT_LOG;
DELETE FROM BILLING.EVENTMESSAGERECEIVED; 

SELECT ID, CURRENTSTATUS, STATUS_CHANGED_WHEN, CUSTOMERID, PRODUCTID,PRODUCTIONQUANTITY FROM PRODUCTION.PRODUCTIONORDER;
SELECT ID, TRANSACTION_ID, EVENT_ID, EVENT_CLASSIFICATION, DOMAINOBJECT_ID, EVENT_DATA FROM PRODUCTION.EVENT_LOG ;
SELECT id, event_id,event_classification,eventmsg_status,message, REMARKS FROM PRODUCTION.EVENTMESSAGE_LOG;
SELECT ID, TRANSACTION_ID, EVENT_CLASSIFICATION, RECEIVED_STATUS, EVENT_ID, MESSAGE_DATA, NOTE FROM BILLING.EVENTMESSAGERECEIVED 
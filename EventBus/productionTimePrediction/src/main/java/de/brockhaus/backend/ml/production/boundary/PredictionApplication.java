package de.brockhaus.backend.ml.production.boundary;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * created by mbohnen at Jun 27, 2024
 * Brockhaus Consulting GmbH
 */
@ApplicationPath("/prediction/service")
public class PredictionApplication  extends Application {

}

package de.brockhaus.backend.ml.production.model.control;

import javax.inject.Inject;

import de.brockhaus.backend.ml.production.model.boundary.ProductionPredictionModelService;
import de.brockhaus.backend.ml.production.model.entity.ProductionModelFileDAO;
import de.brockhaus.backend.ml.production.model.entity.ProductionPredictionModelName;
import weka.classifiers.Classifier;

/**
 * 
 * created by mbohnen at Jul 18, 2024
 * Brockhaus Consulting GmbH
 */
public class ProductionPredictionModelServiceBean implements ProductionPredictionModelService {
	
	@Inject
	private ProductionModelFileDAO dao;

	@Override
	public Classifier getClassifierByName(ProductionPredictionModelName name) {
		
		return dao.getModelEntries().get(name);
	}

}

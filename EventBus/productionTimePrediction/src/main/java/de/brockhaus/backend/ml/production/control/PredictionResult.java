package de.brockhaus.backend.ml.production.control;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * The result of a prediction related to a production process
 * 
 * created by mbohnen at Jun 26, 2024 Brockhaus Consulting GmbH
 */
@Schema(name = "PredictionResult", description = "POJO that represents the prediction.")
public class PredictionResult {

	@Schema(required = true)
	private String modelUsed;
	
	@Schema(required = true)
	private int productionAmount;

	@Schema(required = true)
	private int productionLoadCurrent;

	private double estimatedProductionTime;

	public PredictionResult(String modelUsed, int productionAmount, int productionLoadCurrent, double prediction) {
		super();
		this.modelUsed = modelUsed;
		this.productionAmount = productionAmount;
		this.productionLoadCurrent = productionLoadCurrent;
		this.estimatedProductionTime = prediction;
	}

	public String getModelUsed() {
		return modelUsed;
	}

	public void setModelUsed(String modelUsed) {
		this.modelUsed = modelUsed;
	}

	public int getProductionLoadCurrent() {
		return productionLoadCurrent;
	}

	public void setProductionLoadCurrent(int productionLoadCurrent) {
		this.productionLoadCurrent = productionLoadCurrent;
	}

	public double getEstimatedProductionTime() {
		return estimatedProductionTime;
	}

	public void setEstimatedProductionTime(double estimatedProductionTime) {
		this.estimatedProductionTime = estimatedProductionTime;
	}

	public int getProductionAmount() {
		return productionAmount;
	}

	public void setProductionAmount(int productionAmount) {
		this.productionAmount = productionAmount;
	}
}

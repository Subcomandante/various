package de.brockhaus.backend.ml.production.boundary;

import de.brockhaus.backend.ml.production.control.PredictionResult;
import de.brockhaus.backend.ml.production.model.entity.ProductionPredictionModelName;

/**
 * Service interface for prediction service
 * 
 * created by mbohnen at Jun 26, 2024
 * Brockhaus Consulting GmbH
 */
public interface DeliveryTimePredictionService {
	
	PredictionResult getEstimatedTime(int productAmount, int currentLoad) throws Exception;
	
	PredictionResult getEstimatedTime(int productAmount, int currentLoad, ProductionPredictionModelName classifierName) throws Exception;
	
	public void trainAndSaveLinearRegressionModel(String dataFileName, String modelFileName) throws Exception;

}

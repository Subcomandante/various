package de.brockhaus.backend.ml.production.model.entity;

/**
 * 
 * created by mbohnen at Jul 21, 2024
 * Brockhaus Consulting GmbH
 */
public enum ProductionPredictionModelName {
	
	LINEARREGRESSION_MODEL("models/LinearRegression.model"),
	MLP_MODEL("models/MultiLayerPerceptron.model"),
	RANDOMFOREST_MODEL("models/RandomForest.model"),
	ADDITIVEREGRESSION_MODEL("models/AdditiveRegression.model"),
	RANDOMCOMITEE_MODEL("models/RandomComitee.model"),
	KNEARESTNEIGHBOUR_MODEL("models/KNearestNeighbour.model"),
	DECISIONTABLE_MODEL("models/DecisionTable.model");
		
	public String location;
	
	private ProductionPredictionModelName(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}	
	
	
}



package de.brockhaus.backend.ml.production.model.boundary;

import de.brockhaus.backend.ml.production.model.entity.ProductionPredictionModelName;
import weka.classifiers.Classifier;

/**
 * 
 * created by mbohnen at Jul 23, 2024
 * Brockhaus Consulting GmbH
 */
public interface ProductionPredictionModelService {
	
	public Classifier getClassifierByName(ProductionPredictionModelName name);

}

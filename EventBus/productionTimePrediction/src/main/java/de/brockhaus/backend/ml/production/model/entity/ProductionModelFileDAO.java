package de.brockhaus.backend.ml.production.model.entity;

import java.io.InputStream;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.classifiers.Classifier;

/**
 * 
 * created by mbohnen at Jul 23, 2024
 * Brockhaus Consulting GmbH
 */
public class ProductionModelFileDAO {
	
	private static final Logger LOG = LogManager.getLogger(ProductionModelFileDAO.class);

	public HashMap<ProductionPredictionModelName, Classifier> modelEntries = new HashMap<ProductionPredictionModelName, Classifier>();
	
	public HashMap<ProductionPredictionModelName, Classifier> getModelEntries() {
		return modelEntries;
	}

	@PostConstruct
	public void loadModels() {
		
		for (ProductionPredictionModelName modelEntry : ProductionPredictionModelName.values()) {
			
			String location = modelEntry.getLocation();
			
			try {
				// load file
				InputStream input = this.getClass().getClassLoader().getResourceAsStream(location);
				
				if(null != input) {
					// deserialize
					Classifier classifier = (Classifier) weka.core.SerializationHelper.read(input);
					modelEntries.put(modelEntry, classifier);
					
				} else {
					LOG.warn("Can't load model: {}", modelEntry.toString());
				}

			} catch (Exception e) {
				LOG.error(e);
			}
			
		}
	}
}

package de.brockhaus.backend.ml.production.boundary;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponseSchema;

import de.brockhaus.backend.ml.production.control.PredictionResult;

/**
 * Restful controle for prediction of production time
 * 
 * Invocation example:
 * http://0.0.0.0:8080/prediction/service/production/v1/estimatedTime/4/3
 * 
 * OpenAPI interface description:
 * http://0.0.0.0:8080/prediction/service/openapi.json
 * 
 * created by mbohnen at Jun 27, 2024 Brockhaus Consulting GmbH
 */
@Path("/production/v1")
@ApplicationScoped
public class DeliveryTimePredictionController {

	@Inject
	@Named("weka")
	private DeliveryTimePredictionService service;

	@GET
	@Path("/estimatedTime/{productAmount}/{currentLoad}")
	@Produces(MediaType.APPLICATION_JSON)
	@APIResponseSchema(value = PredictionResult.class, responseDescription = "Result of production-time prediction", responseCode = "200")
	@Operation(summary = "prediction of production time", description = "Provides some prediction of production time based upon the amount of googd to be produced and the current queue of goods to be produced")
	public PredictionResult getEstimatedTime(
			@Parameter(description = "how many pieces to produce", required = true, example = "99", schema = @Schema(type = SchemaType.STRING)) 
			@PathParam("productAmount") int productAmount,
			@Parameter(description = "how many pieces are currenly enqueued for production", required = true, example = "99", schema = @Schema(type = SchemaType.STRING))
			@PathParam("currentLoad") int currentLoad) throws Exception {
		
		return service.getEstimatedTime(productAmount, currentLoad);
	}

	@GET
	@Path("/ping")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(summary = "heartbeat / isAlive", description = "Just 2 check if service is alive")
	public String ping() {
		return this.getClass().getSimpleName() + " -> " + "pong";
	}
}

package de.brockhaus.backend.ml.production.control;

import java.io.InputStream;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.backend.ml.production.boundary.DeliveryTimePredictionService;
import de.brockhaus.backend.ml.production.model.boundary.ProductionPredictionModelService;
import de.brockhaus.backend.ml.production.model.entity.ProductionPredictionModelName;
import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 * The weka based implementation for prediction of total production time
 * 
 * https://www.javatpoint.com/machine-learning-using-java
 * 
 * created by mbohnen at Jun 26, 2024 Brockhaus Consulting GmbH
 */
@Named("weka")
public class DeliveryTimePredictionServiceWeka3Bean implements DeliveryTimePredictionService {

	private static final Logger LOG = LogManager.getLogger(DeliveryTimePredictionServiceWeka3Bean.class);
	
	@Inject
	private ProductionPredictionModelService modelService;

////	@Inject
//    private Config config = ConfigProvider.getConfig();
//    private String greeting = ConfigProvider.getConfig().getValue("greeting.message", String.class);
//    
//    @Inject
//    private Foo foo;

	private Classifier classifierUsed4Prediction;

	@Override
	public PredictionResult getEstimatedTime(int productAmount, int currentLoad) throws Exception {

		// which attributes
		Attribute productAmountAttribute = new Attribute("productAmount");
		Attribute currentLoadAttribute = new Attribute("currentLoad");
		Attribute totalTimeAttribute = new Attribute("totalTime");

		ArrayList<Attribute> attributes = new ArrayList<>();
		attributes.add(productAmountAttribute);
		attributes.add(currentLoadAttribute);
		attributes.add(totalTimeAttribute);

		Instances dataUnpredicted = new Instances("TestInstances", attributes, 1);

		// last feature is target variable
		dataUnpredicted.setClassIndex(dataUnpredicted.numAttributes() - 1);

		// Creating an Instance for predictions
		DenseInstance predictionInstance = new DenseInstance(dataUnpredicted.numAttributes());
		predictionInstance.setDataset(dataUnpredicted);
		predictionInstance.setValue(0, productAmount);
		predictionInstance.setValue(1, currentLoad);
		LOG.debug("prediction instance: {}", predictionInstance);

		double predictedTime = classifierUsed4Prediction.classifyInstance(predictionInstance);
		LOG.debug("Predicted time: {}", predictedTime);

		// the result 2b returned
		PredictionResult result = new PredictionResult(classifierUsed4Prediction.toString(), productAmount, currentLoad,
				predictedTime);

		return result;
	}
	
	@Override
	public PredictionResult getEstimatedTime(int productAmount, int currentLoad,
			ProductionPredictionModelName classifierName) throws Exception {
		
		classifierUsed4Prediction = this.modelService.getClassifierByName(classifierName);
		
		return this.getEstimatedTime(productAmount, currentLoad);
	}

	@PostConstruct
	private void init() {

		this.classifierUsed4Prediction = this.modelService.getClassifierByName(ProductionPredictionModelName.LINEARREGRESSION_MODEL);
	}

	public void trainAndSaveLinearRegressionModel(String dataFileName, String modelFileName) throws Exception {
		try {
			// loading data from arff file
			ConverterUtils.DataSource source = new ConverterUtils.DataSource(dataFileName);
			Instances dataSet = source.getDataSet();

			// setting the last attribute (totalTime) to the class index
			dataSet.setClassIndex(dataSet.numAttributes() - 1);

			// creating a linear regression based classifier
			LinearRegression classifier = new LinearRegression();

			// train model
			classifier.buildClassifier(dataSet);

			// statistics
			Evaluation eval = new Evaluation(dataSet);
			eval.evaluateModel(classifier, dataSet);
			double errorRatio = eval.errorRate();
			LOG.debug("Summary: {}", eval.toSummaryString());
			LOG.debug("Calculation error rate (RootMeanError, the closer to zero, the better): {} ", errorRatio);

			if (modelFileName == null) {
				weka.core.SerializationHelper.write(modelFileName, classifier);
				
			} else {
				weka.core.SerializationHelper.write(modelFileName, classifier);
			}

		} catch (Exception e) {
			LOG.error(e);
			throw e;
		}
	}
}

package de.brockhaus.backend.eventBus.ml.boundary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import de.brockhaus.backend.ml.production.boundary.DeliveryTimePredictionController;
import de.brockhaus.backend.ml.production.control.PredictionResult;

/**
 * 
 * created by mbohnen at Jul 1, 2024
 * Brockhaus Consulting GmbH
 */
public class DeliveryTimePredictionControllerTest {

	private static final Logger LOG = LogManager.getLogger(DeliveryTimePredictionControllerTest.class);

	private WeldContainer container;
	private DeliveryTimePredictionController service;

	@Test
	public void testGetEstimatedTime() throws Exception {
		PredictionResult result = service.getEstimatedTime(15, 330);
		LOG.info("Predicted time: {}", result.getEstimatedProductionTime());
	}
	
	@BeforeClass
	public void setUp() {
		Weld weld = new Weld();
		container = weld.initialize();
		service = container.select(DeliveryTimePredictionController.class).get();
	}

	@AfterClass
	public void tearDown() {
		container.shutdown();
	}
}

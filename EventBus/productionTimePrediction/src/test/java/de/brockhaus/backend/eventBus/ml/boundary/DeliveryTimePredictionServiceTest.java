package de.brockhaus.backend.eventBus.ml.boundary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import de.brockhaus.backend.ml.production.boundary.DeliveryTimePredictionService;
import de.brockhaus.backend.ml.production.control.PredictionResult;



/**
 * 
 * created by mbohnen at Jun 26, 2024 Brockhaus Consulting GmbH
 */
public class DeliveryTimePredictionServiceTest {

	private static final Logger LOG = LogManager.getLogger(DeliveryTimePredictionServiceTest.class);

	private WeldContainer container;
	private DeliveryTimePredictionService service;

	@Test
	public void testGetEstimatedTime() throws Exception {
		
		final int amountOfProduct = 4;
		final int productsEnqueued = 3;

		PredictionResult result = service.getEstimatedTime(amountOfProduct, productsEnqueued);
		
		LOG.info("\n model used: {} \n predicted time: {} \n amount 2b produced: {}, \n products already enqueued 4 production: {}",
				result.getModelUsed(), result.getEstimatedProductionTime(), amountOfProduct, productsEnqueued);
		
		Assert.assertTrue(result.getEstimatedProductionTime() >= 0);
	}

	@Test
	public void trainAndSaveModel() throws Exception {
		service.trainAndSaveLinearRegressionModel("modelData/DataSetProductionTimeTest.arff", null);
	}

	@BeforeClass
	public void setUp() {
		Weld weld = new Weld();
		container = weld.initialize();
		service = container.select(DeliveryTimePredictionService.class).get();
	}

	@AfterClass
	public void tearDown() {
		container.shutdown();
	}
}

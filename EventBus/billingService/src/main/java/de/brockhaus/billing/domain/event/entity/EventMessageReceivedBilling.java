package de.brockhaus.billing.domain.event.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.brockhaus.common.event.domain.inbound.EventMessageReceived;

/**
 * The messages received (which MUST be stored immediately and might be processed later on)
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 6, 2024
 *
 */
@Entity
@Table( schema = "BILLING",
		name = "EventMessageReceived",
		uniqueConstraints = { 
		@UniqueConstraint(name = "Idempotency", columnNames = { "TRANSACTION_ID", "EVENT_ID", "EVENT_CLASSIFICATION" }) }
		)
public class EventMessageReceivedBilling extends EventMessageReceived {
	


}

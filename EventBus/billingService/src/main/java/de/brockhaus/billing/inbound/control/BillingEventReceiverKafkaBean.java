package de.brockhaus.billing.inbound.control;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.boundary.EventReceiverAdapter;
import de.brockhaus.common.event.exception.EventReceivingException;
import de.brockhaus.common.event.exception.EventSendingException;

/**
 * https://www.oreilly.com/library/view/kafka-the-definitive/9781491936153/ch04.html
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Mar 7, 2024
 *
 */
public class BillingEventReceiverKafkaBean {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventReceiverKafkaBean.class);

	private static final String RECEIVE_TOPIC_NAME = "UvzEvent";

	private KafkaConsumer<String, String> consumer = null;

	@Inject
	private EventReceiverAdapter eventService;

//	@Inject
//	private WaWiRepository repository;

	public void startConsuming() throws EventReceivingException {
		
		consumer.subscribe(Arrays.asList(RECEIVE_TOPIC_NAME));
		
		try {
		    while (true) {
		        ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
		        
		        if(records.count() == 0) {
		        	LOG.debug("no records at [topic -> {} time -> {}", RECEIVE_TOPIC_NAME, LocalDateTime.now());
		        }
		        
		        for (ConsumerRecord<String, String> record : records) {
		            System.out.printf(">>>> offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
		        }
		    }

		} catch (Exception e) {
	    	throw new EventReceivingException(e);
	    }
	}

	@PostConstruct
	public void setUp() throws EventSendingException {
//		this.checkKafkaAvailability();
		this.createConsumer();
	}
	
	@PreDestroy
	public void tearDown() {
		if(consumer != null) {
			consumer.close();
		}
	}

	public void createConsumer() throws EventSendingException {

		try {
			Properties consumerProps = new Properties();
			consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9099");
			consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, "appfact");
			consumerProps.put("enable.auto.commit", "true");
			consumerProps.put("auto.commit.interval.ms", "1000");
			consumerProps.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			consumerProps.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			consumerProps.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
			
			consumer = new KafkaConsumer<>(consumerProps);

			LOG.info("Consumer created");

		} catch (Exception e) {
			LOG.error(e);
			throw new EventSendingException("Creation of consumer failed", e);
		}
	}

	/**
	 * this is an awful hack but it seems to be the only option
	 * 
	 * @throws EventSendingException
	 */
	public boolean checkKafkaAvailability() throws EventReceivingException {

		boolean ret = false;

		try {
			Properties kafkaConnectProps = new Properties();
			kafkaConnectProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9099");
			kafkaConnectProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringDeserializer");
			kafkaConnectProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringDeserializer");

			KafkaConsumer consumer = new KafkaConsumer<>(kafkaConnectProps);
			Map topics = consumer.listTopics(Duration.ofMillis(10000));

			consumer.close();

			LOG.info("Kafka available");

			ret = true;

		} catch (Exception e) {
			LOG.error(e);
			throw new EventReceivingException("Establishing connection to Kafka failed", e);
		}

		return ret;
	}
}

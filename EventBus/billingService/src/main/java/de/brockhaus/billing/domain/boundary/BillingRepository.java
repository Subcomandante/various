package de.brockhaus.billing.domain.boundary;

import de.brockhaus.common.service.repository.GenericRepository;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 6, 2024
 *
 */
public interface BillingRepository extends GenericRepository {

}

package de.brockhaus.billing.outbound.control;

import java.util.HashMap;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.billing.outbound.boundary.BillingEventSendService;
import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.boundary.MessageContentType;
import de.brockhaus.common.event.domain.outbound.EventMessage;

/**
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Dec 8, 2023
 *
 */
public class BillingEventSendServiceBean implements BillingEventSendService {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventSendServiceBean.class);
	
	@Inject
	@Named("jms") // inject either jms, rest or anything (2b configured external at one time)
	private EventSenderAdapter outboundAdapter;

	@Override
	public void createAndSendEvent(String transactionId) throws Exception {
		
		HashMap<String, Object> msgData = new HashMap<>();
		msgData.put("INVOICE_ID", "99");
		msgData.put("DEED_RELATED", "666");
		msgData.put("INVOICE_AMOUNT", "99,63");
		
		
		EventMessage event =  new EventMessage.EventMessageBuilder().
				withDomainClass("de.bnotk.wawi.Invoice").
				withDomainObjectId("99").
				withEventId(UUID.randomUUID().toString()).
				withMessageDataType(MessageContentType.JSON).
				withMsgData(msgData).
				withEventClassification("APPFACT_INVOICE_CREATED").
				withSenderIdentifier("APPFACT").
				withTransactionId(transactionId).
				build();
		
		this.outboundAdapter.sendEvent(event);
		
	}
}

package de.brockhaus.billing.inbound.control;

import static org.apache.activemq.ActiveMQConnection.DEFAULT_BROKER_URL;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.billing.domain.boundary.BillingRepository;
import de.brockhaus.billing.domain.event.entity.EventMessageReceivedBilling;
import de.brockhaus.common.event.boundary.EventReceiverAdapter;
import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.service.repository.RepositoryException;
import de.brockhaus.common.util.json.JSONParser;
import jakarta.jms.Connection;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageListener;
import jakarta.jms.Queue;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;

/**
 * The receiver for a dedicated technology (here JMS) 
 * Just delegating to a service which implements InboundEventAdapter
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Nov 9, 2023
 *
 */
public class BillingEventReceiverJMSBean implements MessageListener {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventReceiverJMSBean.class);
	
	// the name of the destination we are listening to
	private static final String RECEIVE_DESTINATION_NAME = "prod/event";
	
	// message selector
	private static final String FILTER_CRITERIA = "EVENT_CLASSIFICATION = 'PRODUCTION_ORDER_FINISHED'";

	private Connection connection;

	private Session session;

	private Queue receiveDestination;
	
	// adapter to handle event
	@Inject 
	private EventReceiverAdapter eventService;

	@Override
	public void onMessage(Message message) {

		try {
			TextMessage msg = (TextMessage) message;
			String payload = msg.getText();
			
			LOG.info("Message received: {}", payload);
			
			// get the message object out of the payload
			EventMessage eventMessage = new JSONParser().fromJSON(EventMessage.class, payload);
						
			// delegate for further processing
			this.eventService.onEvent(eventMessage);
			
			// commit (on transacted sessions only)
//			session.commit();
			
		} catch (Exception e) {
			
			LOG.error(e);
			try {
				session.rollback();
				
			} catch (JMSException jmsExcp) {
				LOG.error(e);
			}
			
		}
	}

	@PostConstruct
	public void setUp() {

		try {
			// create a connection to ActiveMQ JMS broker using AMQP protocol
			ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(DEFAULT_BROKER_URL); //amqp://localhost:5672
			
			connection = factory.createConnection();
			connection.start();

			// create a session (transactional)
//			session = connection.createSession(true, -1); // transacted session
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			
			// create a queue
			receiveDestination = session.createQueue(RECEIVE_DESTINATION_NAME);
			MessageConsumer consumer = session.createConsumer(receiveDestination, FILTER_CRITERIA);
			consumer.setMessageListener(this);
			
			LOG.info("Listener up'n'running");

		} catch (Exception e) {
			LOG.error("Can`t setup properly", e);
		}
	}

	@PreDestroy
	public void cleanUp() {
		try {
			connection.close();
			LOG.info("Listener down");

		} catch (JMSException e) {
			LOG.error(e);
		}
	}
}

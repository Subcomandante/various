package de.brockhaus.billing.domain.event.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Priority;
import javax.interceptor.Interceptor;
import javax.interceptor.InterceptorBinding;

/**
 * 
 * created by mbohnen at May 29, 2024
 * Brockhaus Consulting GmbH
 */
@InterceptorBinding
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE})
@Priority(Interceptor.Priority.APPLICATION)
public @interface DeDupMessage {

}

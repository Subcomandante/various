package de.brockhaus.billing.outbound.boundary;

/**
 * Usually this should be the send service with the sole purpose to send an event
 * All the hokey pokey how this event is created is omitted, now we will just send based
 * on a message we received.
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Dec 8, 2023
 *
 */
public interface BillingEventSendService {

	void createAndSendEvent(String transactionId) throws Exception;
}

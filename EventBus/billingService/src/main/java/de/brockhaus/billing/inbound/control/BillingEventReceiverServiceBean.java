package de.brockhaus.billing.inbound.control;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.billing.domain.boundary.BillingRepository;
import de.brockhaus.billing.domain.event.entity.EventMessageReceivedBilling;
import de.brockhaus.billing.domain.event.interceptor.DeDupMessage;
import de.brockhaus.billing.outbound.boundary.BillingEventSendService;
import de.brockhaus.common.event.boundary.EventReceiverAdapter;
import de.brockhaus.common.event.domain.inbound.EventMessageReceivedStatus;
import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessage.MessageHeaderProperties;
import de.brockhaus.common.service.repository.RepositoryException;

/**
 * Here it happens on incoming events ...
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 15, 2024
 *
 */
public class BillingEventReceiverServiceBean implements EventReceiverAdapter {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventReceiverServiceBean.class);
	
	@Inject
	private BillingRepository repository;
	
	@Inject
	private BillingEventSendService sendService;

	@Override
	@DeDupMessage
	public void onEvent(EventMessage eventMessage) throws Exception {
		
		Objects.requireNonNull(eventMessage, "EventMessage must not be null");
		
		// save first
		this.saveMessage(eventMessage);
		
		// just do almost nothing
		String transactionId = eventMessage.getMessageHeader().get(MessageHeaderProperties.TRANSACTION_ID);
		
		// just routing through and sending ... no further business logic
//		this.sendService.createAndSendEvent(transactionId);
	}

	private void saveMessage(EventMessage eventMessage) throws Exception {
		
		EventMessageReceivedBilling msgReceived = new EventMessageReceivedBilling();
		
		try {
			String classificationString = eventMessage.getMessageHeader().get(MessageHeaderProperties.EVENT_CLASSIFICATION);
			EventClassification classification = EventClassification.valueOf(classificationString);
			Objects.requireNonNull(classification, "EventClassification must not be null");
			msgReceived.setClassification(classification);
			
			String transactionId = eventMessage.getMessageHeader().get(MessageHeaderProperties.TRANSACTION_ID);
			Objects.requireNonNull(transactionId, "TransactionId must not be null");
			msgReceived.setTransactionId(transactionId);
			
			msgReceived.setEventId(eventMessage.getMessageHeader().get(MessageHeaderProperties.EVENT_ID));
			
			msgReceived.setMsgData(eventMessage.toJson());
			
			msgReceived.setCurrentStatus(EventMessageReceivedStatus.MSG_RECEIVED_OK);
				
			
		} catch (Exception e) {
			msgReceived.setCurrentStatus(EventMessageReceivedStatus.MSG_RECEIVED_NOK);
			msgReceived.setNote(e.getLocalizedMessage());
		}
		
		try {
			this.repository.createOrUpdateEntity(msgReceived);
			
		} catch (RepositoryException e) {
			LOG.error(e);
			throw e;
		}
		
	}

}

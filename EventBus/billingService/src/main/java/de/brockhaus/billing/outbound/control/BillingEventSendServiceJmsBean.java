package de.brockhaus.billing.outbound.control;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessage.MessageHeaderProperties;
import de.brockhaus.common.util.json.JSONParser;
import jakarta.jms.Connection;
import jakarta.jms.JMSException;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;
import jakarta.jms.Topic;

/**
 * The concrete implementation of the OutboundEventAdapter using JMS 
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Dec 8, 2023
 *
 */
@Named("jms")
public class BillingEventSendServiceJmsBean implements EventSenderAdapter {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventSendServiceJmsBean.class);
	
	private static final String APPFACT_EVENT_DESTINATION = "appfact/event";

	private Connection connection;
	private Session session;
	private MessageProducer producer;

	private Topic eventDestination;
	
	@Override
	public void sendEvent(EventMessage event) throws Exception {
		
		LOG.info("sending event by JMS");
		
		try {
			
			// create a producer specific to queue
			producer = session.createProducer(eventDestination);
			
			String payload = new JSONParser().toJSON(event, true, false);
			
			// create a message object
	        TextMessage reply = session.createTextMessage(payload);
	        
	        // setting JMS Header
	        reply.setStringProperty(MessageHeaderProperties.DOMAIN_CLASS.toString(), event.getMessageHeader().get(MessageHeaderProperties.DOMAIN_CLASS));
	        reply.setStringProperty(MessageHeaderProperties.DOMAINOBJECT_ID.toString(), event.getMessageHeader().get(MessageHeaderProperties.DOMAINOBJECT_ID));
	        reply.setStringProperty(MessageHeaderProperties.EVENT_CLASSIFICATION.toString(), event.getMessageHeader().get(MessageHeaderProperties.EVENT_CLASSIFICATION));
	        reply.setStringProperty(MessageHeaderProperties.EVENT_ID.toString(), event.getMessageHeader().get(MessageHeaderProperties.EVENT_ID));
	        reply.setStringProperty(MessageHeaderProperties.MESSAGE_DATATYPE.toString(), event.getMessageHeader().get(MessageHeaderProperties.MESSAGE_DATATYPE));
	        reply.setStringProperty(MessageHeaderProperties.SENDER_IDENTIFIER.toString(), event.getMessageHeader().get(MessageHeaderProperties.SENDER_IDENTIFIER));
	        reply.setStringProperty(MessageHeaderProperties.TRANSACTION_ID.toString(), event.getMessageHeader().get(MessageHeaderProperties.TRANSACTION_ID));
	        
	        producer.send(reply);
	        session.commit();
	        
		} catch (JMSException e) {
			LOG.error(e);
		}
		
	}

	@PostConstruct
	public void setUp() {

		try {
			// create a connection to ActiveMQ JMS broker using AMQP protocol
			ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
			connection = factory.createConnection();
			connection.start();

			// create a session (transactional)
			session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
			eventDestination = session.createTopic(APPFACT_EVENT_DESTINATION);

		} catch (JMSException e) {
			LOG.error(e);
		}
	}


}

package de.brockhaus.billing.outbound.control;

import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.boundary.EventSenderAdapter;
import de.brockhaus.common.event.domain.outbound.EventMessage;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Apr 3, 2024
 *
 */
@Named("kafka")
public class BillingEventSendServiceKafkaBean implements EventSenderAdapter {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventSendServiceKafkaBean.class);

	@Override
	public void sendEvent(EventMessage event) throws Exception {
		
		LOG.info("sending event by Kafka");
		
		// TODO implement
		
	}

}

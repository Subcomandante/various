package de.brockhaus.billing.domain.event.interceptor;

import javax.annotation.PostConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessage.MessageHeaderProperties;

/**
 * 
 * created by mbohnen at May 28, 2024
 * Brockhaus Consulting GmbH
 */
@Interceptor
@DeDupMessage
public class DeDupMessageInterceptor {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(DeDupMessageInterceptor.class);
	
	private boolean isInitialized = false;

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {
		
		Object ret = null;
		
		try {
			if(! isInitialized) {
				this.initCache();
			}
			
			Object[] params = ctx.getParameters();
			Object paramRaw = params[0];
			EventMessage msg = (EventMessage) paramRaw;
			String eventID = msg.getMessageHeader().get(MessageHeaderProperties.EVENT_ID);
			
			boolean cached = this.checkEventIdInCache(eventID);
			
			if(! cached) {
				// continues as not cached
				LOG.debug("EventID was not cached, new EventMessage");
				
				//TODO put 2 cache 
				
				// go ahead
				ret = ctx.proceed();
				
			} else {
				// no further processing, ret stays null
				LOG.warn("EventID {} already in cache, EventMessage must be a duplicate", eventID);
				
			}
			
		} finally {
			LOG.debug("Leaving {}", ctx.getMethod().getName());
		}
		
		return ret;
	}

	private boolean checkEventIdInCache(String eventID) {
		return false;
	}
	
	@PostConstruct
	public void initCache() {
		this.isInitialized = true;
		LOG.info("Cache initialized");
	}
}

package de.brockhaus.billing.inbound.boundary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.billing.inbound.control.BillingEventReceiverJMSBean;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 26, 2023
 *
 */
public class BillingEventReceiverJMSBeanIntegrationTest {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventReceiverJMSBeanIntegrationTest.class);
	
	private WeldContainer container;
	private BillingEventReceiverJMSBean receiver;
	
	@Test
	public void run() {
		while(true);
	}

	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		
		// starting the event receiver in AppFact
		receiver = container.select(BillingEventReceiverJMSBean.class).get();
		
	}
	
	@AfterTest
	public void tearDown() {
		LOG.info("Listener down");
		container.close();
	}
}

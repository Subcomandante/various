package de.brockhaus.billing.outbound.boundary;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.billing.outbound.boundary.BillingEventSendService;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Dec 14, 2023
 *
 */
public class BillingEventSendServiceIntegrationTest {

	
	private WeldContainer container;
	private BillingEventSendService service;
	
	@Test
	public void testDoSend() throws Exception {
		this.service.createAndSendEvent("aBC-123 TransactionID");
	}

	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		service = container.select(BillingEventSendService.class).get();
		
	}
	
	@AfterTest
	public void tearDiwn() {
		container.close();
	}
}

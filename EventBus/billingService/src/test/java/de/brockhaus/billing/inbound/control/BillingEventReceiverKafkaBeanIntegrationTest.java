package de.brockhaus.billing.inbound.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.billing.inbound.control.BillingEventReceiverKafkaBean;
import de.brockhaus.common.event.exception.EventReceivingException;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Mar 8, 2024
 *
 */
public class BillingEventReceiverKafkaBeanIntegrationTest {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventReceiverKafkaBeanIntegrationTest.class);
	private WeldContainer container;
	private BillingEventReceiverKafkaBean receiver;
	
	@Test
	public void testConsume() throws EventReceivingException {
		receiver.startConsuming();
	}
	
	@Test
	public void testCheckKafkaAvailability() throws EventReceivingException {
		receiver.checkKafkaAvailability();
	}

	
	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		
		// starting the event receiver in AppFact
		receiver = container.select(BillingEventReceiverKafkaBean.class).get();
		
	}
	
	@AfterTest
	public void tearDown() {
		LOG.info("Listener down");
		container.close();
	}
}

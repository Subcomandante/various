package de.brockhaus.billing.outbound.control;

import java.util.HashMap;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.billing.outbound.control.BillingEventSendServiceJmsBean;
import de.brockhaus.common.event.boundary.MessageContentType;
import de.brockhaus.common.event.domain.outbound.EventMessage;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 23, 2024
 *
 */
public class BillingEventSendServiceBeanIntegrationTest {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventSendServiceBeanIntegrationTest.class);
	
	private WeldContainer container;
	private BillingEventSendServiceJmsBean sender;
	
	@Test
	public void testSendEvent() throws Exception {
		
		HashMap<String, Object> msgData = new HashMap<>();
		msgData.put("INVOICE_ID", "99");
		msgData.put("DEED_RELATED", "666");
		msgData.put("INVOICE_AMOUNT", "99,63");
		
		EventMessage event =  new EventMessage.EventMessageBuilder().
				withDomainClass("de.bnotk.wawi.Invoice").
				withDomainObjectId("99").
				withEventId(UUID.randomUUID().toString()).
				withMessageDataType(MessageContentType.JSON).
				withMsgData(msgData).
				withEventClassification("APPFACT_INVOICE_CREATED").
				withSenderIdentifier("APPFACT").
				withTransactionId("ABC-123").
				build();
		
		sender.sendEvent(event);
		
	}

	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		
		// starting the event receiver in AppFact
		sender = container.select(BillingEventSendServiceJmsBean.class).get();
		
	}
	
	@AfterTest
	public void tearDown() {
		LOG.info("Listener down");
		container.close();
	}

}

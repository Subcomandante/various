package de.brockhaus.billing.inbound.boundary;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import de.brockhaus.billing.inbound.control.BillingEventReceiverServiceBean;
import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessage.MessageHeaderProperties;

/**
 * 
 * created by mbohnen at May 28, 2024
 * Brockhaus Consulting GmbH
 */
public class BillingEventReceiverServiceBeanIntegrationTest {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(BillingEventReceiverServiceBeanIntegrationTest.class);
	
	private WeldContainer container;
	private BillingEventReceiverServiceBean receiver;
	
	
	@Test
	public void testOnEvent() throws Exception {
		
		EventMessage msg = new EventMessage();
		
		HashMap<MessageHeaderProperties, String> msgHeaderMap = msg.getMessageHeader();
		msgHeaderMap.put(MessageHeaderProperties.EVENT_CLASSIFICATION, EventClassification.BILLING_INVOICE_CREATED.toString());
		msgHeaderMap.put(MessageHeaderProperties.EVENT_ID, "123");
		msgHeaderMap.put(MessageHeaderProperties.TRANSACTION_ID, "999");
		
		HashMap<String, Object> msgBodyMap = msg.getMessageBody();
		msgBodyMap.put("ProductionOrderID", "ABC-123");

		
		receiver.onEvent(msg);
	}

	@BeforeTest
	public void setUp() {
		
		Weld weld = new Weld();
		container = weld.initialize();
		
		// starting the event receiver in AppFact
		receiver = container.select(BillingEventReceiverServiceBean.class).get();
		
	}
	
	@AfterTest
	public void tearDown() {
		LOG.info("Listener down");
		container.close();
	}
}

package de.brockhaus.common.interceptor.message;

/**
 *
 * @author Bohnen
 *
 */
public class CallTraceLogMessage extends LogMessage {

	private String params;
	private String result;

	public CallTraceLogMessage() {

	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}

package de.brockhaus.common.process;

import java.io.Serializable;

/**
 *
 * @author mbohnen
 *
 * @param <T> the class of the object this process is dealing with
 */
public class GenericProcessResult<T extends Serializable> implements Serializable{

	/** the object(s) this process is dealing with */
	private T processSubject;

	/** was the object successfully processed ? */
	private boolean success;

	/** what was the reason of failure */
	private String error;

	public GenericProcessResult() {
		super();
	}

	public GenericProcessResult(T processSubject, boolean success, String error) {
		super();
		this.processSubject = processSubject;
		this.success = success;
		this.error = error;
	}

	public T getProcessSubject() {
		return processSubject;
	}

	public void setProcessSubject(T processSubject) {
		this.processSubject = processSubject;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}

package de.brockhaus.common.process;

/**
 * The generic action result of a concrete action
 *
 * @author mbohnen
 *
 * @param <T>
 */
public class GenericActionResult<T> {

    private T result;

    public GenericActionResult() {
    }

    public GenericActionResult(T result) {
        this.result = result;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}

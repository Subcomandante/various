package de.brockhaus.common.service.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import de.brockhaus.common.service.domain.AbstractEntity;
import de.brockhaus.common.service.repository.criteria.Criteria;


/**
 * Interface for the GenericDAO. Implementations for managed environments (JavaEE) and for unmanaged environments (JavaSE)
 * 
 * @author mbohnen
 *
 */
public interface GenericRepository {

	/**
	 * Creating a new record if no id exists or merging an existing (id exists)
	 *
	 * Create:
	 * 		Insert a new register to the database
	 * 		Attach the object to the entity manager.
	 *
	 * Merge:
	 * 		Find an attached object with the same id and update it.
	 * 		If exists update and return the already attached object.
	 * 		If doesn't exist insert the new register to the database.
	 *
	 * @param <T>
	 * @param entity
	 * @return
	 * @throws RepositoryException
	 */
    <T extends AbstractEntity> T createOrUpdateEntity(T entity) throws RepositoryException;

    <T extends AbstractEntity> void deleteEntity(T entity) throws RepositoryException;

    // convenience method
    <T extends AbstractEntity> long getIdHighValue(Class<T> clazz);

    // avoiding LazyInitializationExceptions if used
    <T extends AbstractEntity> T findAndLoadEntityWithLazyReferencesById(Class<T> clazz, long id) throws RepositoryException;

    <T extends AbstractEntity> T findEntityById(Class<T> clazz, long id) throws RepositoryException;

    <T extends AbstractEntity> T findEntityByKey(Class<T> clazz, String key) throws RepositoryException;

    <T extends AbstractEntity> List<T> findAll(Class<T> clazz);

    <T extends AbstractEntity> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName,
            Map<String, Object> params);

    public <T> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName);

    <T extends AbstractEntity> List<T> findEntitiesByQueryString(Class<T> clazz, String queryString);

    <T extends AbstractEntity> List<T> findEntitiesByCriteria(Class<T> entityClass,	Collection<Criteria<T>> criterias);

	void checkDatabase(String queryString);

	<T extends AbstractEntity> List<Long> findIds(Class<T> clazz);
	
	EntityManager getEntityManager();

}
package de.brockhaus.common.event.domain.outbound;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 25, 2023
 *
 */
public enum EventMessageStatus {
	MSG_SEND_PENDING, MSG_SEND_OK, MSG_SEND_NOK, MSG_SEND_FAILED;
}

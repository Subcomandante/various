package de.brockhaus.common.interceptor.process;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Priority;
import javax.interceptor.Interceptor;
import javax.interceptor.InterceptorBinding;

/**
 * Flagging a class as a process service.
 *
 * Ensure, the interceptor registered within beans.xml
 *
 * @author Bohnen
 *
 */
@InterceptorBinding
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE})
@Priority(Interceptor.Priority.APPLICATION)
public @interface Process {

}

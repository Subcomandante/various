package de.brockhaus.common.process.action;

import java.io.Serializable;
import java.util.Map;

import de.brockhaus.common.process.GenericActionResult;
import de.brockhaus.common.process.ProcessException;

/**
 * Abstract generic action class to be used within concrete processes
 *
 * @author mbohnen
 *
 * @param <T>
 */
public interface ProcessAction<T> extends Serializable {

	/**
	 * to be implemented by concrete actions
	 *
	 * @param params the parameters needed for execution of the action
	 * @return a parameterized result of the action
	 * @throws ProcessException
	 */
	public GenericActionResult<T> doAction(Map<String, ?> params) throws Exception ;
}

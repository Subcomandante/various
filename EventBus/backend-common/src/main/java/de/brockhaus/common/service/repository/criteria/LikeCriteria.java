package de.brockhaus.common.service.repository.criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author mbohnen
 *
 * @param <T>
 */
public class LikeCriteria<T> implements Criteria<T> {

    private final String fieldName;
    private final String pattern;
    private final boolean negate;
    private final String association;

    public LikeCriteria(String fieldName, String pattern, boolean negate) {
        this.fieldName = fieldName;
        this.pattern = pattern;
        this.negate = negate;
        this.association="";
    }

    public LikeCriteria(String fieldName, String pattern, String association, boolean negate) {
        this.fieldName = fieldName;
        this.pattern = pattern;
        this.negate = negate;
        this.association = association;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Path<String> v = root.get(fieldName);

        if (pattern == null) {
            return negate ? builder.isNotNull(v) : builder.isNull(v);
        } else {
            return negate ? builder.notLike(v, pattern) : builder.like(v, pattern);
        }
    }

    public String getFieldName() {
        return fieldName;
    }

    public boolean isNegate() {
        return negate;
    }

	public String getAssociation() {
		return association;
	}

	public String getPattern() {
		return pattern;
	}

	@Override
	public String getCriteriaType() {
		return "LIKE_CRITERIA";
	}
}

package de.brockhaus.common.interceptor;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Tracing the threads accessing a method flagged with that annotation
 *
 * @author mbohnen
 *
 */
@Interceptor
@ThreadTracer
public class ThreadTracerInterceptor extends AbstractInterceptor implements Serializable {

	private static final Logger LOG = LogManager.getLogger(ThreadTracerInterceptor.class);

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {

		// getting data from InvocationContext
		String clazz = ctx.getTarget().getClass().getSimpleName();

		// in case we have some WELD proxies
		String targetClassName = clazz.substring(0, clazz.indexOf('$'));

		// method invoked
		String method = ctx.getMethod().getName();

		try {
			LOG.debug("{} Entering {}.{}() by thread {}", LocalDateTime.now(), targetClassName, method, Thread.currentThread().getName());

			// go ahead
			return ctx.proceed();

		} finally {
			LOG.debug("{} Leaving {}.{}() by thread {}", LocalDateTime.now(), targetClassName, method, Thread.currentThread().getName());
		}

	}

}

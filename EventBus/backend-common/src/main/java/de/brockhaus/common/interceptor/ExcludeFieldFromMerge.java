package de.brockhaus.common.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Priority;
import javax.interceptor.Interceptor;
import javax.interceptor.InterceptorBinding;

/**
 * if a field is annotated, the merge() method of AbstractEntity will exclude it from merging
 * one entity into another.
 *
 * @author mbohnen,
 * Brockhaus Consulting GmbH, May 21, 2023
 *
 */
@InterceptorBinding
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.TYPE })
@Priority(Interceptor.Priority.APPLICATION)
public @interface ExcludeFieldFromMerge {

}

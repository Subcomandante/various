package de.brockhaus.common.interceptor;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.interceptor.message.CallTraceLogMessage;
import de.brockhaus.common.interceptor.message.LogMessage.Direction;


/**
 * Interceptor for tracking the incoming and outgoing parameters / result
 *
 * @author mbohnen
 *
 */
@Interceptor
@CallTracer
public class CallTracerInterceptor extends AbstractInterceptor implements Serializable {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(CallTracerInterceptor.class);

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {
		CallTraceLogMessage msg = (CallTraceLogMessage) super.createMessage(this.getClass(), ctx, CallTraceLogMessage.class);
		msg.setDirection(Direction.IN);

		Object[] data = ctx.getParameters();
		String params = this.resolveContextData(data);
		msg.setParams(params);

		Object result = null;
		try {
			result = ctx.proceed();
			LOG.info(msg.toString());
			// go ahead
			return result;

		} finally {
			if(result != null) {
				msg.setResult(result.toString());
			} else {
				msg.setResult("No result, empty");
			}
			msg.setDirection(Direction.OUT);
			LOG.info(msg.toString());
		}

	}

	private String resolveContextData(Object[] data) {
		String params = "";
		for (Object param : data) {
			params = params + param.toString() + " | " ;
		}
		return params;
	}
}

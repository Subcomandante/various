package de.brockhaus.common.util.i18n;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Ansel
 *
 *
 */
public class I18NMessageResolver {

	private static final String MESSAGES_BUNDLE_NAME = "de.kommone.wibas.heimarbeit.messages";
//	private static final String LABELS_BUNDLE_NAME = "de.kommone.wibas.heimarbeit.labels";
//	private static final String VALIDATION_BUNDLE_NAME = "de.kommone.wibas.heimarbeit.validation";
	private static final ResourceBundle MESSAGES_BUNDLE = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME);
//	private static final ResourceBundle LABELS_BUNDLE = ResourceBundle.getBundle(LABELS_BUNDLE_NAME);
//	private static final ResourceBundle VALIDATION_BUNDLE = ResourceBundle.getBundle(VALIDATION_BUNDLE_NAME);

	private I18NMessageResolver() {
	}

	public static String getMessage(String key) {
		try {
			return MESSAGES_BUNDLE.getString(key);

		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	public static String getMessage(String key, Object... params) {
		try {
			return MessageFormat.format(MESSAGES_BUNDLE.getString(key), params);

		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

//	public static String getValidationLength(String key) {
//		try {
//			return VALIDATION_BUNDLE.getString(key);
//
//		} catch (MissingResourceException e) {
//			return '!' + key + '!';
//		}
//	}
//
//	public static String getLabel(String key) {
//		try {
//			return LABELS_BUNDLE.getString(key);
//
//		} catch (MissingResourceException e) {
//			return '!' + key + '!';
//		}
//	}
//
//	public static String getLabel(String key, Object... params) {
//		try {
//			return MessageFormat.format(LABELS_BUNDLE.getString(key), params);
//
//		} catch (MissingResourceException e) {
//			return '!' + key + '!';
//		}
//	}
}

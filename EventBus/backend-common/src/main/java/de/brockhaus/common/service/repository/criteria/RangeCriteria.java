package de.brockhaus.common.service.repository.criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author mbohnen
 *
 * @param <T>
 * @param <V>
 */
public class RangeCriteria<T, V extends Comparable<? super V>> implements Criteria<T> {

    private final String fieldName;
    private final V low;
    private final V high;
    private final boolean negate;
    private final String association;

    public RangeCriteria(String fieldName, V low, V high, boolean negate) {
        this.fieldName = fieldName;
        this.low = low;
        this.high = high;
        this.negate = negate;
        this.association="";
    }

    public RangeCriteria(String fieldName, V low, V high, String association, boolean negate) {
        this.fieldName = fieldName;
        this.low = low;
        this.high = high;
        this.negate = negate;
        this.association = association;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Path<V> v = root.get(fieldName);

        if (low != null && high != null) {
            Predicate between = builder.between(v, low, high);
            return negate ? between.not() : between;

        } else if (low != null) {
            Predicate ge = builder.greaterThanOrEqualTo(v, low);
            return negate ? ge.not() : ge;

        } else if (high != null) {
            Predicate le = builder.lessThanOrEqualTo(v, high);
            return negate ? le.not() : le;

        } else {
            return builder.conjunction();
        }
    }

    public String getFieldName() {
        return fieldName;
    }

    public V getLow() {
        return low;
    }

    public V getHigh() {
        return high;
    }

    public boolean isNegate() {
        return negate;
    }

	public String getAssociation() {
		return association;
	}

	@Override
	public String getCriteriaType() {
		return "RANGE_CRITERIA";
	}
}

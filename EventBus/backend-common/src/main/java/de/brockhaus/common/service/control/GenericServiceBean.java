package de.brockhaus.common.service.control;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import de.brockhaus.common.service.boundary.GenericService;
import de.brockhaus.common.service.domain.AbstractEntity;
import de.brockhaus.common.service.repository.GenericRepository;
import de.brockhaus.common.service.repository.RepositoryException;
import de.brockhaus.common.service.repository.criteria.Criteria;



/**
 *
 * @author mbohnen
 *
 */
public abstract class GenericServiceBean implements GenericService {

    // the concrete DAO will be injected by concrete class
    protected GenericRepository repository;

    public GenericServiceBean() {
        // lazy
    }

    // used 4 Mockito
    public GenericServiceBean(GenericRepository dao) {
        this.repository = dao;
    }

    // must be overwritten by concrete class
    public abstract void setRepository();

    // if this method is overwritten, don't forget to call it explicitly at this
    // class
    @PostConstruct
    public void setUp() {
        this.setRepository();
    }

    @Override
    public <T extends AbstractEntity> T createOrUpdateEntity(T entity) throws RepositoryException {
        return this.repository.createOrUpdateEntity(entity);
    }

    @Override
    public <T extends AbstractEntity> void deleteEntity(T entity) throws RepositoryException {
        this.repository.deleteEntity(entity);
    }

    @Override
    public <T extends AbstractEntity> long getIdHighValue(Class<T> clazz) {
        return this.repository.getIdHighValue(clazz);
    }

    @Override
    public <T extends AbstractEntity> T findAndLoadEntityWithLazyReferencesById(Class<T> entityClass, long id)
            throws RepositoryException {
        return this.repository.findAndLoadEntityWithLazyReferencesById(entityClass, id);
    }

    @Override
    public <T extends AbstractEntity> T findEntityById(Class<T> entityClass, long id) throws RepositoryException {
        return this.repository.findEntityById(entityClass, id);
    }

    @Override
    public <T extends AbstractEntity> T findEntityByKey(Class<T> entityClass, String key) throws RepositoryException {
        return this.repository.findEntityByKey(entityClass, key);
    }

    @Override
    public <T extends AbstractEntity> List<T> findAll(Class<T> entityClass) {
        return this.repository.findAll(entityClass);
    }

    @Override
    public <T extends AbstractEntity> List<T> findEntitiesByNamedQuery(Class<T> entityClass, String queryName,
            Map<String, Object> params) {
        return this.repository.findEntitiesByNamedQuery(entityClass, queryName, params);
    }

    public <T> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName) {
    	return this.repository.findEntitiesByNamedQuery(clazz, queryName);
    }

    @Override
    public <T extends AbstractEntity> List<T> findEntitiesByQueryString(Class<T> entityClass, String queryString) {
    	return this.repository.findEntitiesByQueryString(entityClass, queryString);
    }

    @Override
    public <T extends AbstractEntity> List<T> findEntitiesByCriteria(Class<T> entityClass,
			Collection<Criteria<T>> criterias) {
    	return this.repository.findEntitiesByCriteria(entityClass, criterias);
    }

    @Override
    public <T extends AbstractEntity> List<Long> findIds(Class<T> clazz) {
    	return this.repository.findIds(clazz);

    }
}

package de.brockhaus.common.interceptor.message;

/**
 *
 * @author Bohnen
 *
 */
public class ValidationLogMessage extends LogMessage {

	private boolean passed;

	// what will be validated
	private String object;

	public ValidationLogMessage() {

	}

	public boolean isPassed() {
		return passed;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

}

package de.brockhaus.common.service.repository.criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author mbohnen
 *
 * @param <T>
 */
public interface Criteria<T> {

    Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder);

    public enum CriteriaType {EQUAL_CRITERIA, LIKE_CRITERIA, IN_CRITERIA, RANGE_CRITERIA};

	String getCriteriaType();

}

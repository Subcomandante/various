package de.brockhaus.common.event.domain.outbound;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 14, 2023
 *
 */
public enum EventClassification {

	PRODUCTION_ORDER_CREATED, PRODUCTION_ORDER_FINISHED, PRODUCTION_ORDER_STOPPED, PRODUCTION_ORDER_SHIPPED, BILLING_INVOICE_CREATED;
}

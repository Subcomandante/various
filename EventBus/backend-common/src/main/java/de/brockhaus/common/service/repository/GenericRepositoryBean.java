package de.brockhaus.common.service.repository;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.service.boundary.GenericService;
import de.brockhaus.common.service.domain.AbstractEntity;
import de.brockhaus.common.service.repository.criteria.Criteria;

/**
 * The generic DAO implementation for unmanaged environments (Java SE)
 *
 * Creating an entity manager per conversation (i.e. transaction); context will
 * be created and destroyed with your entity manager. check here:
 * https://stackoverflow.com/questions/10762974/should-jpa-entity-manager-be-closed
 *
 * Normally it is best to create a new EntityManager for each transaction to
 * avoid have stale objects remaining in the persistence context, and to allow
 * previously managed objects to garbage collect.
 *
 * After a successful commit the EntityManager can continue to be used, and all
 * of the managed objects remain managed. However it is normally best to close
 * or clear the EntityManager to allow garbage collection and avoid stale data.
 * If the commit fails, then the managed objects are considered detached, and
 * the EntityManager is cleared.
 *
 * @author Bohnen
 *
 */
@Singleton
public abstract class GenericRepositoryBean implements GenericService, GenericRepository {

	private static final Logger LOG = LogManager.getLogger(GenericRepositoryBean.class);

	/**
	 * get the entity manager defined per domain from a specific DAO
	 *
	 * @return
	 */
	public abstract EntityManager getEntityManager();

	// get the name of the persistence unit in use here
	protected abstract String getPersistenceUnit();

	/*
	 * (non-Javadoc)
	 *
	 * @see de.kommone.service.entity.GenericDAO#createOrUpdateEntity(T)
	 */
	@Override
	public synchronized <T extends AbstractEntity> T createOrUpdateEntity(T entity) throws RepositoryException {

		if (!Objects.nonNull(entity)) {
			throw new RepositoryException("No entity provided, nothing to create or update");
		}

		String clazzName = entity.getClass().getSimpleName();
		EntityManager entityManager = this.getEntityManager();

		// creation date will be set on instantiation
		EntityTransaction tx = entityManager.getTransaction();

		if (!entityManager.isOpen() || !tx.isActive()) {
			LOG.warn(
					"EntityManager closed {} or transaction active {} prior to persist: {}, id: {} this is a serious warning as it should NOT happen under any circumstances",
					entityManager.isOpen(), tx.isActive(), clazzName, entity.getId());
		}

		try {

			tx.begin();

			// as we have no id yet, it must be freshly brewed
			if (null == entity.getId()) {
				entityManager.persist(entity);

				String msg = String.format("SAVED %s entity with id %s, Thread: %s, transaction is active: %s",
						clazzName, entity.getId(), Thread.currentThread().getName(), tx.isActive());
				LOG.debug(msg);

			} else {
				entity = entityManager.merge(entity);
				String msg = String.format(
						"MERGED (updated) %s entity with id %s, Thread: %s, transaction is active: %s", clazzName,
						Thread.currentThread().getName(), entity.getId(), tx.isActive());
				LOG.debug(msg);
			}

			tx.commit();

		} catch (Exception e) {

			String msg = String.format("Creation failed on %s entity with id %s, Thread: %s, transaction is active: %s",
					clazzName, entity.getId(), Thread.currentThread().getName(), tx.isActive());
			LOG.error(msg, e);

			if (null != tx && tx.isActive()) {
				tx.rollback();
				LOG.warn("Transaction rolled back, entity: {}, Thread: {}, transaction is active: {}", entity.getId(),
						Thread.currentThread().getName(), tx.isActive());
			}

			throw new RepositoryException(msg, e);

		} finally {

			this.closeEntityManager(entityManager);
			LOG.debug("Finally -> EntityManager is open: {}, Transaction is active: {}, entity: {},  Thread: {}",
					entityManager.isOpen(), tx.isActive(), entity.getId(), Thread.currentThread().getName());
		}

		return entity;
	}


	@Override
	public synchronized <T extends AbstractEntity> void deleteEntity(T entity) throws RepositoryException {

		if (!Objects.nonNull(entity)) {
			throw new RepositoryException("No entity provided for database operation");
		}

		Long id = entity.getId();

		EntityManager entityManager = this.getEntityManager();

		// re-attaching to persistence context by finding
		entity = (T) entityManager.find(entity.getClass(), id);

		if (entity == null) {
			LOG.warn("The entity about to be deleted does not exist within DB, will continue without deleting");
			this.closeEntityManager(entityManager);

		} else {

			EntityTransaction tx = entityManager.getTransaction();

			try {

				tx.begin();
				entityManager.remove(entity);
				tx.commit();

				String msg = String.format("Deleted %s entity with id %s, transaction is active: %s",
						entity.getClass().getSimpleName(), id, tx.isActive());
				LOG.debug(msg);

			} catch (Exception e) {
				String msg = String.format("Error on deleting %s entity with id %s, transaction is active: %s",
						entity.getClass().getSimpleName(), id, tx.isActive());

				LOG.error(msg, e);

				if (null != tx && tx.isActive()) {
					tx.rollback();
					LOG.debug("Transaction rolled back, transaction still active: ", tx.isActive());
				}

				throw new RepositoryException(msg, e);

			} finally {
				this.closeEntityManager(entityManager);
			}
		}
	}

	@Override
	public <T extends AbstractEntity> long getIdHighValue(Class<T> clazz) {

		LOG.debug("getIdHighValue 4 class: {}", clazz.getSimpleName());

		EntityManager entityManager = this.getEntityManager();

		Query q = entityManager.createQuery("SELECT coalesce(MAX(e.id), 0) FROM " + clazz.getSimpleName() + " e");
		Object result = q.getSingleResult();

		this.closeEntityManager(entityManager);

		return (Long) result;
	}


	/*
	 * avoiding LazyInitializationExceptions if used, lot of HokeyPokey used
	 *
	 * (non-Javadoc)
	 *
	 * @see
	 * de.kommone.service.entity.GenericDAO#findAndLoadEntityWithLazyReferencesById(
	 * java.lang.Class, long)
	 */
	@Override
	public synchronized <T extends AbstractEntity> T findAndLoadEntityWithLazyReferencesById(Class<T> clazz, long id)
			throws RepositoryException {

		LOG.debug("findAndLoadEntityWithLazyReferencesById: {}, {}", clazz.getName(), id);

		EntityManager entityManager = this.getEntityManager();

		T entity = entityManager.find(clazz, id);

		if (entity != null) {
			// getting all fields of the object
			List<Field> myFields = Arrays.asList(clazz.getDeclaredFields()) ;

			// for each field we need to ...
			for (Field field : myFields) {
				LOG.trace("Checking field: {} ", field.getName());
				// ... determine the type and ...
				Class<?> fieldType = field.getType();

				// ... do the needful (check, if we have a collection as field type)
				if (Collection.class.isAssignableFrom(fieldType)) {
					try {
						// generating a string which represents the method name
						String getterMethodName = this.generateGetterMethodNameFromFieldName(field.getName());
						LOG.trace("getter by field: {}", getterMethodName);

						// generating a string which represents the method name
						String setterMethodName = this.generateSetterMethodNameFromFieldName(field.getName());
						LOG.trace("setter by field: {}", setterMethodName);

						Method getterMethod = clazz.getMethod(getterMethodName);
						Method setterMethod = clazz.getMethod(setterMethodName, getterMethod.getReturnType());

						Class<?>[] paramTypes = setterMethod.getParameterTypes();
						// hopefully just one parameter
						Class<?> paramType = paramTypes[0];

						// invoking the getter method to get the collection
						Collection<?> returnFromGetter = (Collection<?>) getterMethod.invoke(entity);

						// if there are elements ...
						if (CollectionUtils.isNotEmpty(returnFromGetter)) {
							// ... 'touch' the first one
							returnFromGetter.iterator().next();

						} else {
							// unfortunately we can't touch the first element ... and still might get that
							// LazyInitializationException. So we will put a plain new Collection to the
							// field so we get rid of the PersistentBag of Hibernate
							LOG.trace("Collection {} of type {} is empty", field.getName(), field.getClass());

							// hopefully nothing but List
							if (List.class.isAssignableFrom(returnFromGetter.getClass())) {

								if(null != paramType) {
									setterMethod.invoke(entity, new ArrayList<>());
								}
							} else {
								throw new RepositoryException("Collection can't be initialized, maybe not of type List");
							}
						}

					} catch (Exception e) {
						throw new RepositoryException(
								"Failed to find with lazy references for: " + clazz.getSimpleName() + ", id: " + id, e);
					}
				}
			}
		}

		this.closeEntityManager(entityManager);

		return entity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.kommone.service.entity.GenericDAO#findEntityById(java.lang.Class,
	 * long)
	 */
	@Override
	public synchronized <T extends AbstractEntity> T findEntityById(Class<T> clazz, long id) throws RepositoryException {
		LOG.debug("findEntityById: {}, {}", clazz.getSimpleName(), id);

		EntityManager entityManager = this.getEntityManager();

		T entity = entityManager.find(clazz, id);

		if (entity == null) {
			LOG.warn("No entity found for id: {} ", id);
		}

		this.closeEntityManager(entityManager);

		return entity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.kommone.service.entity.GenericDAO#findEntityByKey(java.lang.Class,
	 * java.lang.String)
	 */
	@Override
	public synchronized <T extends AbstractEntity> T findEntityByKey(Class<T> clazz, String key) throws RepositoryException {
		LOG.debug("findEntityByKey: {}, {}", clazz.getSimpleName(), key);

		EntityManager entityManager = this.getEntityManager();

		TypedQuery<T> query = entityManager.createQuery("FROM " + clazz.getSimpleName() + " AS c WHERE c.key = :key",
				clazz);
		query.setParameter("key", key);

		T result = query.getSingleResult();

		if (result == null) {
			LOG.warn("No entity found by key: {}", key);
			throw new RepositoryException("No entity found for key: " + key);
		}

		entityManager.refresh(result);
		LOG.debug("Entity found for key {}: {}", key, result);

		this.closeEntityManager(entityManager);

		return result;
	}


	@Override
	public synchronized <T extends AbstractEntity> List<T> findAll(Class<T> clazz) {

		LOG.debug("findAll: {}", clazz.getSimpleName());

		EntityManager entityManager = this.getEntityManager();

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<T> query = cb.createQuery(clazz);
	    Root<T> root = query.from(clazz);
	    query.select(root);
	    List<T> result = entityManager.createQuery(query).getResultList();

		this.closeEntityManager(entityManager);

		LOG.info("Number of entities found: {}" , result.size());

		return result;
	}

	/**
	 *
	 * @param <T>
	 * @param entityClass
	 * @param criterias
	 * @return
	 */
	public synchronized <T extends AbstractEntity> List<T> findEntitiesByCriteria(Class<T> entityClass,
			Collection<Criteria<T>> criterias) {
		LOG.debug("findEntitiesByCriteria: {}", entityClass.getName());

		if (criterias == null || criterias.size() == 0) {
			return this.findAll(entityClass);
		}

		EntityManager entityManager = this.getEntityManager();

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		Root<T> root = criteriaQuery.from(entityClass);

		Predicate[] predicates = criterias.stream().
				map(c -> c.toPredicate(root, criteriaQuery, criteriaBuilder)).
				toArray(Predicate[]::new);

		Predicate conjunction = criteriaBuilder.and(predicates);

		TypedQuery<T> query = entityManager.createQuery(criteriaQuery.where(conjunction));

		List<T> result = query.getResultList();

		LOG.debug("found: {}", result.size());

		this.closeEntityManager(entityManager);

		return result;
	}


	@Override
	public synchronized <T extends AbstractEntity> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName,
			Map<String, Object> params) {

		LOG.debug("findEntitiesByNamedQuery: {}, {}", clazz.getName(), queryName);

		EntityManager entityManager = this.getEntityManager();

		TypedQuery<T> query = entityManager.createNamedQuery(queryName, clazz);

		if (params != null) {
			for (String paramName : params.keySet()) {
				query.setParameter(paramName, params.get(paramName));
			}
		}

		List<T> result = query.getResultList();

		if (result.size() == 0) {
			LOG.warn("No entity found for named query: {} (but this might be intended)", queryName);
		}

		LOG.debug("Number of entities found by: {} results: {}", queryName, result.size());

		this.closeEntityManager(entityManager);

		return result;
	}

	public synchronized <T> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName) {

		LOG.debug("findEntitiesByNamedQuery: {}, {}", clazz.getName(), queryName);

		EntityManager entityManager = this.getEntityManager();

		TypedQuery<T> query = entityManager.createNamedQuery(queryName, clazz);

		List<T> result = query.getResultList();

		if (result.size() == 0) {
			LOG.warn("No entity found for named query: {} (but this might be intended)", queryName);
		}

		LOG.debug("Number of entities found by: {} results: {}", queryName, result.size());

		this.closeEntityManager(entityManager);

		return result;
	}

	@Override
	public synchronized <T extends AbstractEntity> List<T> findEntitiesByQueryString(Class<T> clazz, String queryString) {
		LOG.debug("findEntitiesByQueryString: {}, {}", clazz.getName(), queryString);

		EntityManager entityManager = this.getEntityManager();

		TypedQuery<T> query = entityManager.createQuery(queryString, clazz);

		List<T> result = query.getResultList();
		LOG.debug("Number of entities found by {} -> {}", queryString, result.size());

		this.closeEntityManager(entityManager);

		return result;
	}

	@Override
	public synchronized <T extends AbstractEntity> List<Long> findIds(Class<T> clazz) {
		LOG.debug("findEntitiesIds: {}", clazz.getName());

		List<Long> ids = new ArrayList<>();
		EntityManager entityManager = this.getEntityManager();

		String clazzName = clazz.getSimpleName();
		String query = "SELECT o.id FROM " + clazzName + " AS o";

		Query q = entityManager.createQuery(query);
		ids = q.getResultList();

		LOG.debug("ids found: {}", ids.size());

		return ids;
	}

	@Override
	public void checkDatabase(String queryString) {
		LOG.debug("checkDatabase: {}, {}", queryString);

		EntityManager entityManager = this.getEntityManager();
		Query query = entityManager.createNativeQuery(queryString);

		List hits = query.getResultList();

		LOG.info("Health check produces {} hits", hits.size());

		this.closeEntityManager(entityManager);
	}

	/**
	 * Do the things needed to get a 'getFoo()' from a field named 'foo'
	 *
	 * @param fieldName the field name
	 * @return the getter method name for the give field name
	 */
	private String generateGetterMethodNameFromFieldName(String fieldName) {
		return "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	private String generateSetterMethodNameFromFieldName(String fieldName) {
		return "set" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	/**
	 * closing the entity manager. It is recommended to close it after every
	 * operation
	 *
	 * @param entityManager
	 */
	private void closeEntityManager(EntityManager entityManager) {
		if (entityManager.isOpen()) {
			entityManager.close();
			LOG.debug("EntityManager closed by: {}", Thread.currentThread().getName());

		} else {
			LOG.warn("EntityManager was already closed (Thread: {})", Thread.currentThread().getName());
		}
	}
}

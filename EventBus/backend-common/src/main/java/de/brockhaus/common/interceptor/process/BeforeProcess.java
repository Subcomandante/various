package de.brockhaus.common.interceptor.process;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Priority;
import javax.interceptor.Interceptor;
import javax.interceptor.InterceptorBinding;

/**
 * Flag the method, you want to invoke automatically before
 * the process starts
 *
 * Bear in mind that the parameters of this method must be the same
 * as the parameters of the method flagged with @ProcessMethod
 *
 * @author Bohnen
 *
 */
@InterceptorBinding
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD})
@Priority(Interceptor.Priority.APPLICATION)
public @interface BeforeProcess {

}

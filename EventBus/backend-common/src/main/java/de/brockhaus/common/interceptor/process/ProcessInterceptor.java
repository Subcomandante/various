package de.brockhaus.common.interceptor.process;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A class annotated with @Process will be scanned for the annotations @ProcessMethod, @BeforeProcess and
 * @AfterProcess. If a method annotated with @ProcessMethod is found, the invocation of this method will
 * be 'woven' around with the invocations of the methods annotated with @BeforeProcess and @AfterProcess.
 *
 * @author mbohnen
 *
 */
@Interceptor
@Process
public class ProcessInterceptor  implements Serializable {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(ProcessInterceptor.class);

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {

		// if not flagged as a process method just continue
		if (ctx.getMethod().getAnnotation(ProcessMethod.class) == null) {
			Object result = ctx.proceed();
			return result;
		}

		Class<? extends Object> clazz = ctx.getTarget().getClass();

		// getting all methods of the process class
		Method[] methods = clazz.getDeclaredMethods();

		for (Method method : methods) {

			// if flagged with @BeforeProcess
			if(method.getAnnotation(BeforeProcess.class) != null) {
				try {
					LOG.debug("invocation of @BeforeProcess at {}.{}", clazz.getSimpleName(), ctx.getMethod().getName());
					Parameter[] invocationParams = method.getParameters();

					if(invocationParams.length > 0) {

						method.invoke(ctx.getTarget(), ctx.getParameters());

					} else {

						method.invoke(ctx.getTarget());
					}

				} catch (Exception e) {
					LOG.error(e);
					throw e;
				}
			}
		}

		// go ahead with the @ProcessMethod annotated method
		LOG.debug("invocation of @ProcessMethod at {}.{}", clazz.getSimpleName(), ctx.getMethod().getName());
		Object result = ctx.proceed();

		for (Method method : methods) {
			// if flagged with @AfterProcess
			if(method.getAnnotation(AfterProcess.class) != null) {
				try {
					LOG.debug("invocation of @AfterProcess at {}.{}", clazz.getSimpleName(), ctx.getMethod().getName());
					Parameter[] invocationParams = method.getParameters();

					if(invocationParams.length > 0) {

						method.invoke(ctx.getTarget(), ctx.getParameters());

					} else {

						method.invoke(ctx.getTarget());
					}

				} catch (Exception e) {
					LOG.error(e);
					throw e;
				}
			}
		}

		return result;
	}
}

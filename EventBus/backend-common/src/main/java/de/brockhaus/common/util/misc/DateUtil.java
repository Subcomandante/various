package de.brockhaus.common.util.misc;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Bohnen
 *
 */
public class DateUtil {

    private static String pattern = "yyyy-MM-dd HH:mm:ss:SSS";

    public static LocalDateTime getDateFromMillies(long millies) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millies);
        LocalDateTime date = LocalDateTime.ofInstant(c.toInstant(), ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        String text = date.format(formatter);
        LocalDateTime parsedDate = LocalDateTime.parse(text, formatter);
        return parsedDate;
    }

    public static LocalDateTime getCurrentDate() {
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        String text = date.format(formatter);
        LocalDateTime parsedDate = LocalDateTime.parse(text, formatter);
        return parsedDate;
    }

    // Converts XML Gregorian Date to SQL Date
    public static Date convertXMLGregorianDateToSQLDate(XMLGregorianCalendar xmlCalendar) {
        Date convertedDate = new Date(xmlCalendar.toGregorianCalendar().getTime().getTime());
        return convertedDate;
    }

    // Converts java.sql.Date to XMLGregorianCalendar Date
    public static XMLGregorianCalendar convertSQLDateToXMLGregorian(Date sqlDate) throws DatatypeConfigurationException {
        GregorianCalendar xmlDate = new GregorianCalendar();
        xmlDate.setTime(sqlDate);
        XMLGregorianCalendar convertedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(xmlDate);
        return convertedDate;
    }

    // Converts java.util.Date to XMLGregorianCalendarDate
    public static XMLGregorianCalendar convertJavaUtilDateToXMLGregorian(java.util.Date date)
            throws DatatypeConfigurationException {
        GregorianCalendar xmlDate = new GregorianCalendar();
        xmlDate.setTime(date);
        XMLGregorianCalendar convertedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(xmlDate);
        return convertedDate;
    }

    // Converts LocalDateTime to XMLGregorianCalendar Date
    public static XMLGregorianCalendar convertLocalDateTimeToXMLGregorian(LocalDateTime date)
            throws DatatypeConfigurationException {
        GregorianCalendar xmlDate = new GregorianCalendar();
        xmlDate.setTime(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()));
        XMLGregorianCalendar convertedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(xmlDate);
        return convertedDate;
    }

    public static java.util.Date convertLocalDateTimeToDate(LocalDateTime date) {
    	return java.util.Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
    }
}

package de.brockhaus.common.util.misc;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessage;

/**
 * performing on the fly validation where needed
 *
 * @author mbohnen
 *
 */
public class ValidationUtil<T> {

	//just a logger
	private static final Logger LOG = LogManager.getLogger(ValidationUtil.class);

	public boolean isValid(T toBeChecked) throws ValidationException {
		boolean passed = false;

		ValidatorFactory factory = javax.validation.Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> errors = validator.validate(toBeChecked);

		if (! errors.isEmpty()) {
			LOG.info("Validation success: {} -> {}", toBeChecked.getClass().getSimpleName(), passed);

			StringBuilder strBuilder = new StringBuilder();
			for (ConstraintViolation<Object> constraintViolation : errors) {
				LOG.info(new ParameterizedMessage("Violation {}: {} -> {} ", constraintViolation.getRootBean().getClass().getSimpleName(), constraintViolation.getPropertyPath(),
						constraintViolation.getMessage()));

				strBuilder.append("\n Constraint violation: " + constraintViolation.getRootBean().getClass().getSimpleName() + ": " + constraintViolation.getPropertyPath() + " "
						+ constraintViolation.getMessage());
			}

			throw new ValidationException(strBuilder.toString());

		} else {
			passed = true;
		}

		LOG.info("Validation success: {} -> {}", toBeChecked.getClass().getSimpleName(), passed);

		return passed;
	}

}

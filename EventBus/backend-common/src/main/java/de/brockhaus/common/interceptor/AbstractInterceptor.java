package de.brockhaus.common.interceptor;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import de.brockhaus.common.interceptor.message.LogMessage;
import de.brockhaus.common.util.misc.DateUtil;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Nov 8, 2023
 *
 */
public abstract class AbstractInterceptor implements Serializable {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(AbstractInterceptor.class);

	private Map<String, String> mdcContent;

	public AbstractInterceptor() {
		// lazy
	}

	/**
	 * must be implemented by concrete interceptors
	 *
	 * @param ctx
	 * @return
	 * @throws java.lang.Exception
	 */
	public abstract Object onInvocation(InvocationContext ctx) throws java.lang.Exception;

	/**
	 * Convenience method to create a message for the interceptor subclass
	 * @param interceptor
	 * @param ctx
	 * @param msgClazz
	 * @return
	 */
	protected LogMessage createMessage(Class<? extends AbstractInterceptor> interceptor, InvocationContext ctx, Class<? extends LogMessage> msgClazz){
		LogMessage msg = null;

		try {
			this.check4Id();
			// getting data from InvocationContext
			String clazz = ctx.getTarget().getClass().getSimpleName();
			// in case we have some WELD proxies
			String targetClassName = clazz.substring(0, clazz.indexOf('$'));

			String method = ctx.getMethod().getName();

			// creating specific LogMessage
			msg = msgClazz.newInstance();

			msg.setInterceptor(interceptor.getSimpleName());
			msg.setInvocationTime(DateUtil.getCurrentDate().toString());
			msg.setTargetClazz(targetClassName);
			msg.setTargetMethod(method);

			String clientIP = this.getCurrentClientIpAddress();
			ThreadContext.put("clientIP", clientIP);

			msg.setMdcContext(ThreadContext.getImmutableContext());

		} catch (InstantiationException | IllegalAccessException e) {
			LOG.error("Error on instantiation of Log message", e);
		}

		return msg;
	}

	/**
	 * checking, whether we have a unique id already existing in ThreadContext
	 */
	void check4Id() {

		LOG.debug("ThreadContext empty: {}", ThreadContext.isEmpty());

		// do we have the id in MDC?
		if (ThreadContext.get("callId") == null || ThreadContext.get("callId").equals("")) {
			// ... no, generating one
			ThreadContext.put("callId", UUID.randomUUID().toString());
			LOG.debug("No callId in MDC, creating one: {}", ThreadContext.get("callId"));

		} else {
			// yes
			LOG.debug("callId in MDC is: {}", ThreadContext.get("callId"));
		}
	}

	/**
	 * a somewhat nasty hack to get the IP address of the accessing client
	 */
	private String getCurrentClientIpAddress() {
		String currentThreadName = Thread.currentThread().getName();
		LOG.debug("Threadname: {}", currentThreadName);
		int begin = currentThreadName.indexOf('-') + 1;
		String remoteClient = currentThreadName.substring(begin);
		return remoteClient;
	}

	public Map<String, String> getMdcContent() {
		return mdcContent;
	}

	public void setMdcContent(Map<String, String> mdcContent) {
		this.mdcContent = mdcContent;
	}
}

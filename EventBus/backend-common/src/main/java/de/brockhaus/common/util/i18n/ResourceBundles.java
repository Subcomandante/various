package de.brockhaus.common.util.i18n;

/**
 * @author Ansel
 *
 */
public enum ResourceBundles {

    MESSAGES("messages"), LABELS("labels"), VALIDATION("validation");
    private String bundleName;

    ResourceBundles(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getBundleName() {
        return bundleName;
    }

    @Override
    public String toString() {
        return bundleName;
    }
}

package de.brockhaus.common.event.boundary;

import de.brockhaus.common.event.domain.outbound.EventMessage;

/**
 * 
 * Somehow similar to Message interface but technology agnostic an focused on EventMessages
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 15, 2024
 *
 */
public interface EventReceiverAdapter {

	void onEvent(EventMessage eventMessage) throws Exception;
}

package de.brockhaus.common.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.service.control.Initializable;

/**
 * Use this interceptor in case of a class that needs to be initialized prior to accessing it and
 * you want to have control on the things going on.
 *
 * This sometimes is helpful if you want/need to deal with exceptions but the @PostConstruct annotation
 * enforces to deal with them in the method as not allowing to throw an exception.
 *
 * @NeedsInitialization
 * public class FooServiceBean implements Initializable {
 *
 * 		private boolean isInitialized = false;
 *
 * 		@Override
 * 		public void init() {
 * 			// do the needful
 * 			this.isInitialized = true;
 * 		}
 *
 * 		@Override
 * 		public boolean isInitialized() {
 * 			return isInitialized;
 * 		}
 *
 * 		@InitializeBefore
 *  	public void doBar() {
 *  		// do whatever is needed.
 *  		// On every invocation it will be checked, if this class is initialized
 *  	}
 *  }
 *
 *
 * @author mbohnen
 *
 */
@Interceptor
@NeedsInitialization
public class NeedsInitializationInterceptor {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(NeedsInitializationInterceptor.class);

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {

		LOG.debug("NeedsInitializationInterceptor started");

		// is the target implementing Initalizable interface?
		if (ctx.getTarget() instanceof Initializable) {

			LOG.debug("Initialializable instance invoked");
			Initializable target = (Initializable) ctx.getTarget();

			// if not flagged as a method needing initialization just continue
			if (ctx.getMethod().getAnnotation(InitializeBefore.class) != null && ! target.isInitialized()) {
				LOG.debug("Target {} not initialized yet, will initialize", target.getClass().getSimpleName());
				target.init();
			}
		}

		return ctx.proceed();
	}
}

package de.brockhaus.common.util.json;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

/**
 *
 * @author mbohnen
 *
 */
public class JSONParser {

	// just a logger
	private Logger LOG = LogManager.getLogger(JSONParser.class);

	private ObjectMapper objectMapper;

	// Singleton
	public JSONParser() {
		objectMapper = new ObjectMapper();
		this.configureParser();
	}

	private void configureParser() {
		// getting a new instance every time
		this.objectMapper = new ObjectMapper();

		objectMapper.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());

		objectMapper.configure(Feature.AUTO_CLOSE_SOURCE, true);
	}

	public String toJSON(Object o, boolean formatted, boolean defaulTyping) throws JsonProcessingException {
		String json = "";
		try {
			objectMapper.setSerializationInclusion(Include.NON_NULL);

			if (defaulTyping) {
				this.activateDefaultTypingOnCollections();
			}

			if (formatted) {
				json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);

			} else {
				json = objectMapper.writeValueAsString(o);
			}

		} catch (JsonProcessingException e) {
			LOG.error("Error on processing JSON", e);
			throw e;
		}
		return json;
	}

	void activateDefaultTypingOnCollections() {
		PolymorphicTypeValidator ptv = objectMapper.getPolymorphicTypeValidator();
		objectMapper.activateDefaultTyping(ptv);
	}

	public void writeJSONToFile(Object o, File file) throws IOException {
		try {
			objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, o);
		} catch (IOException e) {
			LOG.error("Error on writing JSON 2 file",e);
			throw e;
		}
	}

	public <T> T readJSONFromFile(Class<T> clazz, File file, boolean defaultTyping) throws IOException {
		LOG.debug("converting JSON file {} to {} with default typing on {}", file.getName(), clazz.getSimpleName(),
				defaultTyping);
		try {

			if (defaultTyping) {
				this.activateDefaultTypingOnCollections();
			}

			return objectMapper.readValue(file, clazz);
		} catch (IOException e) {
			LOG.error("Error on reading JSON from file", e);
			throw e;
		}
	}

	/**
	 * De-serializing a JSON string into a java object using the type validator (
	 * not recommended using JSON arrays)
	 * @param <T>
	 * @param clazz
	 * @param json
	 * @return
	 * @throws IOException
	 */
	public <T> T fromJSON(Class<T> clazz, String json) throws IOException {
		try {
			this.activateDefaultTypingOnCollections();
			return objectMapper.readValue(json, clazz);
		} catch (IOException e) {
			LOG.error("Error on deserializing object from JSON", e);
			throw e;
		}
	}

	/**
	 * De-serializing a JSON string into a java object with optional use of the type validator
	 * (for JSON arrays it is recommended to switch it off)
	 * @param <T>
	 * @param clazz
	 * @param typeValidation
	 * @param json
	 * @return
	 * @throws IOException
	 */
	public <T> T fromJSON(Class<T> clazz, boolean typeValidation, String json) throws IOException {
		if (typeValidation) {
			this.activateDefaultTypingOnCollections();;
		}
		return objectMapper.readValue(json, clazz);
	}

	/**
	 * De-serializing a JSON array into a java object (the given List class)
	 * @param <T>
	 * @param elementClass
	 * @param json
	 * @return
	 * @throws IOException
	 */
	public <T> List<T> fromJSON2List(Class<T> elementClass, String json) throws IOException {
		this.configureParser();
		try {
			CollectionType valueType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class,
					elementClass);
			return objectMapper.readValue(json, valueType);

		} catch (IOException e) {
			LOG.error("Error on de-serializing JSON 2 object", e);
			throw e;
		}
	}

	public String toXML(Class<?> clazz, Object obj) throws JAXBException {
		try {
			LOG.debug("Converting to XML instance of: {}", clazz.getSimpleName());

			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			java.io.StringWriter sw = new StringWriter();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(obj, sw);

			return sw.toString();
		} catch (JAXBException e) {
			LOG.error("Error on serializing object 2 XML", e);
			throw e;
		}
	}


	@SuppressWarnings("unchecked")
	public <T> T fromXML(Class<T> clazz, String xml) throws JAXBException {
		try {
			StringReader reader = new StringReader(xml);
			return (T) JAXBContext.newInstance(clazz).createUnmarshaller().unmarshal(reader);

		} catch (JAXBException e) {
			LOG.error("Error on deserializing object from XML", e);
			throw e;
		}
	}

	// last resort for individual configuration
	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	// added for tests
	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper=objectMapper;

	}
}

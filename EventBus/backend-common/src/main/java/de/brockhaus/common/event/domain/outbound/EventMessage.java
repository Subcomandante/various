package de.brockhaus.common.event.domain.outbound;

import java.time.LocalDateTime;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.event.boundary.MessageContentType;
import de.brockhaus.common.util.json.JSONParser;

/**
 * 
 * The message itself as java representation ... 
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Nov 29, 2023
 *
 */
public class EventMessage {
	
	// header data keys
	public enum MessageHeaderProperties {TRANSACTION_ID, EVENT_ID, EVENT_CLASSIFICATION, DOMAINOBJECT_ID, DOMAIN_CLASS, SENDER_IDENTIFIER, REPLY_TO, MESSAGE_DATATYPE, MESSAGE_SCHEMA_URI, CREATED_WHEN };
	
	// header data
	private HashMap<MessageHeaderProperties, String> messageHeader = new HashMap<>(); 
	
	// payload as key/value pairs
	private HashMap<String, Object> messageBody = new HashMap<>();
	
	public EventMessage() {
		
	}
	
	private EventMessage(EventMessageBuilder builder) {
		// envelope data
		this.messageHeader.put(MessageHeaderProperties.TRANSACTION_ID, builder.transactionId);
		this.messageHeader.put(MessageHeaderProperties.EVENT_ID, builder.eventId);
		this.messageHeader.put(MessageHeaderProperties.MESSAGE_DATATYPE, builder.dataType.toString());
		this.messageHeader.put(MessageHeaderProperties.MESSAGE_SCHEMA_URI, builder.messageSchemaUri);
		this.messageHeader.put(MessageHeaderProperties.SENDER_IDENTIFIER, builder.senderIdentifier);
		this.messageHeader.put(MessageHeaderProperties.EVENT_CLASSIFICATION, builder.eventClassification);
		this.messageHeader.put(MessageHeaderProperties.REPLY_TO, builder.replyTo);
		this.messageHeader.put(MessageHeaderProperties.DOMAINOBJECT_ID, builder.domainObjectId);
		this.messageHeader.put(MessageHeaderProperties.DOMAIN_CLASS, builder.domainClass);
		this.messageHeader.put(MessageHeaderProperties.CREATED_WHEN, LocalDateTime.now().toString());
		
		// payload
		this.setMessageBody(builder.msgData);
	}

	

	public HashMap<String, Object> getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(HashMap<String, Object> messageBody) {
		this.messageBody = messageBody;
	}
	
	public HashMap<MessageHeaderProperties, String> getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(HashMap<MessageHeaderProperties, String> headerProperties) {
		this.messageHeader = headerProperties;
	}

	public void setMessageData(HashMap<String, Object> messageData) {
		this.messageBody = messageData;
	}
	
	public String toJson() throws JsonProcessingException {
		return new JSONParser().toJSON(this, true, false);
	}

	public static class EventMessageBuilder {
		
		private String transactionId;
		
		private String eventId;
		
		private String senderIdentifier;
		
		private String eventClassification;
		
		private String domainObjectId;
		
		private String domainClass;
		
		private String replyTo = "some jms destination or URI";
		
		private MessageContentType dataType = MessageContentType.JSON;
		
		private String messageSchemaUri = "Some URI to a schema";
		
		private HashMap<String, Object> msgData;
		
		public EventMessageBuilder withTransactionId(String transactionId) {
			this.transactionId = transactionId;
			return this;
		}
		
		public EventMessageBuilder withEventId(String eventId) {
			this.eventId = eventId;
			return this;
		}
		
		public EventMessageBuilder withDomainObjectId(String domainObjectId) {
			this.domainObjectId = domainObjectId;
			return this;
		}
		
		public EventMessageBuilder withDomainClass(Class domainClass) {
			this.domainClass = domainClass.getCanonicalName();
			return this;
		}
		
		public EventMessageBuilder withSenderIdentifier(String senderIdentifier) {
			this.senderIdentifier = senderIdentifier;
			return this;
		}
		
		public EventMessageBuilder withEventClassification(String eventClassification) {
			this.eventClassification = eventClassification;
			return this;
		}
		
		public EventMessageBuilder withReplyTo(String replyTo) {
			this.replyTo = replyTo;
			return this;
		}
		
		public EventMessageBuilder withMessageDataType(MessageContentType dataType) {
			this.dataType = dataType;
			return this;
		}
		
		public EventMessageBuilder withMessageSchemaUri(String uri) {
			this.messageSchemaUri = uri;
			return this;
		}
		
		public EventMessageBuilder withMsgData(HashMap<String, Object> msgData) {
			this.msgData = msgData;
			return this;
		}
		
		public EventMessageBuilder withDomainClass(String clazzName) {
			this.domainClass = clazzName;
			return this;
		}
		
		public EventMessage build() {
			return new EventMessage(this);
		}	
	}
}

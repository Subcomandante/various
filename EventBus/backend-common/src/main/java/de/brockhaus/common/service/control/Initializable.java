package de.brockhaus.common.service.control;

/**
 * Marker interface to indicate, that the implementing class needs to be initilalized prior to
 * access several things.
 * Make use of this interface in conjunction with @NeedsInitilalization annotation on class level and
 * @InitializeBefore annotation on method level.
 *
 * @author mbohnen
 *
 */
public interface Initializable {

	/**
	 *
	 * @throws Exception
	 */
	void init() throws Exception;

	/**
	 *
	 * @return
	 */
	boolean isInitialized();
}

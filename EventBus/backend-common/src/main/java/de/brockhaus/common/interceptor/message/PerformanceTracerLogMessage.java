package de.brockhaus.common.interceptor.message;

/**
 *
 * @author Bohnen
 *
 */
public class PerformanceTracerLogMessage extends LogMessage {

	private String start;
	private String end;
	private long duration_ms;

	public PerformanceTracerLogMessage() {

	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public long getDuration_ms() {
		return duration_ms;
	}

	public void setDuration_ms(long duration_ms) {
		this.duration_ms = duration_ms;
	}


}

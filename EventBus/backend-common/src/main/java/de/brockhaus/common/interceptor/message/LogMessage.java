package de.brockhaus.common.interceptor.message;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.util.json.JSONParser;



/**
 *
 * @author Bohnen
 *
 */
@XmlRootElement
public abstract class LogMessage {

	private static final Logger LOG = LogManager.getLogger(LogMessage.class);

	public enum Direction {
		IN, OUT
	};

	private String host;
	private Direction direction;
	private String interceptor;
	private String targetClazz;
	private String targetMethod;
	private String invocationTime;
	private Map<String, String> mdcContext;

	public LogMessage() {
		try {
			this.host = Inet4Address.getLocalHost().getHostName() + ":" + Inet4Address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			LOG.error("Problems on resolving host", e);
		}
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public String getInterceptor() {
		return interceptor;
	}

	public void setInterceptor(String interceptor) {
		this.interceptor = interceptor;
	}

	public String getTargetClazz() {
		return targetClazz;
	}

	public void setTargetClazz(String targetClazz) {
		this.targetClazz = targetClazz;
	}

	public String getTargetMethod() {
		return targetMethod;
	}

	public void setTargetMethod(String targetMethod) {
		this.targetMethod = targetMethod;
	}

	public String getInvocationTime() {
		return invocationTime;
	}

	public void setInvocationTime(String invocationTime) {
		this.invocationTime = invocationTime;
	}

	public Map<String, String> getMdcContext() {
		return mdcContext;
	}

	public void setMdcContext(Map<String, String> mdcContext) {
		this.mdcContext = mdcContext;
	}

	@Override
	public String toString() {
		String ret = "";
		try {
			ret = new JSONParser().toJSON(this, false, false);
		} catch (JsonProcessingException e) {
			LOG.error("Error on serializing Object 2 JSON", e);
		}

		return ret;
	}
}

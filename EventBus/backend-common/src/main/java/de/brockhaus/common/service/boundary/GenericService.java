package de.brockhaus.common.service.boundary;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.brockhaus.common.service.domain.AbstractEntity;
import de.brockhaus.common.service.repository.RepositoryException;
import de.brockhaus.common.service.repository.criteria.Criteria;

/**
 *
 * @author Bohnen
 *
 */
public interface GenericService {

    <T extends AbstractEntity> T createOrUpdateEntity(T entity) throws RepositoryException;

    <T extends AbstractEntity> void deleteEntity(T entity) throws RepositoryException;

    <T extends AbstractEntity> long getIdHighValue(Class<T> clazz);

    <T extends AbstractEntity> T findAndLoadEntityWithLazyReferencesById(Class<T> clazz, long id) throws RepositoryException;

    <T extends AbstractEntity> T findEntityById(Class<T> clazz, long id) throws RepositoryException;

    <T extends AbstractEntity> T findEntityByKey(Class<T> clazz, String key) throws RepositoryException;

    <T extends AbstractEntity> List<T> findAll(Class<T> clazz);

    <T extends AbstractEntity> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName,
            Map<String, Object> params);

    public <T> List<T> findEntitiesByNamedQuery(Class<T> clazz, String queryName);

    <T extends AbstractEntity> List<T> findEntitiesByQueryString(Class<T> clazz, String queryString);

    <T extends AbstractEntity> List<T> findEntitiesByCriteria(Class<T> entityClass,	Collection<Criteria<T>> criterias);

	<T extends AbstractEntity> List<Long> findIds(Class<T> clazz);
}

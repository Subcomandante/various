package de.brockhaus.common.event.domain.outbound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.service.domain.AbstractEntity;
import de.brockhaus.common.util.json.JSONParser;

/**
 * 
 * The unique events (e.g. status changes) related to an business aggregate or domain object like a deed.
 * All relevant data for this event should be stored within the event data map.
 * 
 * Maybe a PropertyChangeListener should be implemented to keep all in sync (the HashMap)
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Oct 14, 2023
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class EventLogEntry extends AbstractEntity {
	
	// just a logger
	private static final Logger LOG = LogManager.getLogger(EventLogEntry.class);

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	/** the id related to the business transaction (which might span several services and multiple events) */
	@Column(nullable = false, name = "TRANSACTION_ID")
	private String transactionId;

	/** the id related to this event */
	@Column(nullable = false, unique = true, name = "EVENT_ID")
	private String eventId;

	@Column(nullable = false, unique = true, name = "EVENT_CLASSIFICATION")
	@Enumerated(EnumType.STRING)
	private EventClassification classification;
	
	@Column(nullable = false, unique = true, name = "DOMAINOBJECT_ID")
	private String domainObjectId;
	
	@Column(nullable = false, name = "DOMAINOBJECT_CLASS")
	private String domainObjectClass;
	
	@Transient
	private HashMap<String, Object> eventDataMap = new HashMap<>();
	
	// the event data as JSON for persisting purposes
	@Column(nullable = false, name = "EVENT_DATA")
	@Lob
	private String eventData;
	
	/** association */
	@OneToMany(
			mappedBy = "event",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	@JsonIgnore
	private List<EventMessageLogEntry> msgLogEntries = new ArrayList<>();

	public EventLogEntry() {
		this.eventId = UUID.randomUUID().toString();
	}
	
	public EventLogEntry(EventClassification classification) {
		this();
		this.setClassification(classification);
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Long getId() {
		return id;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public EventClassification getClassification() {
		return classification;
	}

	public void setClassification(EventClassification classification) {
		this.classification = classification;
	}

	public String getDomainObjectId() {
		return domainObjectId;
	}

	public void setDomainObjectId(String domainObjectId) {
		this.domainObjectId = domainObjectId;
	}

	public String getDomainObjectClass() {
		return domainObjectClass;
	}

	public void setDomainObjectClass(String domainObjectClass) {
		this.domainObjectClass = domainObjectClass;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public List<EventMessageLogEntry> getMsgLogEntries() {
		return msgLogEntries;
	}

	public void setMsgLogEntries(List<EventMessageLogEntry> msgLogEntries) {
		this.msgLogEntries = msgLogEntries;
	}
	
	public void setEventDataMap(HashMap<String, Object> eventDataMap) {
		this.eventDataMap =  eventDataMap; 
	}

	public HashMap<String, Object> getEventDataMap() {
		return this.eventDataMap;
	}
	
	@PrePersist
	private void serializeEventDataMap() {
		try {
			this.eventData = new JSONParser().toJSON(eventDataMap, true, false);
		} catch (JsonProcessingException e) {
			LOG.error(e);
		}
	}
	
	@PostLoad
	private void deserializeEventDataMap() {
		try {
			this.eventDataMap= new JSONParser().fromJSON(HashMap.class, this.eventData);
		} catch (IOException e) {
			LOG.error(e);
		}
	}
	
}

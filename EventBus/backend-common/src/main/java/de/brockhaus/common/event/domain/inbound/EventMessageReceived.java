package de.brockhaus.common.event.domain.inbound;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;

import de.brockhaus.common.event.domain.outbound.EventClassification;
import de.brockhaus.common.service.domain.AbstractEntity;

/**
 * 
 * created by mbohnen at Jun 11, 2024
 * Brockhaus Consulting GmbH
 */
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class EventMessageReceived extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	/** the id related to the business transaction (which might span several services and multiple events) */
	@Column(name = "TRANSACTION_ID")
	private String transactionId;

	/** the id related to this event */
	@Column(name = "EVENT_ID")
	private String eventId;

	@Column(name = "EVENT_CLASSIFICATION")
	@Enumerated(EnumType.STRING)
	private EventClassification classification;
	
	@Column(nullable = false, name = "RECEIVED_STATUS")
	@Enumerated(EnumType.STRING)
	private EventMessageReceivedStatus currentStatus;
	
	@Lob
	@Column(name = "NOTE")
	private String note;

	@Lob
	@Column(name = "MESSAGE_DATA")
	private String msgData;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public EventClassification getClassification() {
		return classification;
	}

	public void setClassification(EventClassification classification) {
		this.classification = classification;
	}

	public String getMsgData() {
		return msgData;
	}

	public void setMsgData(String msgData) {
		this.msgData = msgData;
	}

	public EventMessageReceivedStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(EventMessageReceivedStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

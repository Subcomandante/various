package de.brockhaus.common.process;

/**
 *
 * @author mbohnen
 *
 */
public class ProcessException extends Exception {

	public ProcessException() {
		super();
	}

	public ProcessException(String message) {
		super(message);
	}

	public ProcessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ProcessException(String message, Throwable cause) {
		super(message, cause);
	}

}

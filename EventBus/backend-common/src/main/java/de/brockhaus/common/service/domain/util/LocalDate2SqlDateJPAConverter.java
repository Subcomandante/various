package de.brockhaus.common.service.domain.util;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Converting LocalDate to java.sql.Date et vice versa (JPA2.1 does not support
 * LocalDate right now)
 *
 * @author mbohnen
 *
 */
@Converter(autoApply = true)
public class LocalDate2SqlDateJPAConverter implements AttributeConverter<LocalDate, java.sql.Date> {

    @Override
    public java.sql.Date convertToDatabaseColumn(LocalDate locDate) {
        return (locDate == null ? null : Date.valueOf(locDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(java.sql.Date sqlDate) {
        return (sqlDate == null ? null : mapSqlDateToLocalDate(sqlDate));
    }

    private LocalDate mapSqlDateToLocalDate(java.sql.Date sqlDate) {
        java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
        Instant instant = utilDate.toInstant();
        ZonedDateTime zdt = instant.atZone(ZoneId.of("Europe/Paris"));
        return zdt.toLocalDate();
    }
}

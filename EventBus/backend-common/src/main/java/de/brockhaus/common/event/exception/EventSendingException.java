package de.brockhaus.common.event.exception;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Mar 7, 2024
 *
 */
public class EventSendingException extends Exception {

	public EventSendingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EventSendingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EventSendingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EventSendingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EventSendingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

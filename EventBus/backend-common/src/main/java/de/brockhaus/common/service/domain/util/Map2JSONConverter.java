package de.brockhaus.common.service.domain.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.brockhaus.common.util.json.JSONParser;

/**
 * A custom validator allowing to store a Map as JSON
 *
 * @author mbohnen
 *
 */
public class Map2JSONConverter implements AttributeConverter<Map<String, String>, String> {

	// just a logger
	private static final Logger LOG = LogManager.getLogger(Map2JSONConverter.class);

	@Override
	public String convertToDatabaseColumn(Map<String, String> attribute) {
		String json = "not converted";
		try {
			json = new JSONParser().toJSON(attribute, true, true);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	@Override
	public Map<String, String> convertToEntityAttribute(String dbData) {
		Map<String, String> ret = null;
		try {
			ObjectMapper mapper = new JSONParser().getObjectMapper();
			TypeReference<HashMap<String, String>> typeRef
			  = new TypeReference<HashMap<String, String>>() {};
			ret = mapper.readValue(dbData, typeRef);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

}

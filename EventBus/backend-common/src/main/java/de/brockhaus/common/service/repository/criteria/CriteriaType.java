package de.brockhaus.common.service.repository.criteria;

public enum CriteriaType {


	EQUAL_CRITERIA("EqualCriteria"),
	LIKE_CRITERIA("LikeCriteria"),
	IN_CRITERIA("InCriteria"),
	RANGE_CRITERIA("RangeCriteria");


	String type;

	CriteriaType(String type) {
		this.type = type;
	}
}

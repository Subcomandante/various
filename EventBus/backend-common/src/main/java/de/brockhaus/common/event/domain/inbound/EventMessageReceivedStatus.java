package de.brockhaus.common.event.domain.inbound;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 25, 2023
 *
 */
public enum EventMessageReceivedStatus {
	MSG_RECEIVED_OK, MSG_RECEIVED_NOK;
}

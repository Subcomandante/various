package de.brockhaus.common.event.boundary;

public enum MessageContentType {
	JSON, XML, CSV, PLAIN_TEXT;
}

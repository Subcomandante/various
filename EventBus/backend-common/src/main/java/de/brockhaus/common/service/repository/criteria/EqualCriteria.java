/*
 * Copyright 2019 - KOMMONE Anstalt des öffentlichen Rechts, Krailenshaldenstraße 44, 70469 Stuttgart
 * Die Weitergabe und Vervielfältigung dieser Software oder Teile daraus sind ohne die ausdrückliche Genehmigung der KOMMONE AöR nicht gestattet.
 */
package de.brockhaus.common.service.repository.criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author mbohnen
 *
 * @param <T>
 * @param <V>
 */
public class EqualCriteria<T, V extends Comparable<? super V>> implements Criteria<T> {

    private final String fieldName;
    private final V value;
    private final String association;
    private final boolean negate;
    public EqualCriteria(String fieldName, V value, String association, boolean negate) {
		super();
		this.fieldName = fieldName;
		this.value = value;
		this.association = association;
		this.negate = negate;
	}

    public EqualCriteria(String fieldName, V value, boolean negate) {
        this.fieldName = fieldName;
        this.value = value;
        this.negate = negate;
        this.association="";
    }


	@Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Path<V> v = root.get(fieldName);

        if (value == null) {
            return negate ? builder.isNotNull(v) : builder.isNull(v);
        } else {
            return negate ? builder.notEqual(v, value) : builder.equal(v, value);
        }
    }

	public String getFieldName() {
        return fieldName;
    }

    public V getValue() {
        return value;
    }

    public boolean isNegate() {
        return negate;
    }

    public String getAssociation() {
		return association;
	}

	@Override
	public String getCriteriaType() {
		return "EQUAL_CRITERIA";

	}

}

package de.brockhaus.common.event.boundary;

import de.brockhaus.common.event.domain.outbound.EventMessage;

/**
 * Common interface 4 all specific adapters (JMS, REST, whatever) 2 send events
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Oct 14, 2023
 *
 */
public interface EventSenderAdapter {
	
	void sendEvent(EventMessage event) throws Exception;
}
package de.brockhaus.common.service.domain;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import de.brockhaus.common.interceptor.ExcludeFieldFromMerge;

/**
 *
 * @author Bohnen
 *
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

	// just a logger
	private static Logger LOG = LogManager.getLogger(AbstractEntity.class);

	/** optimistic locking */
	@Version
	@Column(name = "VERSION")
	@ExcludeFieldFromMerge
	private long version;

	/** the business key */
	@Column(nullable = false, unique = true, name = "BUSINESS_KEY")
	protected String bizKey;

	/** when was the object created */
	@Column(name = "RECORD_CREATED_WHEN")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@ExcludeFieldFromMerge
	@JsonIgnore
	private LocalDateTime dateOfCreation;

	/** when was the last update */
	@Column(name = "LAST_UPDATE_WHEN")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@ExcludeFieldFromMerge
	@JsonIgnore
	private LocalDateTime lastUpdate;

	// who created this entity
	@Column(name = "USER")
	@JsonIgnore
	private String userId;

	/**
	 * this is an optional cache to have some place 2 store whatever objects, will
	 * NOT be persisted
	 */
	@Transient
	@JsonIgnore
	private HashMap<String, ArrayList<Object>> cache = new HashMap<String, ArrayList<Object>>();

	public AbstractEntity() {
		// creating the business key as per default
		this.bizKey = UUID.randomUUID().toString();

		// initializing the date of creation
		this.dateOfCreation = LocalDateTime.now();

		// initializing userId
		this.setUserId(null);
	}

	public abstract Long getId();

	public abstract void setId(Long id);

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		// if no user provided ...
		if (userId == null) {
			// ... get him from the system
			this.userId = System.getProperty("user.name");

		} else {
			this.userId = userId;
		}
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public void setDateOfCreation(LocalDateTime creationDate) {
		this.dateOfCreation = creationDate;
	}

	public String getBizKey() {
		return bizKey;
	}

	public LocalDateTime getDateOfCreation() {
		return dateOfCreation;
	}

	@XmlTransient
	public HashMap<String, ArrayList<Object>> getCache() {
		return cache;
	}

	public void setCache(HashMap<String, ArrayList<Object>> cache) {
		this.cache = cache;
	}

	// use this in case you don't want the UUID as default
	public void setBizKey(String bizKey) {
		this.bizKey = bizKey;
	}

	@PrePersist
	@PreUpdate
	private void setLastUpdate() {
		// if not updated but created
		if (null == this.lastUpdate) {
			this.lastUpdate = this.dateOfCreation;

		} else {
			this.lastUpdate = LocalDateTime.now();
		}
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hcb = new HashCodeBuilder();
		// not using id coz might not be set yet
		hcb.append(this.bizKey);

		return hcb.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (!(obj instanceof AbstractEntity)) {
			return false;
		}

		AbstractEntity that = (AbstractEntity) obj;
		EqualsBuilder eb = new EqualsBuilder();

		// not using id coz might not be set yet
		eb.append(this.getBizKey(), that.getBizKey());

		return eb.isEquals();
	}

	/**
	 * merge one entity into another
	 *
	 * @param otherEntity
	 */
	public void merge(AbstractEntity otherEntity) {

		// avoid appleas and peas
		assert this.getClass().isAssignableFrom(otherEntity.getClass());
//		this.getClass().getName().equals(otherEntity.getClass().getName());

		for (Field thisField : this.getClass().getDeclaredFields()) {

			if (! thisField.isAnnotationPresent(ExcludeFieldFromMerge.class)) {

				for (Field otherField : otherEntity.getClass().getDeclaredFields()) {

					if (thisField.getName().equals(otherField.getName())) {

						try {
							thisField.setAccessible(true);
							otherField.setAccessible(true);
							thisField.set(this, otherField.get(otherEntity) == null ? thisField.get(this)
									: otherField.get(otherEntity));

						} catch (IllegalAccessException ignore) {
							// Field update exception on final modifier and other cases.
							LOG.warn(ignore);
						}
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		ToStringBuilder tsb = new ToStringBuilder(this);

		tsb.append("id", this.getId());
		tsb.append("key", this.bizKey);

		return tsb.toString();
	}
}

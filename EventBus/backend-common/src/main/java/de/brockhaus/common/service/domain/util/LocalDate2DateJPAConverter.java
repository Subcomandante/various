package de.brockhaus.common.service.domain.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.AttributeConverter;

/**
 * Converting LocalDate to java.util.Date et vice versa (JPA2.1 does not support
 * LocalDate right now)
 *
 * @author mbohnen
 *
 */
public class LocalDate2DateJPAConverter implements AttributeConverter<LocalDate, Date> {

    ZoneId defaultTimeZone = ZoneId.of("Europe/Paris");

    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
        return (locDate == null ? null : Date.from(locDate.atStartOfDay(defaultTimeZone).toInstant()));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date date) {
        return (date == null ? null : date.toInstant().atZone(defaultTimeZone).toLocalDate());
    }

}

package de.brockhaus.common.process.action;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.process.GenericActionResult;
import de.brockhaus.common.process.ProcessException;
import de.brockhaus.common.service.repository.GenericRepository;

/**
 * 2b finished
 *
 * @author mbohnen
 *
 * @param <T>
 * @param <V> <V implements GenericRepository>
 */
public class GenericFindByIdAction<T, V extends GenericRepository> implements ProcessAction<T>{

	// just a logger
	private static final Logger LOG = LogManager.getLogger(GenericFindByIdAction.class);

	private V repository;

	@Override
	public GenericActionResult<T> doAction(Map<String, ?> params) throws ProcessException {

		// TODO Auto-generated method stub
		return null;
	}

	public V getRepository() {
		return repository;
	}

	public void setRepository(V repository) {
		this.repository = repository;
	}

}

/*
 * Copyright 2019 - KOMMONE Anstalt des öffentlichen Rechts, Krailenshaldenstraße 44, 70469 Stuttgart
 * Die Weitergabe und Vervielfältigung dieser Software oder Teile daraus sind ohne die ausdrückliche Genehmigung der KOMMONE AöR nicht gestattet.
 */
package de.brockhaus.common.util.misc;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @copyright KOMMONE AöR
 * @author Stefan Spangenberg
 * @since 20.08.2019
 */
public class DateTimeUiUtils {

    public static final String DATE_PATTERN = "dd.MM.yyyy";
    public static final String DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm:ss";

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

    public static LocalDate parseLocalDate(String text) {
        return DATE_FORMATTER.parse(text, LocalDate::from);
    }

    public static LocalDateTime parseLocalDateTime(String text) {
        return DATE_TIME_FORMATTER.parse(text, LocalDateTime::from);
    }

    public static LocalDateTime getLocalDateAtTime(LocalDate localDate, LocalTime localTime) {
        return localDate.atTime(localTime);
    }

    public static String formatLocalDate(LocalDate localDate) {
        return DATE_FORMATTER.format(localDate);
    }

    public static String formatLocalDateTime(LocalDateTime localDateTime) {
        return DATE_TIME_FORMATTER.format(localDateTime);
    }

    public static Date convertLocalDateTimeToDate(LocalDateTime dateToConvert) {
        Instant instant = dateToConvert.atZone(ZoneId.systemDefault())
                .toInstant();
        return Date.from(instant);
    }

    public static Date convertLocalDateToDate(LocalDate dateToConvert, LocalTime localTime) {
        LocalDateTime localDateTime = getLocalDateAtTime(dateToConvert, localTime);
        return convertLocalDateTimeToDate(localDateTime);
    }
}

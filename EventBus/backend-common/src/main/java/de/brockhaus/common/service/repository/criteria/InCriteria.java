package de.brockhaus.common.service.repository.criteria;

import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author mbohnen
 *
 * @param <T>
 * @param <V>
 */
public class InCriteria<T, V extends Comparable<? super V>> implements Criteria<T> {

    private final String fieldName;
    private final Collection<V> values;
    private final boolean negate;
    private final String association;

    public InCriteria(String fieldName, Collection<V> values, boolean negate) {
        this.fieldName = fieldName;
        this.values = values;
        this.negate = negate;
        this.association="";
    }

    public InCriteria(String fieldName, Collection<V> values, String association, boolean negate) {
        this.fieldName = fieldName;
        this.values = values;
        this.negate = negate;
        this.association = association;
    }

	@Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Path<V> v = root.get(fieldName);
        In<V> in = builder.in(v);

        for (V value : values) {
            in = in.value(value);
        }

        return negate ? in.not() : in;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Collection<V> getValues() {
        return values;
    }

    public boolean isNegate() {
        return negate;
    }

    public String getAssociation() {
		return association;
	}

    @Override
	public String getCriteriaType() {
		return "IN_CRITERIA";
	}
}

package de.brockhaus.common.event.domain.outbound;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.service.domain.AbstractEntity;

/**
 * Event message tracking
 * 
 * @author mbohnen, Brockhaus Consulting GmbH, Nov 24, 2023
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@NamedQueries(
		@NamedQuery(name = "FIND_UNSEND", 
		query = "SELECT m FROM EventMessageLogEntry AS m WHERE m.eventMsgStatus = de.brockhaus.common.event.domain.outbound.EventMessageStatus.MSG_SEND_PENDING OR m.eventMsgStatus = de.brockhaus.common.event.domain.outbound.EventMessageStatus.MSG_SEND_NOK")
		)
public abstract class EventMessageLogEntry extends AbstractEntity {
	
	// named query
	public static final String FIND_UNSEND = "FIND_UNSEND";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, name = "EVENTMSG_CREATED_WHEN")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime createdWhen = LocalDateTime.now();

	@Column(nullable = false, name = "EVENTMSG_STATUS")
	@Enumerated(EnumType.STRING)
	private EventMessageStatus eventMsgStatus;
	
	@Column(nullable = false, name = "EVENT_CLASSIFICATION")
	@Enumerated(EnumType.STRING)
	private EventClassification classification;
	
	@Column(nullable = false)
	private int sendAttempts;
	
	/** what to send */
	@Lob
	@Column(name = "MESSAGE")
	private String payLoad;
	
	/** anything special, an exception maybe? */
	@Lob
	@Column(name = "REMARKS")
	private String remarks;
	
	/** association */
	@ManyToOne(fetch = FetchType.EAGER)
	private EventLogEntry event;

	
	public EventMessageLogEntry() {
		
	}
	
	public EventMessageLogEntry(EventMessageStatus eventMsgStatus, EventLogEntry event) throws JsonProcessingException {
		this.eventMsgStatus = eventMsgStatus;
		this.event = event;
		this.classification = event.getClassification();
		
		String payload = this.createMessagePayload();
		this.setPayLoad(payload);
	}
	
	// must be concretized by subclasses
	protected abstract String createMessagePayload () throws JsonProcessingException;

	public LocalDateTime getCreatedWhen() {
		return createdWhen;
	}

	public void setCreatedWhen(LocalDateTime createdWhen) {
		this.createdWhen = createdWhen;
	}

	public EventMessageStatus getEventMsgStatus() {
		return eventMsgStatus;
	}

	public void setEventMsgStatus(EventMessageStatus eventMsgStatus) {
		this.eventMsgStatus = eventMsgStatus;
	}

	public EventMessageStatus getEventStatus() {
		return eventMsgStatus;
	}

	public void setEventStatus(EventMessageStatus eventStatus) {
		this.eventMsgStatus = eventStatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPayLoad() {
		return payLoad;
	}

	public void setPayLoad(String payLoad) {
		this.payLoad = payLoad;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}

	public EventClassification getClassification() {
		return classification;
	}

	public void setClassification(EventClassification classification) {
		this.classification = classification;
	}

	public EventLogEntry getEvent() {
		return event;
	}

	public void setEvent(EventLogEntry event) {
		this.event = event;
	}

	public int getSendAttempts() {
		return sendAttempts;
	}

	public void setSendAttempts(int sendAttempts) {
		this.sendAttempts = sendAttempts;
	}
}

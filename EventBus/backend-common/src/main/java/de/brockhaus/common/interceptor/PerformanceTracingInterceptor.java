package de.brockhaus.common.interceptor;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.brockhaus.common.interceptor.message.PerformanceTracerLogMessage;
import de.brockhaus.common.interceptor.message.LogMessage.Direction;
import de.brockhaus.common.util.misc.DateUtil;

/**
 * Interceptor for tracking the time elapsed between invocation and result
 *
 * @author Bohnen
 *
 */
@Interceptor
@PerformanceTracer
public class PerformanceTracingInterceptor extends AbstractInterceptor implements Serializable {

	// just a logger
	private static final Logger LOG = LogManager.getLogger("PerformanceLogger");

	// logging in- and outbound
	private boolean logInboundAndOutbound = false;

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {
		long start = System.currentTimeMillis();

		PerformanceTracerLogMessage msg = (PerformanceTracerLogMessage) super.createMessage(this.getClass(), ctx, PerformanceTracerLogMessage.class);
		msg.setStart(DateUtil.getCurrentDate().toString());
		msg.setDirection(Direction.IN);

		try {
			if(logInboundAndOutbound) {
				LOG.debug(msg.toString());
			}
			return ctx.proceed();

		} finally {
			long end = System.currentTimeMillis();
			long duration = end - start;

			msg.setEnd(DateUtil.getCurrentDate().toString());
			msg.setDuration_ms(duration);
			msg.setDirection(Direction.OUT);
			LOG.debug(msg);
		}
	}

	public void setLogInboundAndOutbound(boolean logInboundAndOutbound) {
		this.logInboundAndOutbound = logInboundAndOutbound;

	}
}

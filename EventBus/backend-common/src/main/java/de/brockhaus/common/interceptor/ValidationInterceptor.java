package de.brockhaus.common.interceptor;

import java.io.Serializable;
import java.util.Set;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessage;

import de.brockhaus.common.interceptor.message.ValidationLogMessage;
import de.brockhaus.common.interceptor.message.LogMessage.Direction;

/**
 * An interceptor which performs validation beforehand (not when the persistence framework comes into play) ...
 *
 * @author mbohnen
 *
 */
@Interceptor
@Validation
public class ValidationInterceptor extends AbstractInterceptor implements Serializable {

	private static final Logger LOG = LogManager.getLogger(ValidationInterceptor.class);

	@AroundInvoke
	public Object onInvocation(InvocationContext ctx) throws java.lang.Exception {

		ValidationLogMessage msg = (ValidationLogMessage) super.createMessage(this.getClass() , ctx, ValidationLogMessage.class);
		msg.setDirection(Direction.IN);

		// getting all parameters of invocation
		Object[] params = ctx.getParameters();
		boolean passed = true;

		try {
			// validating each object passed as parameter
			for (Object object : params) {
				ValidatorFactory factory = javax.validation.Validation.buildDefaultValidatorFactory();
				Validator validator = factory.getValidator();
				Set<ConstraintViolation<Object>> errors = validator.validate(object);

				msg.setObject(object.toString());

				if(errors.size() != 0) {
					passed = false;
					String errorTrace = "";

					LOG.info("Validation success of {}: {}", object.getClass().getSimpleName(), passed);
					StringBuilder strBuilder = new StringBuilder();

					for (ConstraintViolation<Object> constraintViolation : errors) {
						LOG.info(new ParameterizedMessage("Violation {}: {} -> {} ", constraintViolation.getRootBean().getClass().getSimpleName(), constraintViolation.getPropertyPath(),
								constraintViolation.getMessage()));
						strBuilder.append("Constraint violation -> " + object.getClass().getSimpleName() + ": " + constraintViolation.getPropertyPath() + " " + constraintViolation.getMessage() + "\n");
					}

					throw new ValidationException(strBuilder.toString());

				} else {
					LOG.info("Validation success of {}: {}", object.getClass().getSimpleName(), passed);
				}
			}

			return ctx.proceed();

		} finally {
			msg.setPassed(passed);
		}
	}
}

package de.brockhaus.common.event;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.brockhaus.common.event.boundary.MessageContentType;
import de.brockhaus.common.event.domain.outbound.EventMessage;
import de.brockhaus.common.event.domain.outbound.EventMessage.MessageHeaderProperties;
import de.brockhaus.common.util.json.JSONParser;

/**
 * 
 * @author mbohnen, 
 * Brockhaus Consulting GmbH, Feb 16, 2024
 *
 */
public class EventMessageTest {

	@Test
	public void testJsonSerialization() throws JsonProcessingException {
		EventMessage msg = new EventMessage();
		
		msg.getMessageHeader().put(MessageHeaderProperties.MESSAGE_DATATYPE, MessageContentType.JSON.toString());
		msg.getMessageBody().put("ISSUERFIRSTNAME", "John");
		
		String json = new JSONParser().toJSON(msg, true, false);
		System.out.println(json);
		
	}
}

package de.brockhaus.bigdata.cassandra.util;

/**
 * https://www.datastax.com/documentation/cql/3.0/cql/cql_reference/cql_data_types_c.html
 * 
 * Project: cassandra.helloWorld
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Mar 22, 2015
 *
 */
public enum CassandraDataType {
	
	TEXT(0), TIMESTAMP(1);
	
	private final int value;

    private CassandraDataType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}

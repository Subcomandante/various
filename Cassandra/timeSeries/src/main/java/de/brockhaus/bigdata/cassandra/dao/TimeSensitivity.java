package de.brockhaus.bigdata.cassandra.dao;

/**
 * Determine how the data will be stored ...
 * 
 * Project: cassandra.sensordata
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Mar 27, 2015
 *
 */
public enum TimeSensitivity {

	day, hour, minute, second, millisecond;
}

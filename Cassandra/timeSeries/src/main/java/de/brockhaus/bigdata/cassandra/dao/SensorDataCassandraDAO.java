package de.brockhaus.bigdata.cassandra.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import de.brockhaus.bigdata.cassandra.util.CassandraDBUtil;

/**
 * The concrete DAO for Cassandra
 * 
 * The DDL for the table is:
 * CREATE TABLE sensor_data ( sensor_id text, datatype text, time timestamp, value text,  PRIMARY KEY(sensor_id, time));
 * 
 * Project: cassandra.sensordata
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Mar 27, 2015
 *
 */
public class SensorDataCassandraDAO implements SensorDataDAO {

	private TimeSensitivity sensitivity;

	public void insertSensorData(SensorDataTO data) {
		Session session = CassandraDBUtil.INSTANCE.connect();

		String cql = "INSERT INTO sensor_data(SENSOR_ID, DATATYPE, TIME, VALUE) VALUES(?, ?, ?, ?)";

		PreparedStatement pStat = session.prepare(cql);
		BoundStatement bStat = new BoundStatement(pStat);
		bStat.bind(data.getSensorid(), data.getDatatype(), data.getTime(),
				data.getValue());
		session.execute(bStat);
	}

	public void bulkInsertOfSensorData(List<SensorDataTO> list) {
		for (SensorDataTO sensorDataTO : list) {
			this.insertSensorData(sensorDataTO);
		}
	}
	
	public List<SensorDataTO> findBySensorIDAndTimeInterval(String sensorId, Date from, Date to) {
		
		Session session = CassandraDBUtil.INSTANCE.connect();
		List<SensorDataTO> tos = new ArrayList<SensorDataTO>();
		
		// TODO: checkout if necessary
		Timestamp fromTs = new Timestamp(from.getTime());
		Timestamp toTs = new Timestamp(to.getTime());
		
		String cql = "SELECT * FROM sensor_data WHERE SENSOR_ID = ? AND TIME > ? AND TIME < ?";

		PreparedStatement pStat = session.prepare(cql);
		BoundStatement bStat = new BoundStatement(pStat);
		bStat.bind(sensorId, fromTs, toTs);
		
		ResultSet res = session.execute(bStat);

		for (Row row : res) {
			SensorDataTO sensorDataTO = new SensorDataTO();
			sensorDataTO.setDatatype(row.getString("DATATYPE"));
			sensorDataTO.setSensorid(row.getString("SENSOR_ID"));
			sensorDataTO.setTime(row.getDate("TIME"));
			sensorDataTO.setValue(row.getString("VALUE"));
			
			tos.add(sensorDataTO);		 
		}
		
		return tos;
	}
	
	public List<SensorDataTO> findByTimeInterval(Date from, Date to) {
		
		Session session = CassandraDBUtil.INSTANCE.connect();
		List<SensorDataTO> tos = new ArrayList<SensorDataTO>();
		
		// TODO: checkout if necessary
		Timestamp fromTs = new Timestamp(from.getTime());
		Timestamp toTs = new Timestamp(to.getTime());
		
		String cql = "SELECT * FROM sensor_data WHERE TIME > ? AND TIME < ?";

		PreparedStatement pStat = session.prepare(cql);
		BoundStatement bStat = new BoundStatement(pStat);
		bStat.bind(fromTs, toTs);
		
		ResultSet res = session.execute(bStat);

		for (Row row : res) {
			SensorDataTO sensorDataTO = new SensorDataTO();
			sensorDataTO.setDatatype(row.getString("DATATYPE"));
			sensorDataTO.setSensorid(row.getString("SENSOR_ID"));
			sensorDataTO.setTime(row.getDate("TIME"));
			sensorDataTO.setValue(row.getString("VALUE"));
			
			tos.add(sensorDataTO);		 
		}
		
		return tos;
	}	
}

package de.brockhaus.bigdata.cassandra.dao;

import java.io.Serializable;
import java.util.Date;

/**
 * The transfer object related to sensor data
 * 
 * Project: cassandra.sensordata
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Mar 27, 2015
 *
 */
public class SensorDataTO implements Serializable {

	private String sensorid;
	private Date time;
	private String datatype;
	private String value;
	
	public SensorDataTO() {
		
	}

	public SensorDataTO(String sensorid, Date time, String datatype, String value) {
		super();
		this.sensorid = sensorid;
		this.time = time;
		this.datatype = datatype;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSensorid() {
		return sensorid;
	}

	public void setSensorid(String sensorid) {
		this.sensorid = sensorid;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
}

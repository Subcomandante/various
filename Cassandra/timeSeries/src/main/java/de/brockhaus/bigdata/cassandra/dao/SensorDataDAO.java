package de.brockhaus.bigdata.cassandra.dao;

import java.util.Date;
import java.util.List;

/**
 * Just the interface for all sensor data DAOs regardless which implementation is used.
 * 
 * Project: cassandra.sensordata
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Mar 27, 2015
 *
 */
public interface SensorDataDAO {

	void insertSensorData(SensorDataTO data);
	
	void bulkInsertOfSensorData(List<SensorDataTO> list);
	
	List<SensorDataTO> findBySensorIDAndTimeInterval(String sensorId, Date from, Date to);
	
	List<SensorDataTO> findByTimeInterval(Date from, Date to);
}

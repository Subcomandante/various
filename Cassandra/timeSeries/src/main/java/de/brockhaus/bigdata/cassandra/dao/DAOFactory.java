package de.brockhaus.bigdata.cassandra.dao;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * A service to get access to the DAO ... will be replaced by WELD/CDI
 * 
 * Project: cassandra.sensordata
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Mar 27, 2015
 *
 */
@Singleton
public class DAOFactory {
	
	@Inject 
	private SensorDataDAO dao;
	
	public SensorDataDAO getSensorDataDAO() {
		return dao;
	}

}

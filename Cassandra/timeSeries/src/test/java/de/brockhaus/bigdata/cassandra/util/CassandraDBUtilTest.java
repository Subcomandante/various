package de.brockhaus.bigdata.cassandra.util;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CassandraDBUtilTest {
	
	public static void main(String[] args) {
		CassandraDBUtil util = CassandraDBUtil.INSTANCE;
		
		Map<String, CassandraDataType> columns = new HashMap();
		columns.put("sensor_id", CassandraDataType.TEXT);
		columns.put("time", CassandraDataType.TIMESTAMP);
		columns.put("datatype", CassandraDataType.TEXT);
		columns.put("value", CassandraDataType.TEXT);
		
		List<String> pks = new ArrayList<String>();
		pks.add("sensor_id");
		pks.add("time");
		
		util.createTableWithCompoundKey("sensor_data", pks, columns);
	}

}

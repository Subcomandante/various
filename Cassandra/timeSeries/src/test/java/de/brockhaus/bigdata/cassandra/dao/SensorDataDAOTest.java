package de.brockhaus.bigdata.cassandra.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.weld.environment.se.Weld;

public class SensorDataDAOTest {
	
	private static SensorDataDAO dao;

	public static void main(String[] args) {
		
		dao = new Weld().initialize().instance().select(DAOFactory.class).get().getSensorDataDAO();
		SensorDataDAOTest test = new SensorDataDAOTest();
		
//		test.testInsertData();
//		test.testInsertDataByList();
		
		test.testFindDataBySensorIDAndTime();
	}
	
	private void testFindDataBySensorIDAndTime() {
		List<SensorDataTO> hits = dao.findBySensorIDAndTimeInterval("ABC123", new Date(System.currentTimeMillis() - 1000000000), new Date(System.currentTimeMillis()));
		for (SensorDataTO sensorDataTO : hits) {
			System.out.println(sensorDataTO.getSensorid() + " | " + sensorDataTO.getValue() + " | " + sensorDataTO.getTime());
		}
		
	}

	public void testInsertData() {
		SensorDataTO to = new SensorDataTO();
		to.setDatatype("FLOAT");
		to.setSensorid("ABC123");
		to.setTime(new Date(System.currentTimeMillis()));
		to.setValue("1234.99");
		
		dao.insertSensorData(to);
	}
	
	public void testInsertDataByList() {
		
		List<SensorDataTO> list = new ArrayList<SensorDataTO>();
		Date time = new Date(System.currentTimeMillis());
		
		SensorDataTO to1 = new SensorDataTO();
		to1.setDatatype("FLOAT");
		to1.setSensorid("ABC001");
		to1.setTime(time);
		to1.setValue("1000.00");
		
		list.add(to1);
		
		SensorDataTO to2 = new SensorDataTO();
		to2.setDatatype("FLOAT");
		to2.setSensorid("ABC002");
		to2.setTime(time);
		to2.setValue("1000.00");
		
		list.add(to2);
		
		SensorDataTO to3 = new SensorDataTO();
		to3.setDatatype("FLOAT");
		to3.setSensorid("ABC003");
		to3.setTime(time);
		to3.setValue("1000.00");
		
		list.add(to3);
		
		dao.bulkInsertOfSensorData(list);
		
	}
}

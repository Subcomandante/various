package de.brockhaus.messaging.activemq;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * This is the asynchronous implementation of a receiver.
 * You need to implement the MessageListener interface and set this instance as a listener
 * to the MessageConsumer, that's it.
 * 
 * Alternatively you can make use of consumer.receive(), you will get a synchronous receiver then.
 * 
 * Project: activemq.helloWorld
 *
 * Copyright (c) by Brockhaus Group
 * www.brockhaus-gruppe.de
 * @author mbohnen, Apr 9, 2015
 *
 */
public class AsyncTopicReceiver implements MessageListener{

	private ConnectionFactory factory;
	private Connection connection;
	private Session session;
	private Destination destination;
	private MessageConsumer consumer;
	
	public static void main(String[] args) {
		AsyncTopicReceiver receiver = new AsyncTopicReceiver();
		receiver.init();
		
		System.out.println("Receiver is up");
	}
	
	private void init() {
		
		try {
			factory = new ActiveMQConnectionFactory("tcp://localhost:61616?jms.useAsyncSend=true");
//			factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
			connection = factory.createConnection();
			connection.start();

			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createTopic("myTopic");
			consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);
		} 
		catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/** synchronously */
	private void receiveMessage() {
		try {
			Message message = consumer.receive();
			if (message instanceof TextMessage) {
				TextMessage text = (TextMessage) message;
				System.out.println("Message received: \n" + text.getText());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	/** asynchronously */
	public void onMessage(Message message) {
		try {
			if (message instanceof TextMessage) {
				TextMessage text = (TextMessage) message;
				System.out.println("Message is : " + text.getText());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}	
	}
}

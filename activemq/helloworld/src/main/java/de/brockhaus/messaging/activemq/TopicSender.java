package de.brockhaus.messaging.activemq;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;


/**
 * Ensure the infrastructure is working ;-)
 * 
 * Project: activemq.helloWorld
 *
 * Copyright (c) by Brockhaus Group www.brockhaus-gruppe.de
 * 
 * @author mbohnen, Apr 9, 2015
 *
 */
public class TopicSender {
	
	private Session session;
	private Destination destination;
	private MessageProducer producer;
	private Connection connection;
	private String destName = "myTopic";

	/**
	 * here we go ...
	 * @param args
	 */
	public static void main(String[] args) {
		TopicSender sender = new TopicSender();

		sender.init();
		sender.doSend();
		sender.shutDownGracefully();
	}

	/**
	 * ensure ActiveMQ is running ...
	 */
	private void init() {

		try {
			// the proprietary way NOT using InitialContext
			ConnectionFactory conFactory= new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
			
			
			// Getting JMS connection from the server and starting it
			connection = conFactory.createConnection();
			connection.start();

			// JMS messages are sent and received using a Session. We will
			// create here a non-transactional session object. If you want
			// to use transactions you should set the first parameter to 'true'
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// a client can create a new Queue or Topic dynamically either by calling 
			// createQueue() or createTopic() on a JMS Session
			destination = session.createTopic(destName);

			// MessageProducer is used for sending messages (as opposed
			// to MessageConsumer which is used for receiving them)
			producer = session.createProducer(destination);
			
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void doSend() {
		try {
			TextMessage message = session.createTextMessage();
			message.setText("Hello ...This is a sample message..sending from: " + this.getClass().getSimpleName());
			producer.send(message);
			System.out.println("Sent: " + message.getText());
			
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** housekeeping */
	private void shutDownGracefully() {
		try {
			this.session.close();
			this.producer.close();
			this.connection.close();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

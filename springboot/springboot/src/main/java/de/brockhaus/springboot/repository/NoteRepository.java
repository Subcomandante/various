package de.brockhaus.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.brockhaus.springboot.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

}

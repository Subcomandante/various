**Some Remarks**
This project is almost copied from: https://www.callicoder.com/spring-boot-rest-api-tutorial-with-mysql-jpa-hibernate/ 
Project was creted using [Spring Initializr](https://start.spring.io/)

To make it run, just invoke: ***mvn spring-boot:run***

Once the application is running, use some kind of tool like [RESTer](https://addons.mozilla.org/en-US/firefox/addon/rester/) (on Firefox) to create the request.



**Recommended reading:**

 - CRUD Repository:
   https://www.concretepage.com/spring-boot/spring-boot-crudrepository-example
 - Optionals (as used within the Controller):
http://www.oracle.com/technetwork/articles/java/java8-optional-2175753.html 

https://www.mkyong.com/java8/java-8-optional-in-depth/
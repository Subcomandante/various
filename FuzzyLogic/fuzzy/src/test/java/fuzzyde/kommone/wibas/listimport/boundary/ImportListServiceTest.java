package fuzzyde.kommone.wibas.listimport.boundary;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.kommone.wibas.listimport.boundary.ImportListService;
import de.kommone.wibas.listimport.control.ImportListServiceBean;
import de.kommone.wibas.listimport.control.MappingResult;
import de.kommone.wibas.listimport.entity.Homeworker;
import de.kommone.wibas.util.JsonParser;

/**
 * 
 * @author mbohnen
 *
 */
public class ImportListServiceTest {
	
	private List<Homeworker>  dataSet = new ArrayList<>();
	
	private ImportListService service;

	private WeldContainer container;

	@Test
	public void testImportList() throws JsonProcessingException {
		MappingResult result = service.importList(dataSet);
		
		String jsonResult = new JsonParser().toJSON(result, true, false);
		System.out.println(jsonResult);
	}
	
	@BeforeClass
	public void setUp() {
		
		container = (WeldContainer) new Weld().initialize();
		service = container.select(ImportListServiceBean.class).get();
		
		// exact match
		Homeworker matthias = new Homeworker();
		matthias.setBirthday(LocalDate.now());
		matthias.setCity("Viernheim");
		matthias.setFirstName("Matthias");
		matthias.setLastName("Bohnen");
		matthias.setHouseNo("28");
		matthias.setStreet("Lorscher Strasse");
		matthias.setZipCode("68519");

		// no match
		Homeworker josef = new Homeworker();
		josef.setBirthday(LocalDate.now());
		josef.setCity("Stuttgart");
		josef.setFirstName("Josef");
		josef.setLastName("Sladitschek");
		josef.setHouseNo("33");
		josef.setStreet("Krailenshaldenstr");
		josef.setZipCode("71125");
		
		// might match
		Homeworker jakub = new Homeworker();
		jakub.setBirthday(LocalDate.now());
		jakub.setCity("Freiburg im Breisgau");
		jakub.setFirstName("Jakob");
		jakub.setLastName("Tomischeck");
		jakub.setHouseNo("12");
		jakub.setStreet("Schwarzwaldstrasse");
		jakub.setZipCode("88998");
		
		Homeworker carola1 = new Homeworker();
		carola1.setBirthday(LocalDate.now());
		carola1.setCity("Karlsruhe");
		carola1.setFirstName("Carola");
		carola1.setLastName("Wacker-Fischer");
		carola1.setHouseNo("66");
		carola1.setStreet("Am Rhein");
		carola1.setZipCode("78945");
		
		Homeworker carola2 = new Homeworker();
		carola2.setBirthday(LocalDate.now());
		carola2.setCity("Bruchsal");
		carola2.setFirstName("Carola");
		carola2.setLastName("Wacker");
		carola2.setHouseNo("12");
		carola2.setStreet("Bergstrasse");
		carola2.setZipCode("78943");
		
		dataSet.add(matthias);
		dataSet.add(josef);
		dataSet.add(jakub);
		dataSet.add(carola1);
		dataSet.add(carola2);
	}

}

package de.brockhaus.fuzzymatcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.intuit.fuzzymatcher.component.MatchService;
import com.intuit.fuzzymatcher.domain.Document;
import com.intuit.fuzzymatcher.domain.Element;
import com.intuit.fuzzymatcher.domain.ElementType;
import com.intuit.fuzzymatcher.domain.Match;

public class SimpleMatch {

	public static void main(String[] args) throws ParseException {
		SimpleMatch sm = new SimpleMatch();
		
//		sm.simpleMatch();
		sm.matchOriginal();
	}

	private void matchOriginal() throws ParseException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
		Date birthday = formatter.parse("19.07.1983");
		
		Document original = new Document.Builder("0").addElement(new Element.Builder<String>().setValue("Matthias").setType(ElementType.NAME).createElement()).
				addElement(new Element.Builder<String>().setValue("Bohnen").setType(ElementType.NAME).createElement()).
				addElement(new Element.Builder<String>().setValue("Lorscher Strasse 28").setType(ElementType.ADDRESS).createElement()).
				addElement(new Element.Builder<Date>().setValue(birthday).setType(ElementType.DATE).createElement()).
				createDocument();
		
		String[][] input = {
		        {"1", "Matthias Otto Bohnen", "Lorscherstrasse 28", "19.07.1963"},
		        {"2", "Mathias Bohnen", "Lorscher Str. 28", "19.07.1963"},
		        {"3", "M. Bohnen", "Lorscherstr", "19.07.1963"},
		        {"4", "Matias O. Bohnen", "Lorscher Straße 28", "19.07.1963"},
		        {"5", "Matthias Otto Bohnen", "Lorscherstrasse 28", "19.07.1963"},
		        {"6", "Josef Sladitschek", "An der Rennbahn 15", "19.07.1963"},
		        {"7", "Jakub Tomiczek", "Komm.ONE Allee 5" , "19.07.1963"},
		        {"8", "Matthias Bohnen", "Lorscher Strasse 28", "19.07.1963"}
		};
		
		List<Document> documentList = Arrays.asList(input).stream().map(contact -> {
		    return new Document.Builder(contact[0])
		            .addElement(new Element.Builder<String>().setValue(contact[1]).setType(ElementType.NAME).createElement())
		            .addElement(new Element.Builder<String>().setValue(contact[2]).setType(ElementType.ADDRESS).createElement())
		            .addElement(new Element.Builder<Date>().setValue(birthday).setType(ElementType.DATE).createElement())
		            .createDocument();
		}).collect(Collectors.toList());
		
		MatchService matchService = new MatchService();
		Map<Document, List<Match<Document>>> result = matchService.applyMatch(original, documentList);
		
		List<Match<Document>> matches = result.get(original);
		for (Match<Document> match : matches) {
			System.out.println("Weight: " + match.getWeight() + " Score: " + match.getScore().toString());
		}
		
	}

	private void simpleMatch() {
		String[][] input = {
		        {"1", "Matthias Otto Bohnen", "Lorscherstrasse 28"},
		        {"2", "Mathias Bohnen", "Lorscher Str. 28"},
		        {"3", "M. Bohnen", "Lorscherstr"},
		        {"4", "Matias O. Bohnen", "Lorscher Straße 28"},
		        {"5", "Matthias Otto Bohnen", "Lorscherstrasse 28"},
		        {"6", "Josef Sladitschek", "An der Rennbahn 15"},
		        {"7", "Jakub Tomiczek", "Komm.ONE Allee 5"},
		        {"8", "Matthias Bohnen", "Lorscher Strasse 28"}
		};
		
		List<Document> documentList = Arrays.asList(input).stream().map(contact -> {
		    return new Document.Builder(contact[0])
		            .addElement(new Element.Builder<String>().setValue(contact[1]).setType(ElementType.NAME).createElement())
		            .addElement(new Element.Builder<String>().setValue(contact[2]).setType(ElementType.ADDRESS).createElement())
		            .createDocument();
		}).collect(Collectors.toList());
		
		MatchService matchService = new MatchService();
		Map<String, List<Match<Document>>> result = matchService.applyMatchByDocId(documentList);
		
		result.entrySet().forEach(entry -> {
		    entry.getValue().forEach(match -> {
		        System.out.println("Data: " + match.getData() + " Matched With: " + match.getMatchedWith() + " Score: " + match.getScore().getResult());
		    });
		});
		
	}
}

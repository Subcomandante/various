package de.kommone.wibas.listimport.boundary;

import java.util.List;
import java.util.Map;

import de.kommone.wibas.listimport.control.MappingResult;
import de.kommone.wibas.listimport.entity.Homeworker;

/**
 * 
 * @author mbohnen
 *
 */
public interface ImportListService {

	public MappingResult importList(List<Homeworker> importList);
}

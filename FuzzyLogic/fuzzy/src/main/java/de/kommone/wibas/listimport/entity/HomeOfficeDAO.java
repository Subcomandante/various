package de.kommone.wibas.listimport.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

/**
 * 
 * @author mbohnen
 *
 */
public class HomeOfficeDAO {

	private List<Homeworker> initialDataSet = new ArrayList<>();

	public void update(Homeworker worker) {

	}

	public void create(Homeworker worker) {

	}

	public List<Homeworker> findHomeworkerByPlaceOfBusinessId(long id) {

		return initialDataSet;
	}

	@PostConstruct
	public void setUp() {

		Homeworker matthias = new Homeworker();
		matthias.setId(1l);
		matthias.setBirthday(LocalDate.now());
		matthias.setCity("Viernheim");
		matthias.setFirstName("Matthias");
		matthias.setLastName("Bohnen");
		matthias.setHouseNo("28");
		matthias.setStreet("Lorscher Strasse");
		matthias.setZipCode("68519");
		
		Homeworker jakub = new Homeworker();
		jakub.setId(2l);
		jakub.setBirthday(LocalDate.now());
		jakub.setCity("Freiburg");
		jakub.setFirstName("Jakub");
		jakub.setLastName("Tomiczek");
		jakub.setHouseNo("12");
		jakub.setStreet("Schwarzwaldstrasse");
		jakub.setZipCode("88998");
		
		Homeworker carola = new Homeworker();
		carola.setId(3l);
		carola.setBirthday(LocalDate.now());
		carola.setCity("Bruchsal");
		carola.setFirstName("Carola");
		carola.setLastName("Wacker");
		carola.setHouseNo("12");
		carola.setStreet("Bergstrasse");
		carola.setZipCode("78943");

		initialDataSet.add(matthias);
		initialDataSet.add(jakub);
		initialDataSet.add(carola);
	}

}

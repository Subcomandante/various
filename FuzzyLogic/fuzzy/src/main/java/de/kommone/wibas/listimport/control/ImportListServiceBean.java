package de.kommone.wibas.listimport.control;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.intuit.fuzzymatcher.component.MatchService;
import com.intuit.fuzzymatcher.domain.Document;
import com.intuit.fuzzymatcher.domain.Element;
import com.intuit.fuzzymatcher.domain.ElementType;
import com.intuit.fuzzymatcher.domain.Match;

import de.kommone.wibas.listimport.boundary.ImportListService;
import de.kommone.wibas.listimport.entity.HomeOfficeDAO;
import de.kommone.wibas.listimport.entity.Homeworker;

/**
 * 
 * @author mbohnen
 *
 */
public class ImportListServiceBean implements ImportListService {
	
	private static final Logger LOG = LogManager.getLogger(ImportListServiceBean.class);
	
	@Inject
	private HomeOfficeDAO dao;
	
//	@Inject
	private MatchService matchService = new MatchService();
	
	public static final String MATCH = "match";
	public static final String MAYBE = "maybe";
	public static final String NOMATCH = "nomatch"; 

	@Override
	public MappingResult importList(List<Homeworker> importList) {
		
		MappingResult result = new MappingResult();
		
		// the list of matches: pk of homeworker in db and imported homeworker
		Map<Long, Homeworker> matchMap = new HashMap<>();
		
		// the list of possible matches (Score < 1.0): pk of homeworker in db and imported homeworker
		Map<Long, Homeworker> mightMatchMap = new HashMap<>();
		
		// list of imported we can't find a match
		List<Homeworker> noMatch = new ArrayList<>();
		
		List<Homeworker> existingHomeworkerList = this.dao.findHomeworkerByPlaceOfBusinessId(99l);
		
		// creating a list of documents to compare to
		List<Document> existingDocumentList = new ArrayList<>();
		for (Homeworker homeworkerExisting : existingHomeworkerList) {
			existingDocumentList.add(this.createDocument(homeworkerExisting));
		}
		
		// find matches for every worker imported against the ones we know (from DB)
		for (Homeworker homeworkerImported : importList) {
			LOG.info("finding match for {}", homeworkerImported.getFirstName()+homeworkerImported.getLastName());
			
			Document workerDoc = this.createDocument(homeworkerImported);
			List<Match<Document>> matches = this.findMatch(workerDoc, existingDocumentList);
			
			if(matches != null && matches.size() == 1) {
				LOG.info("found matches: {} for {} -> PK: {} (match ratio: {})", matches.size(), homeworkerImported.getFirstName()+homeworkerImported.getLastName(), matches.get(0).getMatchedWith().getKey(), matches.get(0).getScore().getResult());
				
				if(matches.get(0).getScore().getResult() == 1.0) {
					matchMap.put(new Long(matches.get(0).getMatchedWith().getKey()), homeworkerImported);
					
				} else {
					mightMatchMap.put(new Long(matches.get(0).getMatchedWith().getKey()), homeworkerImported);
				}
				
			} else if(matches != null && matches.size() >1) {
				LOG.info("found matches: {} for {} ", matches.size(), homeworkerImported.getFirstName()+homeworkerImported.getLastName());
				
			} else {
				LOG.info("no match for {}", homeworkerImported.getFirstName()+homeworkerImported.getLastName());
				noMatch.add(homeworkerImported);	
			}
		}
		
		result.getFullMatch().add(matchMap);
		result.getMightMatch().add(mightMatchMap);
		result.setNoMatch(noMatch);
		
		return result;
	}

	private List<Match<Document>> findMatch(Document imported, List<Document> existimgDocumentList) {
		
		Map<Document, List<Match<Document>>> result = matchService.applyMatch(imported, existimgDocumentList);
		
		return result.get(imported);	
	}

	private Document createDocument(Homeworker worker) {
		
		String id = null;
		
		// id 0 it must be from list els from DB
		if(worker.getId() == 0) {
			id = worker.getFirstName()+worker.getLastName()+worker.getBirthday().toString();
			
		} else {
			id = Long.toString(worker.getId());
		}
		
		//default time zone
		ZoneId defaultZoneId = ZoneId.systemDefault();
		Date birthday = Date.from(worker.getBirthday().atStartOfDay(defaultZoneId).toInstant());
		
		Document ret = new Document.Builder(id)
	            .addElement(new Element.Builder<String>().setValue(worker.getFirstName()).setType(ElementType.NAME).createElement())
	            .addElement(new Element.Builder<String>().setValue(worker.getLastName()).setType(ElementType.NAME).createElement())
	            .addElement(new Element.Builder<String>().setValue(worker.getCity()).setType(ElementType.ADDRESS).createElement())
	            .addElement(new Element.Builder<String>().setValue(worker.getZipCode()).setType(ElementType.TEXT).createElement())
	            .addElement(new Element.Builder<Date>().setValue(birthday).setType(ElementType.DATE).createElement())
	            .createDocument();
		
		return ret;
	}
}

package de.kommone.wibas.listimport.control;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import de.kommone.wibas.listimport.entity.Homeworker;

@XmlRootElement
public class MappingResult implements Serializable {
	
	private List<Map<Long, Homeworker>> fullMatch = new ArrayList<>();
	
	private List<Map<Long, Homeworker>> mightMatch = new ArrayList<>();
	
	private List<Homeworker> noMatch = new ArrayList<>();

	public List<Map<Long, Homeworker>> getFullMatch() {
		return fullMatch;
	}

	public void setFullMatch(List<Map<Long, Homeworker>> fullMatch) {
		this.fullMatch = fullMatch;
	}

	public List<Map<Long, Homeworker>> getMightMatch() {
		return mightMatch;
	}

	public void setMightMatch(List<Map<Long, Homeworker>> mightMatch) {
		this.mightMatch = mightMatch;
	}

	public List<Homeworker> getNoMatch() {
		return noMatch;
	}

	public void setNoMatch(List<Homeworker> noMatch) {
		this.noMatch = noMatch;
	}	
}

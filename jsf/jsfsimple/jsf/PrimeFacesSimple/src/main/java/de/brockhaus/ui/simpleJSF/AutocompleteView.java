package de.brockhaus.ui.simpleJSF;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
public class AutocompleteView {
	
	private static final Logger LOG = LoggerFactory.getLogger(AutocompleteView.class);

	private List<String> values = new ArrayList<String>();
	private String value;
	
	@PostConstruct
	public void populateList() {
		values.add("a");
		values.add("b");
		values.add("c");
		values.add("aa");
		values.add("ba");
		values.add("ca");
		values.add("aaa");
		values.add("baa");
		values.add("aaa");
		values.add("aba");
		values.add("aca");
		values.add("baa");
		values.add("bba");
		values.add("bca");
		values.add("caa");
		values.add("cba");
		values.add("cbb");
		values.add("cca");
		values.add("ccc");
		LOG.info("List populated by @PostConstruct");
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public List<String> completeText(String query) {
		List<String> matches = new ArrayList<String>();
		// otherwise every string containing "query" will be displayed
		for (String string : values) {
			if(string.startsWith(query)) {
				matches.add(string);
			}
		}
		return matches;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}
	
	public void onItemSelect(SelectEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Item Selected", event.getObject().toString()));
    }
}
